<?php
/* @var $this BasColoniaController */
/* @var $model BasColonia */

$this->breadcrumbs=array(
	'Bas Colonias'=>array('index'),
	$model->id_colonia=>array('view','id'=>$model->id_colonia),
	'Update',
);

$this->menu=array(
	array('label'=>'List BasColonia', 'url'=>array('index')),
	array('label'=>'Create BasColonia', 'url'=>array('create')),
	array('label'=>'View BasColonia', 'url'=>array('view', 'id'=>$model->id_colonia)),
	array('label'=>'Manage BasColonia', 'url'=>array('admin')),
);
?>

<h1>Update BasColonia <?php echo $model->id_colonia; ?></h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>
<?php
/* @var $this BasColoniaController */
/* @var $model BasColonia */

$this->breadcrumbs=array(
	'Bas Colonias'=>array('index'),
	'Create',
);

$this->menu=array(
	array('label'=>'List BasColonia', 'url'=>array('index')),
	array('label'=>'Manage BasColonia', 'url'=>array('admin')),
);
?>

<h1>Create BasColonia</h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>
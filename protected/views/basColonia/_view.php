<?php
/* @var $this BasColoniaController */
/* @var $data BasColonia */
?>

<div class="view">

	<b><?php echo CHtml::encode($data->getAttributeLabel('id_colonia')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->id_colonia), array('view', 'id'=>$data->id_colonia)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('colonia')); ?>:</b>
	<?php echo CHtml::encode($data->colonia); ?>
	<br />


</div>
<?php
/* @var $this BasColoniaController */
/* @var $model BasColonia */

$this->breadcrumbs=array(
	'Bas Colonias'=>array('index'),
	$model->id_colonia,
);

$this->menu=array(
	array('label'=>'List BasColonia', 'url'=>array('index')),
	array('label'=>'Create BasColonia', 'url'=>array('create')),
	array('label'=>'Update BasColonia', 'url'=>array('update', 'id'=>$model->id_colonia)),
	array('label'=>'Delete BasColonia', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->id_colonia),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Manage BasColonia', 'url'=>array('admin')),
);
?>

<h1>View BasColonia #<?php echo $model->id_colonia; ?></h1>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'id_colonia',
		'colonia',
	),
)); ?>

<?php
/* @var $this BasColoniaController */
/* @var $dataProvider CActiveDataProvider */

$this->breadcrumbs=array(
	'Bas Colonias',
);

$this->menu=array(
	array('label'=>'Create BasColonia', 'url'=>array('create')),
	array('label'=>'Manage BasColonia', 'url'=>array('admin')),
);
?>

<h1>Bas Colonias</h1>

<?php $this->widget('zii.widgets.CListView', array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>

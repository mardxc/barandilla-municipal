<?php
/* @var $this GenEstadoController */
/* @var $model GenEstado */

$this->breadcrumbs=array(
	'Gen Estados'=>array('index'),
	'Create',
);

$this->menu=array(
	array('label'=>'List GenEstado', 'url'=>array('index')),
	array('label'=>'Manage GenEstado', 'url'=>array('admin')),
);
?>

<h1>Create GenEstado</h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>
<?php
/* @var $this DetBandasController */
/* @var $model DetBandas */
/* @var $form CActiveForm */
?>

<div class="form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'det-bandas-form',
	'enableAjaxValidation'=>false,
)); ?>

	<p class="note">Fields with <span class="required">*</span> are required.</p>

	<?php echo $form->errorSummary($model); ?>

	<div class="row">
		<?php echo $form->labelEx($model,'banda'); ?>
		<?php echo $form->textField($model,'banda',array('size'=>30,'maxlength'=>30)); ?>
		<?php echo $form->error($model,'banda'); ?>
	</div>

	<div class="row">
		<?php echo $form->textField($model,'colonia',array('style'=>'display: none;')); ?>
		<br><br>
		<?php echo $form->labelEx($model,'Colonia'); ?>
		<?php 
			$this->widget('zii.widgets.jui.CJuiAutocomplete', array(
				'id'=>'coloniaAfectado',
				'name'=>'coloniaAfectado',
				'source'=>$this->createUrl('DetDetenciones/listadoColonia'),
				'options'=>array(
					'delay'=>300,
					'minLength'=>1,
					'showAnim'=>'fold',
					'select'=>"js:function(event,ui){
						$('#DetBandas_colonia').val(ui.item.id);								
					}"
					),
					'htmlOptions'=>array(
						'style'=>'margin-top:10px;'
						),
						
					)); 
			?> 
	</div>

	<div class="row buttons">
		<?php echo CHtml::submitButton($model->isNewRecord ? 'Create' : 'Save'); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- form -->
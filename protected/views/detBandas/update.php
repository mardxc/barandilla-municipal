<?php
/* @var $this DetBandasController */
/* @var $model DetBandas */

$this->breadcrumbs=array(
	'Det Bandases'=>array('index'),
	$model->id_bandas=>array('view','id'=>$model->id_bandas),
	'Update',
);

$this->menu=array(
	array('label'=>'List DetBandas', 'url'=>array('index')),
	array('label'=>'Create DetBandas', 'url'=>array('create')),
	array('label'=>'View DetBandas', 'url'=>array('view', 'id'=>$model->id_bandas)),
	array('label'=>'Manage DetBandas', 'url'=>array('admin')),
);
?>

<h1>Update DetBandas <?php echo $model->id_bandas; ?></h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>
<script>
	function addItem() {
		var id_persona = $('#id_persona').val();
		var alias = $('#alias').val();

		<?php echo CHtml::ajax(array(
			'url'=>array('DetAlias/AddAlias'),
			'data'=>array(
				'id_persona'=>'js:id_persona',
				'alias'=>'js:alias',
			),
			'type'=>'post',
			'dataType'=>'json',
			'success'=>"function(data)
			{
				
			}"
		))
		?>;	
		return false;
	}	
</script>

<br>
<br>
<br>
<h1>
	Alias
</h1>
<a class="btn btn-primary" onclick="addItem()">Agregar Alias
	<i class="fa fa-plus"></i>
</a>

<a class="btn btn-danger" onclick="">Eliminar Producto
	<i class="fa fa-trash-o"></i>
</a>
<table>
	<tr>
		<td>
			Persona
		</td>
		<td>
			<input type="text" style="display:none;" id="id_persona">
			<?php 
				$this->widget('zii.widgets.jui.CJuiAutocomplete', array(
					'id'=>'alias_grid',
					'name'=>'clienteName',
					'source'=>$this->createUrl('DetAlias/listadoPersona'),
					'options'=>array(
						'delay'=>300,
						'minLength'=>1,
						'showAnim'=>'fold',
						'select'=>"js:function(event,ui){
							$('#id_persona').val(ui.item.id);								
						}"
					),
					'htmlOptions'=>array(
						'size'=>'30px',
						'style'=>'margin-top:10px;'
						),
					)); 
				?> 
		</td>
		<td>
			Alias
		</td>
		<td>
			<input type="text" id="alias">
		</td>
	</tr>
</table>

<div class="table-responsive">
	<table class="table table-hover">
		<thead>
			<tr>
				<th>Alias</th>
				<th>Accion</th>
			</tr>
		</thead>
		<tbody>
			<tr>
				<td>Charlie</td>
				<td>
					<i><a href="#" class="fa fa-times"></a></i>
				</td>
			</tr>
		</tbody>
	</table>
</div>
<?php
	  $baseUrl = Yii::app()->theme->baseUrl; 
	  $cs = Yii::app()->getClientScript();
	  Yii::app()->clientScript->registerCoreScript('jquery');
	?>
	<?php
	  $cs->registerScriptFile($baseUrl.'/js/jquery.min.js');
	  $cs->registerScriptFile($baseUrl.'/js/jquery.ui.min.js');
	  $cs->registerScriptFile($baseUrl.'/js/bootstrap.min.js');
	?>
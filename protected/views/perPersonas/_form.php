<?php
/* @var $this PerPersonasController */
/* @var $model PerPersonas */
/* @var $form CActiveForm */
?>

<script>
	function determinaSexo(){
		var sexo= $('#perPersona_sexo').val();
		

			if(sexo=='H')
				$('#AluAlumno_sexo').val('M');
			else
				$('#AluAlumno_sexo').val('F');
		

		
	}
</script>

<div class="form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'per-personas-form',
	'enableAjaxValidation'=>false,
)); ?>

	<p class="note">Fields with <span class="required">*</span> are required.</p>

	<?php echo $form->errorSummary($model); ?>

	<div class="row">
		<?php echo $form->labelEx($model,'nombre'); ?>
		<?php echo $form->textField($model,'nombre',array('size'=>20,'maxlength'=>20)); ?>
		<?php echo $form->error($model,'nombre'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'ape_pat'); ?>
		<?php echo $form->textField($model,'ape_pat',array('size'=>20,'maxlength'=>20)); ?>
		<?php echo $form->error($model,'ape_pat'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'ape_mat'); ?>
		<?php echo $form->textField($model,'ape_mat',array('size'=>20,'maxlength'=>20)); ?>
		<?php echo $form->error($model,'ape_mat'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'telefono'); ?>
		<?php echo $form->textField($model,'telefono'); ?>
		<?php echo $form->error($model,'telefono'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'sexo'); ?>
		<?php echo $form->textField($model,'sexo',array('size'=>10,'maxlength'=>10)); ?>
		<?php echo $form->error($model,'sexo'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'fecha_nacimiento'); ?>
		
		<?php 
		 	$this->widget('zii.widgets.jui.CJuiDatePicker', array(
		 	'model'=>$model,
		 	'attribute'=>'fecha_nac',
		 	'value'=>CTimestamp::formatDate('dd/mm/yyyy',$model->fecha_nacimiento),
		 	'language' => 'es',
		 	'htmlOptions'=> array('style'=>'width:160px'),
		 	'options'=>array(
		 		'autoSize'=>false,
		 		'defaultDate'=>$model->fecha_nac,
		 		'dateFormat'=>'dd/mm/yy',
		 		'buttonImage'=>Yii::app()->baseUrl.'/images/calendar.gif',
		 		'buttonImageOnly'=>true,
		 		'buttonText'=>'Fecha',
		 		 'yearRange'=>'1970:2099',
		 		'selectOtherMonths'=>true,
		 		'showAnim'=>'slide',
		 		'showButtonPanel'=>true,
		 		'showOn'=>'button',
		 		'showOtherMonths'=>true,
		 		'changeMonth' => true,
		 		'changeYear' => true,
		 		),
		 	));
	 	?>
		<?php echo $form->error($model,'fecha_nacimiento'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'estado_civil'); ?>
		<?php echo $form->dropDownList($model,'estado_civil',perPersona::getEstado(),array('width'=>400)); ?>
		<?php echo $form->error($model,'estado_civil'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'nacionalidad'); ?>
		<?php echo $form->textField($model,'nacionalidad',array('size'=>20,'maxlength'=>20)); ?>
		<?php echo $form->error($model,'nacionalidad'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'señas_particulares'); ?>
		<?php echo $form->textField($model,'señas_particulares',array('size'=>50,'maxlength'=>50)); ?>
		<?php echo $form->error($model,'señas_particulares'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'lesiones_presenta'); ?>
		<?php echo $form->textField($model,'lesiones_presenta',array('size'=>50,'maxlength'=>50)); ?>
		<?php echo $form->error($model,'lesiones_presenta'); ?>
	</div>

	<div class="row buttons">
		<?php echo CHtml::submitButton($model->isNewRecord ? 'Create' : 'Save'); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- form -->
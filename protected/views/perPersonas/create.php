<?php
/* @var $this PerPersonasController */
/* @var $model PerPersonas */

$this->breadcrumbs=array(
	'Per Personases'=>array('index'),
	'Create',
);

$this->menu=array(
	array('label'=>'List PerPersonas', 'url'=>array('index')),
	array('label'=>'Manage PerPersonas', 'url'=>array('admin')),
);
?>

<h1>Create PerPersonas</h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>
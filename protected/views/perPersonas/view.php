<?php
/* @var $this PerPersonasController */
/* @var $model PerPersonas */

$this->breadcrumbs=array(
	'Per Personases'=>array('index'),
	$model->id_persona,
);

$this->menu=array(
	array('label'=>'List PerPersonas', 'url'=>array('index')),
	array('label'=>'Create PerPersonas', 'url'=>array('create')),
	array('label'=>'Update PerPersonas', 'url'=>array('update', 'id'=>$model->id_persona)),
	array('label'=>'Delete PerPersonas', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->id_persona),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Manage PerPersonas', 'url'=>array('admin')),
);
?>

<h1>View PerPersonas #<?php echo $model->id_persona; ?></h1>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'id_persona',
		'nombre',
		'ape_pat',
		'ape_mat',
		'telefono',
		'sexo',
		'fecha_nacimiento',
		'estado_civil',
		'nacionalidad',
		'señas_particulares',
		'lesiones_presenta',
	),
)); ?>

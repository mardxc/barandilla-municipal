

<h1>Manage Per Personases</h1>



<?php $this->widget('zii.widgets.grid.CGridView', array(
	'id'=>'per-personas-grid',
	'dataProvider'=>$model->search(),
	'filter'=>$model,
	'columns'=>array(
		'id_persona',
		'nombre',
		'ape_pat',
		'ape_mat',
		'telefono',
		'sexo',
		/*
		'fecha_nacimiento',
		'estado_civil',
		'nacionalidad',
		'señas_particulares',
		'lesiones_presenta',
		*/
		array(
			'class'=>'CButtonColumn',
		),
	),
)); ?>

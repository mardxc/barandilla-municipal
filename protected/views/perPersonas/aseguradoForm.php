<script>
	function guardarAll(){
		var guarda=$('#guardar').val();
		var id_persona=$('#id_persona').val();
		var nombre=$('#nombre').val();
		var ape_pat=$('#ape_pat').val();
		var ape_mat=$('#ape_mat').val();
		var telefono=$('#telefono').val();
		var sexo=$('#sexo').val();
		var fecha_nacimiento=$('#fecha_nacimiento').val();
		var estado_civil=$('#estado_civil').val();
		var colonia=$('#colonia').val();
		var nacionalidad=$('#nacionalidad').val();
		var señas_particulares=$('#señas_particulares').val();
		var lesiones_presenta=$('#lesiones_presenta').val();

		var estatura=$('#estatura').val();
		var color_piel=$('#color_piel').val();
		var cejas=$('#cejas').val();
		var color_ojos=$('#color_ojos').val();
		var color_pelo=$('#color_pelo').val();
		var menton=$('#menton').val();
		var frente=$('#frente').val();
		var complexion=$('#complexion').val();

		var id_objeto_porta=$('#id_objeto_porta').val();
		var descripcion=$('#descripcion').val();
		var camisa=$('#camisa').val();
		var pantalon=$('#pantalon').val();
		var playera=$('#playera').val();
		var chamarra=$('#chamarra').val();
		var tenis=$('#tenis').val();
		var zapatos=$('#zapatos').val();
		var botas=$('#botas').val();
		var cadena=$('#cadena').val();
		var Sombrero=$('#Sombrero').val();
		var pulsera=$('#pulsera').val();
		var gorra=$('#gorra').val();
		var cinturon=$('#cinturon').val();
		var cartera=$('#cartera').val();
		var lentes=$('#lentes').val();
		var otros=$('#otros').val();

		var id_identificaciones=$('#id_identificaciones').val();
		var identificacion=$('#identificacion').val();

		var id_dinero=$('#id_dinero').val();
		var billetes=$('#billetes').prop('checked') ;
		var monedas=$('#monedas').prop('checked');
		var dolares=$('#dolares').prop('checked');
		var otros=$('#otros').prop('checked');
		var total=$('#total').val();

		var id_motivos=$('#id_motivos').val();
		var motivo=$('#motivo').val();
		var descripcion=$('#descripcion').val();
		var gravedad=$('#gravedad').val();

		<?php echo CHtml::ajax(array(
		   	'url'=>array('perPersona/guardarAll'),
		    'data'=> array(
		    	   	'id_persona'=>'js:id_persona',
		    	   	'nombre'=>"js:nombre",
		    	   	'ape_pat'=>"js:ape_pat",
		    	   	'ape_mat'=>"js:ape_mat",
		    	   	'telefono'=>"js:telefono",
		    	   	'sexo'=>"js:sexo",
		    	   	'fecha_nacimiento'=>"js:fecha_nacimiento",
		    		'estado_civil'=>"js:estado_civil",
		    	   	'colonia'=>"js:colonia",
		    	   	'nacionalidad'=>"js:nacionalidad",
		    	   	'señas_particulares'=>"js:señas_particulares",
		    	   	'lesiones_presenta'=>"js:lesiones_presenta",

		    	   	'estatura'=>"js:estatura",
		    	   	'color_piel'=>"js:color_piel",
		    	   	'cejas'=>"js:cejas",
		    	   	'color_ojos'=>"js:color_ojos",
		    	   	'color_pelo'=>"js:color_pelo",
		    	   	'menton'=>"js:menton",
		    	   	'frente'=>"js:frente",
		    	   	'complexion'=>"js:complexion",

		    	   	'id_objeto_porta'=>"js:id_objeto_porta",
		    	   	'descripcion'=>"js:descripcion",
		    	   	'camisa'=>"js:camisa",
		    	   	'pantalon'=>"js:pantalon",
		    	   	'playera'=>"js:playera",
		    	   	'chamarra'=>"js:chamarra",
		    	   	'tenis'=>"js:tenis",
		    	   	'zapatos'=>"js:zapatos",
		    	   	'botas'=>"js:botas",
		    	   	'cadena'=>"js:cadena",
		    	   	'sombrero'=>"js:sombrero",
		    	   	'pulsera'=>"js:pulsera",
		    	   	'gorra'=>"js:gorra",
		    	   	'cinturon'=>"js:cinturon",
		    	   	'cartera'=>"js:cartera",
		    	   	'lentes'=>"js:lentes",
		    	   	'otros'=>"js:otros",
		    	
		     	'guarda'=>"js:guarda"
		    ),
		    'type'=>'post',
		    'dataType'=>'json',
		    'success'=>"function(data){	

				alert('La información de la persona ha sido guardada');
				
					$('#id_persona').val(data.id_persona);

			    if(data.status=='guardar'){
					$('#guardar').html('actualizar');
					$('#guardar').val('actualizar');
			    }
			 } "
		))?>;
		return false; 
	}
</script>

<script>
	function guardarRecluso(){
		var guarda=$('#guardar').val();
		var id_persona=$('#id_persona').val();
		var nombre=$('#nombre').val();
		var ape_pat=$('#ape_pat').val();
		var ape_mat=$('#ape_mat').val();
		var telefono=$('#telefono').val();
		var sexo=$('#sexo').val();
		var fecha_nacimiento=$('#fecha_nacimiento').val();
		var estado_civil=$('#estado_civil').val();
		var colonia=$('#colonia').val();
		var nacionalidad=$('#nacionalidad').val();
		var señas_particulares=$('#señas_particulares').val();
		var lesiones_presenta=$('#lesiones_presenta').val();

		

		<?php echo CHtml::ajax(array(
		   	'url'=>array('perPersona/guardar'),
		    'data'=> array(
		    	'id_persona'=>'js:id_persona',
		    	'nombre'=>"js:nombre",
		    	'ape_pat'=>"js:ape_pat",
		    	'ape_mat'=>"js:ape_mat",
		    	'telefono'=>"js:telefono",
		    	'sexo'=>"js:sexo",
		    	'fecha_nacimiento'=>"js:fecha_nacimiento",
		 		  		    	'estado_civil'=>"js:estado_civil",
		    	'colonia'=>"js:colonia",
		    	'nacionalidad'=>"js:nacionalidad",
		    	'señas_particulares'=>"js:señas_particulares",
		    	'lesiones_presenta'=>"js:lesiones_presenta",

		    	
		     	'guarda'=>"js:guarda"
		    ),
		    'type'=>'post',
		    'dataType'=>'json',
		    'success'=>"function(data){	

				alert('La información de la persona ha sido guardada');
				
					$('#id_persona').val(data.id_persona);

			    if(data.status=='guardar'){
					$('#guardar').html('actualizar');
					$('#guardar').val('actualizar');
			    }
			 } "
		))?>;
		return false; 
	}

function guardarAse()
{
	var salvar=$('#salvar').val();
		var id_persona=$('#id_persona').val();
		var estatura=$('#estatura').val();
		var color_piel=$('#color_piel').val();
		var cejas=$('#cejas').val();
		var color_ojos=$('#color_ojos').val();
		var color_pelo=$('#color_pelo').val();
		var menton=$('#menton').val();
		var frente=$('#frente').val();
		var complexion=$('#complexion').val();
		var nacionalidad=$('#nacionalidad').val();
		
		
<?php echo CHtml::ajax(array(
		   	'url'=>array('perPersona/guardar'),
		    'data'=> array(
		    	'id_persona'=>'js:id_persona',
		    	'estatura'=>"js:estatura",
		    	'color_piel'=>"js:color_piel",
		    	'cejas'=>"js:cejas",
		    	'color_ojos'=>"js:color_ojos",
		    	'color_pelo'=>"js:color_pelo",
		    	'menton'=>"js:menton",
		 		 'frente'=>"js:frente",
		    	'complexion'=>"js:complexion",
		    	'nacionalidad'=>"js:nacionalidad",
		    	
		     	'guarda'=>"js:guarda"
		    ),
		    'type'=>'post',
		    'dataType'=>'json',
		    'success'=>"function(data){	

				alert('La información de la persona ha sido guardada');
				
					$('#id_persona').val(data.id_persona);

			    if(data.status=='guardar'){
					$('#guardar').html('actualizar');
					$('#guardar').val('actualizar');
			    }
			 } "
		))?>;
		return false;


}



function guardarAse2()
{
	var salvar=$('#salvar').val();
		var id_objeto_porta=$('#id_objeto_porta').val();
		var descripcion=$('#descripcion').val();
		var camisa=$('#camisa').val();
		var pantalon=$('#pantalon').val();
		var playera=$('#playera').val();
		var chamarra=$('#chamarra').val();
		var tenis=$('#tenis').val();
		var zapatos=$('#zapatos').val();
		var botas=$('#botas').val();
		var cadena=$('#cadena').val();
		var Sombrero=$('#Sombrero').val();
		var pulsera=$('#pulsera').val();
		var gorra=$('#gorra').val();
		var cinturon=$('#cinturon').val();
		var cartera=$('#cartera').val();
		var lentes=$('#lentes').val();
		var otros=$('#otros').val();

		


		
<?php echo CHtml::ajax(array(
		   	'url'=>array('perPersona/guardar'),
		    'data'=> array(
		    	'id_objeto_porta'=>'js:id_objeto_porta',
		    	'descripcion'=>"js:descripcion",
		    	'camisa'=>"js:camisa",
		    	'pantalon'=>"js:pantalon",
		    	'playera'=>"js:playera",
		    	'chamarra'=>"js:chamarra",
		    	'tenis'=>"js:tenis",
		 		 'zapatos'=>"js:zapatos",
		    	'botas'=>"js:botas",
		    	'cadena'=>"js:cadena",
		    	'Sombrero'=>"js:Sombrero",
		    	'pulsera'=>"js:pulsera",
		    	'gorra'=>"js:gorra",
		    	'cinturon'=>"js:cinturon",
		    			    	'cartera'=>"js:cartera",
		    	'lentes'=>"js:lentes",
		    	'otros'=>"js:otros",

		    



		    	
		     	'guarda'=>"js:guarda"
		    ),
		    'type'=>'post',
		    'dataType'=>'json',
		    'success'=>"function(data){	

				alert('La información de la persona ha sido guardada');
				
					$('#id_persona').val(data.id_persona);

			    if(data.status=='guardar'){
					$('#guardar').html('actualizar');
					$('#guardar').val('actualizar');
			    }
			 } "
		))?>;
		return false;


}

function guardarAse3()
{
	var salvar=$('#salvar').val();
	var id_identificaciones=$('#id_identificaciones').val();
	var identificacion=$('#identificacion').val();

		
<?php echo CHtml::ajax(array(
		   	'url'=>array('perPersona/guardar'),
		    'data'=> array(
		    	'id_identificaciones'=>'js:id_identificaciones',
		    	'identificacion'=>"js:identificacion",



		    	
		     	'guarda'=>"js:guarda"
		    ),
		    'type'=>'post',
		    'dataType'=>'json',
		    'success'=>"function(data){	

				alert('La información de la persona ha sido guardada');
				
					$('#id_persona').val(data.id_persona);

			    if(data.status=='guardar'){
					$('#guardar').html('actualizar');
					$('#guardar').val('actualizar');
			    }
			 } "
		))?>;
		return false;


}


function guardarDinero(){
		//obtiene los valores de los objetos y los guarda en variables JS
		var guarda=$('#guardarDinero').val();
		var id_dinero=$('#id_dinero').val();
		var billetes=$('#billetes').prop('checked') ;
		var monedas=$('#monedas').prop('checked');
		var dolares=$('#dolares').prop('checked');
		var otros=$('#otros').prop('checked');
		var total=$('#total').val();
		

// es el guardar en AJAX
		<?php echo CHtml::ajax(array(
		   	'url'=>array('aluDocumentacion/guardar'),
		    'data'=> array(// las variables JS las convierte a PHP para mandarlas por AJAX
		    	'id_alumno'=>"js:id_alumno",
		    	'billetes'=>"js:billetes",
		    	'monedas'=>"js:monedas",
		    	'dolares'=>"js:dolares",
		    	'otros'=>"js:otros",
		    	'total'=>'js:total',
		    	

		     	'guarda'=>"js:guarda",
		    ),
		    // Hace el paso de valores y con success prepara para la proxima operacion
		    'type'=>'POST',
		    'dataType'=>'json',
		    'success'=>"function(data){	
			    $.each(data.datos,function(key,value){
					if(value==1){
						$('#key').prop('checked','checked') ;
					}else{
						$('#key').removeAttr('checked');
					}
			    });
			    				
				alert('La documentación del recluso ha sido guardada');

			    if(data.status=='guardar' && data.id_dinero==0){
					$('#guardar').html('actualizar');
					$('#guardar').val('actualizar');
			    }
			 } "
		))?>;
		return false; 
	}


function guardarMotivos()
{
	var salvar=$('#salvar').val();
	var id_motivos=$('#id_motivos').val();
	var motivo=$('#motivo').val();
	var descripcion=$('#descripcion').val();
	var gravedad=$('#gravedad').val();

		
<?php echo CHtml::ajax(array(
		   	'url'=>array('perPersona/guardar'),
		    'data'=> array(
		    	'id_motivos'=>'js:id_motivos',
		    	'motivo'=>"js:motivo",
		    	'descripcion'=>"js:descripcion",
		    	'gravedad'=>"js:gravedad",



		    	
		     	'guarda'=>"js:guarda"
		    ),
		    'type'=>'post',
		    'dataType'=>'json',
		    'success'=>"function(data){	

				alert('La información de la persona ha sido guardada');
				
					$('#id_motivos').val(data.id_motivos);

			    if(data.status=='guardar'){
					$('#guardar').html('actualizar');
					$('#guardar').val('actualizar');
			    }
			 } "
		))?>;
		return false;


}



</script>

<?php  
	if (isset($id_persona))  {
		$id_persona=$model->id_persona;
		$nombre=$model->nombre;
		$ape_pat=$model->ape_pat;
		$ape_mat=$model->ape_mat;
		$telefono=$model->telefono;
		$sexo=$model->sexo;
		$fecha_nacimiento=$model->fecha_nacimiento;
		$estado_civil=$model->estado_civil;
		$nacionalidad=$model->nacionalidad;
		$señas_particulares=$model->señas_particulares;
		$lesiones_presenta=$model->lesiones_presenta;

		
	} else{ 
		$id_persona=0;
	
		$nombre='';
		$ape_pat='';
		$ape_mat='';
		$telefono='';
		$sexo='';
		$fecha_nacimiento='';
		$estado_civil='';
		$nacionalidad='';
		$señas_particulares='';
		$lesiones_presenta='';

		
	}
?>

<?php 
if (isset($model->id_persona))  {
		$id_persona=$model->id_persona;
		$estatura=$model->estatura;
		$color_piel=$model->color_piel;
		$cejas=$model->cejas;
		$color_ojos=$model->color_ojos;
		$color_pelo=$model->color_pelo;

		$menton=$model->menton;
		$frente=$model->frente;
		$complexion=$model->complexion;

		$skin=$model->skin;
		$eyes=$model->eyes;
		$hair=$model->hair;
		$skin=$model->skin;
		$body=$model->body;
		$condicion=$model->condicion;
	} else{ 

		$id_persona=0;
		$estatura='';
		$color_piel='';
		$cejas='';
		$color_ojos='';
		$color_pelo='';

		$menton='';
		$frente='';
		$complexion='';

		$skin='';
		$eyes='';
		$hair='';
		$body='';
		$condicion='';
	}
	


 ?>

 <?php 
if (isset($model->id_objeto_porta))  {
		$id_objeto_porta=$model->id_objeto_porta;
		$descripcion=$model->descripcion;
		$camisa=checados($model->camisa);
		$pantalon=checados($model->pantalon);
		$playera=checados($model->playera);
		$chamarra=checados($model->chamarra);

		$tenis=checados($model->tenis);
		$curpp=checados($model->curpp);
		$zapatos=checados($model->zapatos);
		$botas=checados($model->botas);
		$sombrero=checados($model->sombrero);
				$cadena=checados($model->cadena);
		$pulsera=checados($model->pulsera);
		$gorra=checados($model->gorra);
		$cinturon=checados($model->cinturon);
		$cartera=checados($model->cartera);
				$lentes=checados($model->lentes);
						$otros=checados($model->otros);



	} else{ 
		$id_objeto_porta=0;
		$descripcion='';
		$camisa=false;
		$pantalon=false;
		$playera=false;
		$chamarra=false;

		$tenis=false;
		$curpp=false;
		$zapatos=false;
		$botas=false;
		$sombrero=false;
		$cadena=false;
		$pulsera=false;
		$gorra=false;
		$cinturon=false;
		$cartera=false;
		$lentes=false;
		$otros=false;
	}

  ?>

   <?php 
if (isset($model->id_identificaciones))  {
		$id_identificaciones=$model->id_identificaciones;
		$identificacion=$model->identificacion;



	} else{ 
		$id_identificaciones=0;
		$identificacion='';
	}
		

  ?>

  <?php  
//asigna en variables los valores que estan en el modelo
	if (isset($model->dinero))  {
		$id_dinero=$model->id_dinero;
		$billetes=checados($model->billetes);
		$monedas=checados($model->monedas);
		$dolares=checados($model->dolares);
		$otros=checados($model->otros);
		$total=$model->total;

		
	} else{ 
// si no esxiste un id_alumno en el modelo, pone vacios todos los campos
		$id_dinero=0;// $id viene de aluAlumno/detalles, siempre se pasa ese valor junto al modelo
		$billetes=false;
		$monedas=false;
		$dolares=false;
		$otros=false;
		$total='';
		
	}
	
	function checados($check=null){
		if($check==1) return true; else false;// Revisa si los checks estan marcados
	}
?>

<?php 
if (isset($model->id_motivos))  {
		$id_motivos=$model->id_motivos;
		$motivo=$model->motivo;
		$descripcion=$model->descripcion;
		$gravedad=$model->gravedad;


	} else{ 
		$id_motivos=0;
		$motivo='';
		$descripcion='';
		$gravedad='';

	}
	if (isset($id_lugar)) {		
		$colonia=$model->colonia;
		$calle=$model->calle;
		$num_int=$model->num_int;
		$num_ext=$model->num_ext;
		$entre_calle=$model->entre_calle;
		$y_calle=$model->y_calle;
		$a_la_altura=$model->a_la_altura;
		$id_barrio=$model->id_barrio;
	}else{
		$id_lugar='';
		$colonia='';
		$calle='';
		$num_int='';
		$num_ext='';
		$entre_calle='';
		$y_calle='';
		$a_la_altura='';
		$id_barrio='';
	}

  ?>



<br><br><br><br>
<h1>Guardar Persona</h1>

<input type="text" id="id_persona" style="display:none;" value="<?php echo $id_persona; ?>">
<input type="text" id="id_objeto_porta" style="display:none;" value="<?php echo $id_objeto_porta; ?>">
<input type="text" id="id_identificaciones" style="display:none;" value="<?php echo $id_identificaciones; ?>">
<input type="text" id="id_lugar" style="display:none;" value="<?php echo $id_lugar; ?>">


<div>
	<table class="table table-hover">
			<tr>
			<td>Nombres</td>
				<td>
					<?php echo CHtml::textField(
					'nombre',
					$nombre
				); ?>
				</td>

				<td>Apellido Paterno</td>
				<td>
					<?php echo CHtml::textField(
					'ape_pat',
					$ape_pat
				); ?>
				</td>
				
			</tr>

			<tr>
				<td>Apellido Materno</td>
				<td>
					<?php echo CHtml::textField(
					'ape_mat',
					$ape_mat
				); ?>
				</td>
			
			</tr>

			<tr>
				<td>Fecha de nacimiento</td>
				<td>
					<?php 
					 	$this->widget('zii.widgets.jui.CJuiDatePicker', array(
						'name'=>'fecha_nacimiento',
					 	'value'=>$fecha_nacimiento,
					 	'language' => 'es',
					 	'htmlOptions'=> array('style'=>'width:90%'),
					 	'options'=>array(
					 		'autoSize'=>false,
					 		'defaultDate'=>$fecha_nacimiento,
					 		//'dateFormat'=>'dd/mm/yy',
					 		'buttonImage'=>Yii::app()->baseUrl.'/images/calendar.gif',
					 		'buttonImageOnly'=>true,
					 		'buttonText'=>'Fecha',
					 		'yearRange'=>'1970:2099',
					 		'selectOtherMonths'=>true,
					 		'showAnim'=>'slide',
					 		'showButtonPanel'=>true,
					 		'showOn'=>'button',
					 		'showOtherMonths'=>true,
					 		'changeMonth' => true,
					 		'changeYear' => true,
					 		),
					 	));
				 	?>			
		 	</td>


				<td>Nacionalidad</td>
				<td>
					
	<?php echo CHtml::textField(
					'nacionalidad',
					$nacionalidad
				); ?>
					
				</td>
			</tr>

			<tr>
				<td>Telefono</td>		
				<td>
	<?php echo CHtml::textField(
					'telefono',
					$telefono
				); ?>			
				</td>

				<td>Estado Civil</td>		
				<td>
					<?php 
					echo CHtml::dropDownList(
						'estado_civil',
						$estado_civil,						
						perPersonas::getEstado(),
						array('empty' => ' ','style'=>'width:90%')
					); 
				?>
				</td>
			</tr>

			<tr>
				<td>Sexo</td>
				<td>
					<?php 
					echo CHtml::dropDownList(
						'sexo',
						$sexo,
						perPersonas::getSexo(),
						array('empty' => ' ','style'=>'width:90%')
					); 
				?>
				</td>
				
			</tr>


			<tr>
				<td>señas <br>
				 particulares</td>
				 <td>
				 <?php echo CHtml::textField(
					'señas_particulares',
					$señas_particulares
				); ?>			
				 	
				 </td>
			</tr>

			<tr>
				<td>Lesiones que <br>
				presenta</td>

				<td><?php echo CHtml::textField(
					'lesiones_presenta',
					$lesiones_presenta
				); ?></td>
			</tr>


		</table>

		<div class="table-responsive">
			<h1>Caracteristicas del Detenido</h1>
			<br>
		
			<tr>
			<td>Estatura</td>
			<td>
			<?php echo CHtml::textField(
					'estatura',
					$estatura
				); ?>
					
				</td>
			</tr>
		
				
				<tr>
					
				<td>Color de Piel</td>
					<td>
						<?php 
					echo CHtml::dropDownList(
						'skin',
						$skin,
						perPersonas::getSkin(),
						array('empty' => ' ','style'=>'width:90%')
					); 
				?>
					</td>
				</tr>


				<tr>
					<td>
						Cejas
					</td>
					<td>
						<?php echo CHtml::textField(
					'cejas',
					$cejas
				); ?>
					</td>


					<td>
						Color de Ojos
					</td>
					<td>
						<?php 
					echo CHtml::dropDownList(
						'eyes',
						$eyes,
						perPersonas::getEyes(),
						array('empty' => ' ','style'=>'width:90%')
					); 
				?>
					</td>
				</tr>


				<tr>
					<td>
						Color de Cabello
					</td>
					<td>
						<?php 
					echo CHtml::dropDownList(
						'hair',
						$hair,
						perPersonas::getSkin(),
						array('empty' => ' ','style'=>'width:90%')
					); 
				?>
					</td>
					

					<td>
						Mentón
					</td>	
					<td>
						<?php echo CHtml::textField(
					'menton',
					$menton
				); ?>
					</td>		
				</tr>
				

				<tr>
					<td>
						Frente
					</td>
					<td>
						<?php echo CHtml::textField(
					'frente',
					$frente
				); ?>
					</td>
					

					<td>
						Complexión
					</td>
					<td>
						<?php 
					echo CHtml::dropDownList(
						'body',
						$body,
						perPersonas::getBody(),
						array('empty' => ' ','style'=>'width:90%')
					); 
				?>
					</td>
				</tr>
				

				
				<tr>
				<td>condicion <br>
				en que se <br>
				encuentra
				</td>

					<td>
					<?php 
					echo CHtml::dropDownList(
						'condicion',
						$condicion,
						perPersonas::getCondicion(),
						array('empty' => ' ','style'=>'width:90%')
					); 
				?>
					</td>
				</tr>
				
		
		</div>

	<div>
		<tr>
			<h1>Objetos que porta</h1>
				
			</tr>

			<tr>
			<td>Camisa</td>
				<td>
				<?php echo CHtml::checkBox('camisa',$camisa); ?>
				</td>

				<td>Tenis</td>
				<td>
				<?php echo CHtml::checkBox('tenis',$tenis); ?>
				</td>
				
			</tr>
			<tr>
			<td>Pantalón</td>
				<td>
				<?php echo CHtml::checkBox('pantalon',$pantalon); ?>
				</td>
				<td>Zapatos</td>
				<td>
				<?php echo CHtml::checkBox('zapatos',$zapatos); ?>
				</td>
			</tr>

			<tr>
			<td>Playera</td>
				<td>
				<?php echo CHtml::checkBox('playera',$playera); ?>
				</td>
				<td>Botas</td>
				<td>
				<?php echo CHtml::checkBox('botas',$botas); ?>
				</td>
			</tr>


			<tr>
				<td>Chamarra</td>
				<td>
				<?php echo CHtml::checkBox('chamarra',$chamarra); ?>
				</td>
				<td>Sombrero</td>
				<td>
				<?php echo CHtml::checkBox('sombrero',$sombrero); ?>
				</td>
			</tr>


			<tr>
				<td>Cadena</td>
				<td>
				<?php echo CHtml::checkBox('cadena',$cadena); ?>
				</td>
				<td>Pulsera</td>
				<td>
				<?php echo CHtml::checkBox('pulsera',$pulsera); ?>
				</td>
				
			</tr>


			<tr>
				<td>Gorra</td>
				<td>
				<?php echo CHtml::checkBox('gorra',$gorra); ?>
				</td>
				<td>Cartera</td>
				<td>
				<?php echo CHtml::checkBox('cartera',$cartera); ?>
				</td>
				
			</tr>


			<tr>
			<td>Cinturon</td>
			<td>
				<?php echo CHtml::checkBox('cinturon',$cinturon); ?>
			</td>
			<td>Lentes</td>
			<td>
				<?php echo CHtml::checkBox('lentes',$lentes); ?>
			</td>
			</tr>

			<tr>
				<td>Otros</td>
				<td>
				<?php echo CHtml::checkBox('otros',$otros); ?>
				</td>
				<td>Identificaciones</td>
				<td>
				<?php echo CHtml::textField(
					'identificacion',
					$identificacion
				); ?>
					
				</td>
			</tr>

			<tr>
				<td>Descripcion
					<br>de los <br>
					objetos
				</td>
				<td>
					<?php echo CHtml::textField(
					'descripcion',
					$descripcion
				); ?>
				</td>
			</tr>

			<tr>
				<td>Billetes</td>
				<td>
					<?php echo CHtml::checkBox('billetes',$billetes); ?>

				</td>
				<td>Monedas</td>
				<td>
				<?php echo CHtml::checkBox('monedas',$monedas); ?>
				</td>

			</tr>
			<tr>
				<td>Dolares</td>
				<td>
				<?php echo CHtml::checkBox('dolares',$dolares); ?>

				</td>
				<td>Otros</td>
				<td>
				<?php echo CHtml::checkBox('otros',$otros); ?>

				</td>
			</tr>
			<tr>
				<td>Cantidad total</td>
				<td>
					<?php echo CHtml::textField(
					'total',
					$total
				) ;?>
				</td>
			</tr>

	</div>


	<div>
		<h1>Aseguramiento</h1>

		<tr>
		<td>motivo de<br>
		detencion
		</td>
			<td>
				<?php echo CHtml::textField(
					'motivo',
					$motivo
				); ?>
			</td>
		</tr>
		<tr>
			<td>Descripcion del<br>
			Arresto
			</td>
			<td><?php echo CHtml::textField(
					'descripcion',
					$descripcion
				); ?>
					
				</td>
		</tr>

		<tr>
			<td>Gravedad</td>
			<td><?php echo CHtml::textField(
					'gravedad',
					$gravedad
				); ?>
					
			</td>
		</tr>
		<tr>
			<h1>Lugar del Aseguramiento</h1>
		</tr>
		<tr>
			<td>Colonia</td>
			<td><?php echo CHtml::textField(
					'colonia',
					$colonia
				); ?>
					
				</td>
		</tr>
		<tr>
			<td>calle</td>
			<td>
				<?php echo CHtml::textField(
					'calle',
					$calle
				) ;?>
			</td>
			<td>numero int</td>
			<td>
				<?php echo CHtml::textField(
					'num_int',
					$num_int
				); ?>
			</td>
			<td>numero ext</td>
			<td>
				<?php echo CHtml::textField(
					'num_ext',
					$num_ext
				); ?>
			</td>
			<tr>
				<td>Entre calle</td>
				<td>
					<?php echo CHtml::textField(
					'entre_calle',
					$entre_calle
				) ;?>
				</td>
				<td>y calle</td>
				<td>
					<?php echo CHtml::textField(
					'y_calle',
					$y_calle
				); ?>
				</td>
				<td>
					Barrio
				</td>
				<td>
					<?php 
						echo CHtml::dropDownList(
							'id_barrio',
							$id_barrio,
							basBarrios::getListBarrio()
						); 
					?>
				</td>
			</tr>
		</tr>
	</div>
</div>
<div>
		<td align="center" colspan="2">
				<?php if(!isset($model->id_persona)) { ?>
					<button id="guardar" class="btn btn-success" value="guardar" onclick="guardarAll()">Guardar</button>
				<?php } 
				else { ?>
					<!--<button id="guardar" class="btn btn-success" value="actualizar" onclick="guardarRecluso(),guardarAse(),guardarAse2(),guardarAse3()">Actualizar</button>-->
					<button id="guardar" class="btn btn-success" value="actualizar" onclick="guardarAll()">Actualizar</button>
				<?php }; ?>
			</td>

	</div>	
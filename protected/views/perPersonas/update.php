<?php
/* @var $this PerPersonasController */
/* @var $model PerPersonas */

$this->breadcrumbs=array(
	'Per Personases'=>array('index'),
	$model->id_persona=>array('view','id'=>$model->id_persona),
	'Update',
);

$this->menu=array(
	array('label'=>'List PerPersonas', 'url'=>array('index')),
	array('label'=>'Create PerPersonas', 'url'=>array('create')),
	array('label'=>'View PerPersonas', 'url'=>array('view', 'id'=>$model->id_persona)),
	array('label'=>'Manage PerPersonas', 'url'=>array('admin')),
);
?>

<h1>Update PerPersonas <?php echo $model->id_persona; ?></h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>
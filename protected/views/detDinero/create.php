<?php
/* @var $this DetDineroController */
/* @var $model DetDinero */

$this->breadcrumbs=array(
	'Det Dineros'=>array('index'),
	'Create',
);

$this->menu=array(
	array('label'=>'List DetDinero', 'url'=>array('index')),
	array('label'=>'Manage DetDinero', 'url'=>array('admin')),
);
?>

<h1>Create DetDinero</h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>
<?php
/* @var $this DetDineroController */
/* @var $model DetDinero */
/* @var $form CActiveForm */
?>

<div class="form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'det-dinero-form',
	'enableAjaxValidation'=>false,
)); ?>

	<p class="note">Fields with <span class="required">*</span> are required.</p>

	<?php echo $form->errorSummary($model); ?>

	<div class="row">
		<?php echo $form->labelEx($model,'billetes'); ?>
		<?php echo $form->textField($model,'billetes'); ?>
		<?php echo $form->error($model,'billetes'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'monedas'); ?>
		<?php echo $form->textField($model,'monedas',array('size'=>8,'maxlength'=>8)); ?>
		<?php echo $form->error($model,'monedas'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'dolares'); ?>
		<?php echo $form->textField($model,'dolares',array('size'=>8,'maxlength'=>8)); ?>
		<?php echo $form->error($model,'dolares'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'otros'); ?>
		<?php echo $form->textField($model,'otros',array('size'=>8,'maxlength'=>8)); ?>
		<?php echo $form->error($model,'otros'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'total'); ?>
		<?php echo $form->textField($model,'total',array('size'=>8,'maxlength'=>8)); ?>
		<?php echo $form->error($model,'total'); ?>
	</div>

	<div class="row buttons">
		<?php echo CHtml::submitButton($model->isNewRecord ? 'Create' : 'Save'); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- form -->
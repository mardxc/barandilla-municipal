<?php
/* @var $this DetDineroController */
/* @var $model DetDinero */

$this->breadcrumbs=array(
	'Det Dineros'=>array('index'),
	$model->id_dinero=>array('view','id'=>$model->id_dinero),
	'Update',
);

$this->menu=array(
	array('label'=>'List DetDinero', 'url'=>array('index')),
	array('label'=>'Create DetDinero', 'url'=>array('create')),
	array('label'=>'View DetDinero', 'url'=>array('view', 'id'=>$model->id_dinero)),
	array('label'=>'Manage DetDinero', 'url'=>array('admin')),
);
?>

<h1>Update DetDinero <?php echo $model->id_dinero; ?></h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>
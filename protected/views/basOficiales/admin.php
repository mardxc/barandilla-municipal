

<div class="container">
<h1>Oficiales</h1>

<a class="btn btn-primary" href="index.php?r=basOficiales/create">
	<i class="fa fa-plus"></i>&nbsp Nuevo Oficial
</a>

<?php $this->widget('zii.widgets.grid.CGridView', array(
	'id'=>'bas-oficiales-grid',
	'pager'=>array("htmlOptions"=>array("class"=>"pagination")), 
	'emptyText'=>"No existen resultados en esta búsqueda", 
	'summaryText'=>'{start}-{end} de {count} Oficiales',
	'dataProvider'=>$model->search(),
	'filter'=>$model,
	'columns'=>array(
		'nombre',
		'ape_pat',
		'ape_mat',
		'sexo',
		'fecha_nacimiento',
		
		array(
			'class'=>'CButtonColumn',
			'template'=>'{update}{delete}',
			'deleteConfirmation'=>('¿Desea borrar el registro?'),
		),
	),
)); ?>
	
</div>



<?php
/* @var $this BasOficialesController */
/* @var $model BasOficiales */
/* @var $form CActiveForm */
?>

<div class="form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'bas-oficiales-form',
	'enableAjaxValidation'=>false,
)); ?>

	<p class="note">Fields with <span class="required">*</span> are required.</p>

	<?php echo $form->errorSummary($model); ?>

	<div >
		<?php echo $form->labelEx($model,'grado'); ?>
		<?php echo $form->textField($model,'grado',array('size'=>15,'maxlength'=>15)); ?>
		<?php echo $form->error($model,'grado'); ?>
	</div>

	<div >
		<?php echo $form->labelEx($model,'nombre'); ?>
		<?php echo $form->textField($model,'nombre',array('size'=>20,'maxlength'=>20)); ?>
		<?php echo $form->error($model,'nombre'); ?>
	</div>

	<div >
		<?php echo $form->labelEx($model,'ape_pat'); ?>
		<?php echo $form->textField($model,'ape_pat',array('size'=>20,'maxlength'=>20)); ?>
		<?php echo $form->error($model,'ape_pat'); ?>
	</div>

	<div>
		<?php echo $form->labelEx($model,'ape_mat'); ?>
		<?php echo $form->textField($model,'ape_mat',array('size'=>20,'maxlength'=>20)); ?>
		<?php echo $form->error($model,'ape_mat'); ?>
	</div>

	<div>
		<?php echo $form->labelEx($model,'telefono'); ?>
		<?php echo $form->textField($model,'telefono'); ?>
		<?php echo $form->error($model,'telefono'); ?>
	</div>

	<div>
		<?php echo $form->labelEx($model,'sexo'); ?>
		<?php echo $form->textField($model,'sexo',array('size'=>10,'maxlength'=>10)); ?>
		<?php echo $form->error($model,'sexo'); ?>
	</div>

	<div>
		<?php echo $form->labelEx($model,'fecha_nacimiento'); ?>
		<?php 
			$this->widget('zii.widgets.jui.CJuiDatePicker', array(
				'model'=>$model,
				'attribute'=>'fecha_nacimiento',
				'value'=>CTimestamp::formatDate('dd/mm/yyyy',$model->fecha_nacimiento),
				'language'=>'es',
				'htmlOptions'=>array('style'=>'width:100%'),
				'options'=>array(
					'autosize'=>false,
					'defaultDate'=>$model->fecha_nacimiento,
					'dateFormat'=>'dd/mm/yy',
					'buttonImage'=>Yii::app()->baseUrl.'/images/calendar.gif',
					'buttonImageOnly'=>true,
					'buttonText'=>'fecha_nacimiento',
					'selectOtherMonths'=>true,
					'showAmin'=>'slide',
					'showButtonPanel'=>true,
					'showOn'=>'button',
					'showOtherMonths'=>true,
					'changeMonth'=>true,
					'changeYear'=>true,
				),
			))
		?>
		<?php echo $form->error($model,'fecha_nacimiento'); ?>
	</div>

	<div>
		<?php echo $form->labelEx($model,'estado_civil'); ?>
		<?php echo $form->textField($model,'estado_civil',array('size'=>10,'maxlength'=>10)); ?>
		<?php echo $form->error($model,'estado_civil'); ?>
	</div>

	<div>
		<?php echo $form->labelEx($model,'email'); ?>
		<?php echo $form->textField($model,'email',array('size'=>20,'maxlength'=>20)); ?>
		<?php echo $form->error($model,'email'); ?>
	</div>

	<div class="buttons">
		<?php echo CHtml::submitButton($model->isNewRecord ? 'Crear' : 'Save'); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- form -->
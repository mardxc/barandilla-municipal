<?php
/* @var $this DetObjetosController */
/* @var $model DetObjetos */

$this->breadcrumbs=array(
	'Det Objetoses'=>array('index'),
	$model->id_objeto=>array('view','id'=>$model->id_objeto),
	'Update',
);

$this->menu=array(
	array('label'=>'List DetObjetos', 'url'=>array('index')),
	array('label'=>'Create DetObjetos', 'url'=>array('create')),
	array('label'=>'View DetObjetos', 'url'=>array('view', 'id'=>$model->id_objeto)),
	array('label'=>'Manage DetObjetos', 'url'=>array('admin')),
);
?>

<h1>Update DetObjetos <?php echo $model->id_objeto; ?></h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>
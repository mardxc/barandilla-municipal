<?php
/* @var $this DetObjetosController */
/* @var $model DetObjetos */

$this->breadcrumbs=array(
	'Det Objetoses'=>array('index'),
	'Create',
);

$this->menu=array(
	array('label'=>'List DetObjetos', 'url'=>array('index')),
	array('label'=>'Manage DetObjetos', 'url'=>array('admin')),
);
?>

<h1>Create DetObjetos</h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>
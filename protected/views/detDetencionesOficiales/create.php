<?php
/* @var $this DetDetencionesOficialesController */
/* @var $model DetDetencionesOficiales */

$this->breadcrumbs=array(
	'Det Detenciones Oficiales'=>array('index'),
	'Create',
);

$this->menu=array(
	array('label'=>'List DetDetencionesOficiales', 'url'=>array('index')),
	array('label'=>'Manage DetDetencionesOficiales', 'url'=>array('admin')),
);
?>

<h1>Create DetDetencionesOficiales</h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>
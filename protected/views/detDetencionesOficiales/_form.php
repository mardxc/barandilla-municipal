<?php
/* @var $this DetDetencionesOficialesController */
/* @var $model DetDetencionesOficiales */
/* @var $form CActiveForm */
?>

<div class="form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'det-detenciones-oficiales-form',
	'enableAjaxValidation'=>false,
)); ?>

	<p class="note">Fields with <span class="required">*</span> are required.</p>

	<?php echo $form->errorSummary($model); ?>

	<div class="row">
		<?php echo $form->labelEx($model,'responsable'); ?>
		<?php echo $form->textField($model,'responsable',array('size'=>30,'maxlength'=>30)); ?>
		<?php echo $form->error($model,'responsable'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'id_oficial'); ?>
		<?php echo $form->textField($model,'id_oficial'); ?>
		<?php echo $form->error($model,'id_oficial'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'id_detencion'); ?>
		<?php echo $form->textField($model,'id_detencion'); ?>
		<?php echo $form->error($model,'id_detencion'); ?>
	</div>

	<div class="row buttons">
		<?php echo CHtml::submitButton($model->isNewRecord ? 'Create' : 'Save'); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- form -->
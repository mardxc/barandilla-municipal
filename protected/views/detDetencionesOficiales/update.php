<?php
/* @var $this DetDetencionesOficialesController */
/* @var $model DetDetencionesOficiales */

$this->breadcrumbs=array(
	'Det Detenciones Oficiales'=>array('index'),
	$model->id_det_oficiales=>array('view','id'=>$model->id_det_oficiales),
	'Update',
);

$this->menu=array(
	array('label'=>'List DetDetencionesOficiales', 'url'=>array('index')),
	array('label'=>'Create DetDetencionesOficiales', 'url'=>array('create')),
	array('label'=>'View DetDetencionesOficiales', 'url'=>array('view', 'id'=>$model->id_det_oficiales)),
	array('label'=>'Manage DetDetencionesOficiales', 'url'=>array('admin')),
);
?>

<h1>Update DetDetencionesOficiales <?php echo $model->id_det_oficiales; ?></h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>
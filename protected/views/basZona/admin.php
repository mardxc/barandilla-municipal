

<h1>Zonas</h1>

<a class="btn btn-primary" href="index.php?r=basZona/create">
	<i class="fa fa-plus"></i>&nbsp Nueva Zona
</a>

<?php $this->widget('zii.widgets.grid.CGridView', array(
	'id'=>'bas-zona-grid',
	'pager'=>array("htmlOptions"=>array("class"=>"pagination")), 
	'emptyText'=>"No existen resultados en esta búsqueda", 
	'summaryText'=>'{start}-{end} de {count} Departamentos',
	'dataProvider'=>$model->search(),
	'filter'=>$model,
	'columns'=>array(
		'zona',
		array(
			'class'=>'CButtonColumn',
			'template'=>'{update}{delete}',
			'deleteConfirmation'=>('¿Desea borrar el registro?'),
		),
	),
)); ?>

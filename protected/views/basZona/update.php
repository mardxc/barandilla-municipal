<?php
/* @var $this BasZonaController */
/* @var $model BasZona */

$this->breadcrumbs=array(
	'Bas Zonas'=>array('index'),
	$model->id_zona=>array('view','id'=>$model->id_zona),
	'Update',
);

$this->menu=array(
	array('label'=>'List BasZona', 'url'=>array('index')),
	array('label'=>'Create BasZona', 'url'=>array('create')),
	array('label'=>'View BasZona', 'url'=>array('view', 'id'=>$model->id_zona)),
	array('label'=>'Manage BasZona', 'url'=>array('admin')),
);
?>

<h1>Update BasZona <?php echo $model->id_zona; ?></h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>
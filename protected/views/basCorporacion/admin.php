

<h1>Corporaciones</h1>
<a class="btn btn-primary" href="index.php?r=basCorporacion/create">
	<i class="fa fa-plus"></i>&nbsp Nuevo Corporacion
</a>

<?php $this->widget('zii.widgets.grid.CGridView', array(
	'id'=>'bas-corporacion-grid',
	'pager'=>array("htmlOptions"=>array("class"=>"pagination")), 
	'emptyText'=>"No existen resultados en esta búsqueda", 
	'summaryText'=>'{start}-{end} de {count} Corporaciones',
	'dataProvider'=>$model->search(),
	'filter'=>$model,
	'columns'=>array(
		'corporacion',
		'descripcion',
		array(
			'class'=>'CButtonColumn',
			'template'=>'{update}{delete}',
			'deleteConfirmation'=>('¿Desea borrar el registro?'),
		),
	),
)); ?>

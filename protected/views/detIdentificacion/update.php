<?php
/* @var $this DetIdentificacionController */
/* @var $model DetIdentificacion */

$this->breadcrumbs=array(
	'Det Identificacions'=>array('index'),
	$model->id_identificaciones=>array('view','id'=>$model->id_identificaciones),
	'Update',
);

$this->menu=array(
	array('label'=>'List DetIdentificacion', 'url'=>array('index')),
	array('label'=>'Create DetIdentificacion', 'url'=>array('create')),
	array('label'=>'View DetIdentificacion', 'url'=>array('view', 'id'=>$model->id_identificaciones)),
	array('label'=>'Manage DetIdentificacion', 'url'=>array('admin')),
);
?>

<h1>Update DetIdentificacion <?php echo $model->id_identificaciones; ?></h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>
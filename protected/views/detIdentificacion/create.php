<?php
/* @var $this DetIdentificacionController */
/* @var $model DetIdentificacion */

$this->breadcrumbs=array(
	'Det Identificacions'=>array('index'),
	'Create',
);

$this->menu=array(
	array('label'=>'List DetIdentificacion', 'url'=>array('index')),
	array('label'=>'Manage DetIdentificacion', 'url'=>array('admin')),
);
?>

<h1>Create DetIdentificacion</h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>
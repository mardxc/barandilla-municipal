<?php
/* @var $this DetObjetosPortaController */
/* @var $model DetObjetosPorta */

$this->breadcrumbs=array(
	'Det Objetos Portas'=>array('index'),
	$model->id_objetos_porta=>array('view','id'=>$model->id_objetos_porta),
	'Update',
);

$this->menu=array(
	array('label'=>'List DetObjetosPorta', 'url'=>array('index')),
	array('label'=>'Create DetObjetosPorta', 'url'=>array('create')),
	array('label'=>'View DetObjetosPorta', 'url'=>array('view', 'id'=>$model->id_objetos_porta)),
	array('label'=>'Manage DetObjetosPorta', 'url'=>array('admin')),
);
?>

<h1>Update DetObjetosPorta <?php echo $model->id_objetos_porta; ?></h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>
<?php
/* @var $this DetObjetosPortaController */
/* @var $model DetObjetosPorta */

$this->breadcrumbs=array(
	'Det Objetos Portas'=>array('index'),
	'Create',
);

$this->menu=array(
	array('label'=>'List DetObjetosPorta', 'url'=>array('index')),
	array('label'=>'Manage DetObjetosPorta', 'url'=>array('admin')),
);
?>

<h1>Create DetObjetosPorta</h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>
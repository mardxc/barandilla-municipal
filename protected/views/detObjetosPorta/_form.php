<?php
/* @var $this DetObjetosPortaController */
/* @var $model DetObjetosPorta */
/* @var $form CActiveForm */
?>

<div class="form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'det-objetos-porta-form',
	'enableAjaxValidation'=>false,
)); ?>

	<p class="note">Fields with <span class="required">*</span> are required.</p>

	<?php echo $form->errorSummary($model); ?>

	<div class="row">
		<?php echo $form->labelEx($model,'objetos'); ?>
		<?php echo $form->textField($model,'objetos',array('size'=>30,'maxlength'=>30)); ?>
		<?php echo $form->error($model,'objetos'); ?>
	</div>

	<div class="row buttons">
		<?php echo CHtml::submitButton($model->isNewRecord ? 'Create' : 'Save'); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- form -->
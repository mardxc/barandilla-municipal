
<h1>Manage Bas Barrios</h1>


<a class="btn btn-primary" href="index.php?r=basBarrios/create">
	<i class="fa fa-plus"></i>&nbsp Nuevo Barrio
</a>



<?php $this->widget('zii.widgets.grid.CGridView', array(
	'id'=>'bas-barrios-grid',
	'pager'=>array("htmlOptions"=>array("class"=>"pagination")), 
	'emptyText'=>"No existen resultados en esta búsqueda", 
	'summaryText'=>'{start}-{end} de {count} Barrios',
	'dataProvider'=>$model->search(),
	'filter'=>$model,
	'columns'=>array(
		'barrio',
		'colonia',
		'id_municipio',
		array(
			'class'=>'CButtonColumn',
		),
	),
)); ?>

<?php 
	$id_persona='';
	$nombre='';
	$ape_pat='';
	$ape_mat='';
	$edad='';
	$sexo='';
	$ocupacion='';
	$estado_civil='';
	$telefono='';
	$colonia='';
	$calle='';
	$id_zona='';
	$entre_calle='';
	$y_la_calle='';
	$a_la_altura='';
	$corporacion='';
	$sector='';
	$falta='';
	$lugar_det='';
	$billetes='';
	$monedas='';
	$dolares='';
	$otros='';
 ?>
 <style>
 	thead tr th{
 		font-size: 1.5em;
 		text-align: center;
 	}
 </style>
 <script>
 	function openSearch(){
 		$('#dialogGif').dialog('open');

 		<?php echo CHtml::ajax(array(
 			'url'=>array('detDetenciones/BuscarDet'),
 			'type'=>'post',
 			'data'=>'',
 			'dataType'=>'json',
 			'success'=>"function(data)
 			{
 				$('#id_persona').val(data.id_persona);
 				$('#nombre').val(data.nombre);
 				$('#ape_pat').val(data.ape_pat);
 				$('#ape_mat').val(data.ape_mat);
 				$('#edad').val(data.edad);
 				$('#sexo').val(data.sexo);
 				$('#estado_civil').val(data.estado_civil);
 				$('#telefono').val(data.telefono);
 				$('#calle').val(data.calle);				
				
 				alert('INDIVIDUO ENCONTRADO');
 				$('#dialogGif').dialog('close');
 			}"
 		))
 		?>;	
 	}		
 </script>
<div class="jumbotron">
	<div class="container" style="text-align: center;">
		<h2>DIRECCIÓN DE SEGURIDAD PÚBLICA MUNICIPAL DE RIOVERDE, S.L.P.</h2>
		<h3>CONTROL DE DETENIDOS MAYORES DE EDAD</h3>
	</div>
</div>
<?php echo CHtml::textField('id_persona',$id_persona,array('style'=>'display:none;'));?>
<table class="table table-striped table-bordered">
	<thead>
		<tr>
			<th colspan="4">DATOS DEL INFRACTOR</th>
		</tr>
	</thead>
	<tr>
		<th colspan="3" style="text-align: center;">FOTOGRAFIA</th>
		<th>IDENTIFICAR INFRACTOR</th>
	</tr>
	<tr>
		<td colspan="3" style="text-align: center;">
			<table width="100%" height="144">
				<tr>
					<td width="100" valign=top>
						<form>

							<input  type=button  value="Configuracion" onClick="webcam.configure()" class="btn btn-primary">
								<br><br>
							<input type=button value="Tomar Fotografía" onClick="webcam.freeze()" class="btn btn-primary">
								<br><br>
							<input type=button value="Volver a tomar" onClick="webcam.reset()" class="btn btn-primary">
				   		</form>	
					</td>
			   		 <td width="263" valign=top>
						<script>
							document.write( webcam.get_html(320, 240) );//dimensiones de la camara
						</script>
			    	</td>   		 
			   		 <td style="display: none;" width=411>
				    	<div id="upload_results" class="formulario" > </div>
			  		</td>
				</tr>
			</table>
		</td>
		<td>
			<button class=" btn btn-3d btn-primary" onclick="openSearch()" value="Imprimir">
	                    <span class="fa fa-search"></span> Buscar
	                  </button>
		</td>
	</tr>
	<tr>
		<th colspan="2">NOMBRE DEL INFRACTOR</th>
		<th>APELLIDO PATERNO</th>
		<th>APELLIDO MATERNO</th>
	</tr>
	<tr>
		<td colspan="2">
			<?php echo CHtml::textField('nombre',
				$nombre,
				array('onkeypress'=>'letras("nombre")'));?>
				<a  onclick="mensajeAdvertencia('nombre',
				'Ingresar el nombre. <b>Obligatorio</b>')"
				 id="nombre" class="glyphicon glyphicon-info-sign" style="font-size: 1.2em;
				 cursor:hand;"></a>
		</td>
		<td>
			<?php echo CHtml::textField('ape_pat',
				$ape_pat,
				array('onkeypress'=>'letras("ape_pat")'));?>
				<a  onclick="mensajeAdvertencia('ape_pat',
				'Ingresar el apellido paterno. <b>Obligatorio</b>')"
				 id="ape_pat" class="glyphicon glyphicon-info-sign" style="font-size: 1.2em;
				 cursor:hand;"></a>
		</td>
		<td>
			<?php echo CHtml::textField('ape_mat',
				$ape_mat,
				array('onkeypress'=>'letras("ape_mat")'));?>
				<a  onclick="mensajeAdvertencia('ape_mat',
				'Ingresar el apellido materno. <b>Obligatorio</b>')"
				 id="ape_mat" class="glyphicon glyphicon-info-sign" style="font-size: 1.2em;
				 cursor:hand;"></a>
		</td>
	</tr>
	<tr>
		<th>EDAD</th>
		<th>SEXO</th>
		<th colspan="2">OCUPACION</th>
	</tr>
	<tr>
		<td>
			<input type="number" id="edad" value="<?php echo $edad; ?>">
			<a  onclick="mensajeAdvertencia('mensajeEdad',
			'Ingresar la edad del detenido. <b>Obligatorio</b>')"
			 id="mensajeEdad" class="glyphicon glyphicon-info-sign" style="font-size: 1.2em;
			 cursor:hand;"></a>
		</td>
		<td>
			<?php 
				echo CHtml::dropDownList(
					'sexo',
					$sexo,
				array('empty' => '',
					'HOMBRE'=>'HOMBRE',
					'MUJER'=>'MUJER'
				)
			 );
			?>
			<a  onclick="mensajeAdvertencia('mensajeSexo',
			'Selecciona el sexo del detenido. <b>Obligatorio</b>')"
			 id="mensajeSexo" class="glyphicon glyphicon-info-sign" style="font-size: 1.2em;
			 cursor:hand;"></a>
		</td>
		<td colspan="2">
			<?php echo CHtml::textField('ocupacion',
				$ocupacion,
				array('onkeypress'=>'letras("ocupacion")'));?>
				<a  onclick="mensajeAdvertencia('mensajeOcupacion',
				'Ingresar la ocupacion del detenido. <b>Obligatorio</b>')"
				 id="mensajeOcupacion" class="glyphicon glyphicon-info-sign" style="font-size: 1.2em;
				 cursor:hand;"></a>
		</td>
	</tr>
	<tr>
		<th>ESTADO CIVIL</th>
		<th>TELEFONO</th>
		<th>CALLE</th>
		<th>COLONIA</th>
	</tr>
	<tr>
		<td>
			<?php 
				echo CHtml::dropDownList(
					'estado_civil',
					$estado_civil,
				array('empty' => '',
					'SOLTERO/A'=>'SOLTERO/A',
					'COMPROMETIDO/A'=>'COMPROMETIDO/A',
					'CASADO/A'=>'CASADO/A',
					'DIVORCIADO/A'=>'DIVORCIADO/A',
					'VIUDO/A'=>'VIUDO/A'
				)
			 );
			?>
				<a  onclick="mensajeAdvertencia('mensajeCivil',
				'Ingresar el estado civil del detenido. <b>Obligatorio</b>')"
				 id="mensajeCivil" class="glyphicon glyphicon-info-sign" style="font-size: 1.2em;
				 cursor:hand;"></a>
		</td>
		<td>
			<?php echo CHtml::textField('telefono',
				$telefono,
				array('onkeypress'=>'numeros("telefono")'));?>
				<a  onclick="mensajeAdvertencia('telefono',
				'Ingresar el Telefono. <b>Obligatorio</b>')"
				 id="telefono" class="glyphicon glyphicon-info-sign" style="font-size: 1.2em;
				 cursor:hand;"></a>
		</td>
		<td>
			<?php echo CHtml::textField('calle',
				$calle,
				array('onkeypress'=>'vacios("calle")'));?>
				<a  onclick="mensajeAdvertencia('mensajecalle',
				'Ingresar la calle donde se realizo la aprension. <b>Obligatorio</b>')"
				 id="mensajecalle" class="glyphicon glyphicon-info-sign" style="font-size: 1.2em;
				 cursor:hand;"></a>
		</td>
		<td>
			<input type="text" style="display:none;" id="id_colonia">
			<?php 
				$this->widget('zii.widgets.jui.CJuiAutocomplete', array(
					'id'=>'colonia',
					'name'=>'colonia',
					'source'=>$this->createUrl('DetDetenciones/listadoColonia'),
					'options'=>array(
						'delay'=>300,
						'minLength'=>1,
						'showAnim'=>'fold',
						'select'=>"js:function(event,ui){
							$('#colonia').val(ui.item.id);								
						}"
						),
						'htmlOptions'=>array(
							'style'=>'margin-top:10px;'
							),
							
						)); 
				?> 
		</td>
	</tr>
	<thead>
		<tr>
			<th colspan="4">DATOS DE LA DETENCION</th>
		</tr>
	</thead>
	<tr>
		<th>CORPORACION</th>
		<th>ZONA</th>
		<th>MOTIVOS</th>
		<th>CALLE (DETENCION)</th>
	</tr>
	<tr>
		<td>
			<?php 
				echo CHtml::dropDownList(
					'corporacion',
					$corporacion,
					BasCorporacion::getListCorporacion(),
					array('empty' => '','style'=>'width:80%')
				); 
			?>
			<a  onclick="mensajeAdvertencia('mensajeCorporacion',
			'Selecciona la corporacion actual. <b>Obligatorio</b>')"
			 id="mensajeCorporacion" class="glyphicon glyphicon-info-sign" style="font-size: 1.2em;
			 cursor:hand;"></a>
		</td>
		<td>
			<?php 
				echo CHtml::dropDownList(
					'zona',
					$id_zona,
					BasZona::getListZona(),
					array('empty' => ' ','style'=>'width:80%')
				); 
			?>
			<a  onclick="mensajeAdvertencia('mensajeZona',
			'Selecciona la corporacion actual. <b>Obligatorio</b>')"
			 id="mensajeZona" class="glyphicon glyphicon-info-sign" style="font-size: 1.2em;
			 cursor:hand;"></a>
		</td>
		<td>
			<?php 
				echo CHtml::dropDownList(
					'motivos',
					'',
					BasMotivos::getListMotivos(),
					array('empty' => ' ','style'=>'width:50%')
				); 
			?>
			<a class="fa fa-plus btn btn-sm" onclick="addMotivos()"></a>
			<a  onclick="mensajeAdvertencia('mensajeMotivos',
			'Selecciona el motivo de detencion y a continuacion presione el boton +. <b>Obligatorio</b>')"
			 id="mensajeMotivos" class="glyphicon glyphicon-info-sign" style="font-size: 1.2em;
			 cursor:hand;"></a>
			 <br>
			<?php $this->widget('zii.widgets.grid.CGridView', array(
				'id'=>'motivo-grid',
				'itemsCssClass'=>"table table-stripped table-hover table-bordered",
				'pager'=>array("htmlOptions"=>array("class"=>"pagination")),
				'summaryText'=>'',
				'dataProvider'=>$dataProMotivo,
				//'filter'=>$model,
				'columns'=>array(
					'motivo'
				),
			)); ?>
		</td>
		<td>
			<?php echo CHtml::textField('calle',
				$calle,
				array('onkeypress'=>'vacios("calle")'));?>
				<a  onclick="mensajeAdvertencia('mensajeCalle',
				'Ingresar entre que calles se realizo la aprension. <b>Obligatorio</b>')"
				 id="mensajeCalle" class="glyphicon glyphicon-info-sign" style="font-size: 1.2em;
				 cursor:hand;"></a>
		</td>
	</tr>
	<tr>
		<th>ENTRE CALLE</th>
		<th>Y LA CALLE</th>
		<th>A LA ALTURA</th>
		<th>COLONIA</th>
	</tr>
	<tr>
		<td>
			<?php echo CHtml::textField('entre_calle',
				$entre_calle,
				array('onkeypress'=>'vacios("entre_calle")'));?>
				<a  onclick="mensajeAdvertencia('mensajeEntreCalle',
				'Ingresar entre que calles se realizo la aprension. <b>Obligatorio</b>')"
				 id="mensajeEntreCalle" class="glyphicon glyphicon-info-sign" style="font-size: 1.2em;
				 cursor:hand;"></a>
		</td>
		<td>
			<?php echo CHtml::textField('y_la_calle',
				$y_la_calle,
				array('onkeypress'=>'vacios("y_la_calle")'));?>
				<a  onclick="mensajeAdvertencia('mensajeYCalle',
				'Ingresar entre que calles se realizo la aprension. <b>Obligatorio</b>')"
				 id="mensajeYCalle" class="glyphicon glyphicon-info-sign" style="font-size: 1.2em;
				 cursor:hand;"></a>
		</td>
		<td>
			<?php echo CHtml::textField('a_la_altura',
				$a_la_altura,
				array('onkeypress'=>'vacios("a_la_altura")'));?>
				<a  onclick="mensajeAdvertencia('mensajeAltura',
				'Ingresar la calle donde se realizo la aprension. <b>Obligatorio</b>')"
				 id="mensajeAltura" class="glyphicon glyphicon-info-sign" style="font-size: 1.2em;
				 cursor:hand;"></a>
		</td>
		<td>
			<input type="text" style="display:none;" id="id_coloniaAseguramiento">
			<?php 
				$this->widget('zii.widgets.jui.CJuiAutocomplete', array(
					'id'=>'coloniaAseguramiento',
					'name'=>'coloniaAseguramiento',
					'source'=>$this->createUrl('DetDetenciones/listadoColonia'),
					'options'=>array(
						'delay'=>300,
						'minLength'=>1,
						'showAnim'=>'fold',
						'select'=>"js:function(event,ui){
							$('#colonia').val(ui.item.id);								
						}"
						),
						'htmlOptions'=>array(
							'style'=>'margin-top:10px;'
							),
							
						)); 
				?> 
		</td>
	</tr>
</table>



<?php if(!isset($detencion->id_detencion)) { ?>
	<button id="guardarDetenido" class="btn btn-success" value="guardar" onclick="guardarDetenido()">Guardar</button>
<?php } else { ?>
	<button id="guardarDetenido" class="btn btn-success" value="actualizar" onclick="guardarDetenido()">Actualizar</button>
<?php } ?>

<?php 
	$this->beginWidget('zii.widgets.jui.CJuiDialog', array(
		'id'=>'dialogGif',
		'options'=>array(
			'title'=>'BUSCANDO DETENIDO',
			'autoOpen'=>false,
			'modal'=>true,
			'width'=>450,
			'height'=>450,
			'resizable'=>false,
			'position'=>'{my:"bottom", at: "center", of:button}',
		),
	));	
?>
<img src="<?php echo Yii::app()->baseUrl.'/images/load.gif';?>" class="img-responsive">


<?php $this->endWidget();?>
<?php
/* @var $this DetDetencionesController */
/* @var $model DetDetenciones */
/* @var $form CActiveForm */
?>

<div class="form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'det-detenciones-form',
	'enableAjaxValidation'=>false,
)); ?>

	<p class="note">Fields with <span class="required">*</span> are required.</p>

	<?php echo $form->errorSummary($model); ?>

	<div class="row">
		<?php echo $form->labelEx($model,'fecha'); ?>
		<?php echo $form->textField($model,'fecha'); ?>
		<?php echo $form->error($model,'fecha'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'modus_operandi'); ?>
		<?php echo $form->textField($model,'modus_operandi',array('size'=>30,'maxlength'=>30)); ?>
		<?php echo $form->error($model,'modus_operandi'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'bienes_afectados'); ?>
		<?php echo $form->textField($model,'bienes_afectados',array('size'=>30,'maxlength'=>30)); ?>
		<?php echo $form->error($model,'bienes_afectados'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'monto_aproximado_afectacion'); ?>
		<?php echo $form->textField($model,'monto_aproximado_afectacion',array('size'=>8,'maxlength'=>8)); ?>
		<?php echo $form->error($model,'monto_aproximado_afectacion'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'detenido_en'); ?>
		<?php echo $form->textField($model,'detenido_en',array('size'=>30,'maxlength'=>30)); ?>
		<?php echo $form->error($model,'detenido_en'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'folio'); ?>
		<?php echo $form->textField($model,'folio',array('size'=>30,'maxlength'=>30)); ?>
		<?php echo $form->error($model,'folio'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'ocupacion'); ?>
		<?php echo $form->textField($model,'ocupacion',array('size'=>20,'maxlength'=>20)); ?>
		<?php echo $form->error($model,'ocupacion'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'escolaridad'); ?>
		<?php echo $form->textField($model,'escolaridad',array('size'=>20,'maxlength'=>20)); ?>
		<?php echo $form->error($model,'escolaridad'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'id_condicion'); ?>
		<?php echo $form->textField($model,'id_condicion'); ?>
		<?php echo $form->error($model,'id_condicion'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'id_identificacion'); ?>
		<?php echo $form->textField($model,'id_identificacion'); ?>
		<?php echo $form->error($model,'id_identificacion'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'id_banda'); ?>
		<?php echo $form->textField($model,'id_banda'); ?>
		<?php echo $form->error($model,'id_banda'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'id_persona'); ?>
		<?php echo $form->textField($model,'id_persona'); ?>
		<?php echo $form->error($model,'id_persona'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'id_dinero'); ?>
		<?php echo $form->textField($model,'id_dinero'); ?>
		<?php echo $form->error($model,'id_dinero'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'id_objetos_porta'); ?>
		<?php echo $form->textField($model,'id_objetos_porta'); ?>
		<?php echo $form->error($model,'id_objetos_porta'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'id_canalizacion'); ?>
		<?php echo $form->textField($model,'id_canalizacion'); ?>
		<?php echo $form->error($model,'id_canalizacion'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'id_municipio'); ?>
		<?php echo $form->textField($model,'id_municipio'); ?>
		<?php echo $form->error($model,'id_municipio'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'id_corporacion'); ?>
		<?php echo $form->textField($model,'id_corporacion'); ?>
		<?php echo $form->error($model,'id_corporacion'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'id_lugar'); ?>
		<?php echo $form->textField($model,'id_lugar'); ?>
		<?php echo $form->error($model,'id_lugar'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'id_zona'); ?>
		<?php echo $form->textField($model,'id_zona'); ?>
		<?php echo $form->error($model,'id_zona'); ?>
	</div>

	<div class="row buttons">
		<?php echo CHtml::submitButton($model->isNewRecord ? 'Create' : 'Save'); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- form -->



<div class="table-responsive">
	<table class="table table-hover">
		<tr>
		<td>Nombres</td>
			<td>
				<input type="text" name="">
			</td>
			<td>Apellido Paterno</td>
			<td>
				<input type="text" name="">
			</td>
			
		</tr>
		<tr>
			<td>Apellido Materno</td>
			<td>
				<input type="text" name="">
			</td>
			<td>Documento de Identificacion</td>
			<td>
				<select>
					<option>SELECCIONE</option>
					<option>INE</option>
					<option>Cartilla</option>
				</select>
			</td>
		</tr>
		<tr>
			<td>Fecha de nacimiento</td>
			<td>
				<input type="date" name="">
			</td>
			<td>Nacionalidad</td>
			<td>
				<select>
					<option>SELECCIONE</option>
					<option>MEXICANA</option>
					<option>AMERICANA</option>
				</select>
			</td>
		</tr>
		<tr>
			<td>Alias</td>		
			<td>
				<input type="text" name="">
			</td>
			<td>Estado Civil</td>		
			<td>
				<select>
					<option>SELECCIONE</option>
					<option>SOLTERO</option>
					<option>CASADO</option>
					<option>DIVORCIADO</option>
					<option>VIUDO</option>
				</select>
			</td>
		</tr>
		<tr>
			<td>Sexo</td>
			<td>
				<input type="checkbox" name="">M
				<input type="checkbox" name="">F
			</td>
			<td>Escolaridad</td>
			<td>
				<select>
					<option>SELECCIONE</option>
				</select>
			</td>
		</tr>
		<tr>
			<td>Edad</td>
			<td>
				<input type="text" name="">
			</td>
			<td>Ocupacion</td>
			<td>
				<input type="text" name="">
			</td>
		</tr>
	</table>
</div>

<div class="table-responsive">
<h1>Caracteristicas del Detenido</h1>
<br>
	<table class="table table-hover">
	<table class="table-responsive">
		<td>
					TATUAJES, <br>
					MARCAS FÍSICAS <br>
					O SEÑAS
				</td>
				<td>
					<textarea id="txtTatto" rows="5" cols="80"></textarea>
				</td>
	</table>
	<table>
	<tr>
				<td>
					<input type="checkbox" id="check">Normal
				</td>
				<td>
					<input type="checkbox" id="check">Estado de Ebriedad
				</td>
				<td>
					<input type="checkbox" id="check">Drogrado/Intoxicado
				</td>
			</tr>
			<tr>
				
				<td>
					Otro<input type="text" id="name" rows="20">
				</td>
			</tr>
			<tr>
				
				<td>
					Color de Piel
				</td>
				<td>
					<select>
					<option>NEGRO</option>
					</select>
				</td>
			</tr>
			<tr>
				<td>
					Cejas
				</td>
				<td>
					<select>
					<option>UNICEJA</option>
					</select>
				</td>
				<td>
					Color de Ojos
				</td>
				<td>
					<select>
					<option>NEGRO</option>
					</select>
				</td>
			</tr>
			<tr>
				<td>
					Color de Cabello
				</td>
				<td>
					<select>
					<option>NEGRO</option>
					</select>
				</td>
				<td>
					Mentón
				</td>	
				<td>
					<select>
					<option>OVAL</option>
					</select>
				</td>		
			</tr>
			<tr>
				<td>
					Frente
				</td>
				<td>
					<select>
					<option>PEQUEÑA</option>
					</select>
				</td>
				<td>
					Complexión
				</td>
				<td>
					<select>
					<option>ROBUSTA</option>
					</select>
				</td>
			</tr>
		
	</table>
			
	</table>
</div>


<div class="table-responsive">
<h1>Motivos del Aseguramiento</h1>
<br>
<table class="table-responsive">
<tr>
			<tr>
			<td>Motivos</td>
			<td>
			<textarea  rows="5" cols="80"></textarea>			
			</td>
		</tr>
	
</table>
	<table class="table table-hover">
	
		<tr>
			<td>Banda a la que pertenece</td>
			<td>
				<select>
					<option>SELECCIONE</option>
				</select>
				<a class="btn btn-primary" data-toggle="modal" href='#newBanda'><i class="fa fa-plus"></i></a>
			</td>
			<td>Colonia</td>
			<td>
				<input type="text" name="">
			</td>
		
		<tr>
			<td colspan="4">
			<center>
				Lugar del aseguramiento
			</center>
			</td>
		</tr>
		
			<td>Calle</td>
			<td>
				<input type="text" name="">
			</td>
			<td>A la altura de</td>
			<td>
				<input type="text" name="">
			</td>
		</tr>

		
		<tr>
		
			<td>Entre Calle</td>
			<td>
				<input type="text" name="">
			</td>
			<td>Y Calle</td>
			<td>
				<input type="text" name="">
			</td>
		</tr>

	</table>
	<table class="table-responsive">
		<tr>
			<td>Objetos <br>
			Asegurados</td>
			<td>
			<textarea  rows="5" cols="80"></textarea>			
			</td>
		</tr>
	</table>
</div>

<div>
	<table class="table-responsive">
		<tr>
			<th>Objetos que porta</th>
			<td></td>
			
		</tr>
		<tr>
			<td>
				<input type="checkbox" id="check">Camisa
			</td>
			<td>
				<input type="checkbox" id="check">Tenis
			</td>
			
		</tr>
		<tr>
			<td>
				<input type="checkbox" id="check">Pantalón	
			</td>
			<td>
				<input type="checkbox" id="check">Zapatos
			</td>
					
		</tr>
		<tr>
			<td>
				<input type="checkbox" id="check">Playera	
			</td>
			<td>
				<input type="checkbox" id="check">Botas
			</td>
			
		</tr>
		<tr>
			<td>
				<input type="checkbox" id="check">Chamarra	
			</td>
			<td>
				<input type="checkbox" id="check">Sombrero
			</td>
			
		</tr>
		<tr>
			
			<td>
				<input type="checkbox" id="check">Cadena	
			</td>
			
		</tr>
		<tr>
			<td>
				<input type="checkbox" id="check">Pulsera	
			</td>
			<td>
				<input type="checkbox" id="check">Cartera
			</td>
			
		</tr>
		<tr>
			<td>
				<input type="checkbox" id="check">Gorra
			</td>
			<td>
				<input type="checkbox" id="check">Lentes
			</td>
		</tr>
		<tr>
			<td>
				<input type="checkbox" id="check">Cinturón	
			</td>
			<td>
				<input type="checkbox" id="check">Otros
			</td>
		</tr>
		<table class="table-responsive">
			<tr>
			<td>
				Dinero
				<br>
				<input type="checkbox" id="check">billetes
				<input type="checkbox" id="check">Monedas	
				<input type="checkbox" id="check">dolares
				<input type="checkbox" id="check">Otros	

			</td>
		</tr>
		<tr>
		<td>
			total: $<input type="text" id="cantidad"> mxn.
		</td>
			

		</tr>
					
		</table>
	</table>
</div>
<div>
	<tr>
		<td align="center" colspan="2">
			<?php if(!isset($model->id_persona)) { ?>
				<button id="guardar" class="btn btn-success" value="guardar" onclick="guardarpersona()">Guardar</button>
			<?php } else { ?>
				<button id="guardar" class="btn btn-success" value="actualizar" onclick="guardarpersona()">Actualizar</button>
			<?php } ?>
		</td>
	</tr>
</div>

<div class="modal fade" id="newBanda">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
				<h4 class="modal-title">Agregar Banda</h4>
			</div>
			<div class="modal-body">
				
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
				<button type="button" class="btn btn-primary">Save changes</button>
			</div>
		</div>
	</div>
</div>
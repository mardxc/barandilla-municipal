<script>
	function guardarDetenido(){
				
		var guarda=$('#guardarDetenido').val();
		var id_detencion=$('#id_detencion').val();
		var id_persona=$('#id_persona').val();

		var fecha=$('#fecha').text() ;
		var modus_operandi=$('#modus_operandi').val() ;
		var bienes_afectados=$('#bienes_afectados').val() ;
		var monto_aproximado_afectacion=$('#monto_aproximado_afectacion').val() ;
		var detenido_en=$('#detenido_en').val();
		var ocupacion=$('#ocupacion').val();
		var id_condicion=$('#detCondicionesFisicas').val();
		var id_identificacion=$('#identificacion').val();
		var id_banda=$('#detBanda').val();
		// Datos para id_persona
			var nombre=$('#nombre').val();
			var ape_pat=$('#ape_pat').val();
			var ape_mat=$('#ape_mat').val();
			//var telefono=$('#telefono').val();
			var sexo=$('#sexo').val();
			var fecha_nacimiento=$('#fecha_nacimiento').val();
			var estado_civil=$('#estado_civil').val();
			var nacionalidad=$('#nacionalidad').val();
			var señas_particulares=$('#señas_particulares').val();
			var lesiones_presenta=$('#lesiones_presenta').val();
			var id_escolaridad=$('#escolaridad').val();
			var edad=$('#edad').val();
		// End id_persona
		//var id_dinero=$('#id_dinero').val();
		var id_objetos_porta=$('#id_objetos_porta').val();
		var id_canalizacion=$('#id_canalizacion').val();
		var id_municipio=$('#municipio').val();
		var id_corporacion=$('#corporacion').val();
		// Datos para id_lugar
			var id_lugar=$('#id_lugar').val();
			var colonia=$('#colonia').val();
			var calle=$('#calle').val();
			var entre_calle=$('#entre_calle').val();
			var y_la_calle=$('#y_la_calle').val();
			var a_la_altura=$('#a_la_altura').val();
		// End
		var id_zona=$('#zona').val();
		var hora=$('#hora').text();

		var id_descripcion=$('#id_descripcion').val();
		var estatura=$('#estatura').val();
		var color_piel=$('#color_piel').val();
		var ceja=$('#cejas').val();
		var color_ojos=$('#color_ojos').val();
		var color_pelo=$('#color_pelo').val();
		var menton=$('#menton').val();
		var frente=$('#frente').val();
		var complexion=$('#complexion').val();

		var id_domicilio=$('#id_domicilio').val();
		var domCalle=$('#domCalle').val();
		var domColonia=$('#domColonia').val();
		var domMunicipio=$('#domMunicipio').val();

		var id_objetos_porta=$('#id_objetos_porta').val();
		var camisa=$('#camisa').prop('checked');
		var tenis=$('#tenis').prop('checked');
		var pulsera=$('#pulsera').prop('checked');
		var cartera=$('#cartera').prop('checked');
		var pantalon=$('#pantalon').prop('checked');
		var zapatos=$('#zapatos').prop('checked');
		var gorra=$('#gorra').prop('checked');
		var lentes=$('#lentes').prop('checked');
		var playera=$('#playera').prop('checked');
		var botas=$('#botas').prop('checked');
		var cinturon=$('#cinturon').prop('checked');
		var sombrero=$('#sombrero').prop('checked');
		var chamarra=$('#chamarra').prop('checked');
		var otros=$('#otros').prop('checked');
		var obDescripcion=$('#obDescripcion').val();

		var id_canalizacion=$('#id_canalizacion').val();
		var cadena=$('#cadena').prop('checked');
		var juez_calificador=$('#juez_calificador').prop('checked');
		var fuero_comun=$('#fuero_comun').prop('checked');
		var fuero_federal=$('#fuero_federal').prop('checked');
		var mixta=$('#mixta').prop('checked');
		var tutelar_menores=$('#tutelar_menores').prop('checked');
		var migracion=$('#migracion').prop('checked');
		var canaOtros=$('#canaOtros').val();

		var ape_pat_afectado=$('#ape_pat_afectado').val();
		var ape_mat_afectado=$('#ape_mat_afectado').val();
		var nombre_afectado=$('#nombre_afectado').val();
		var sexoAfectado=$('#sexoAfectado').val();

		var id_persona_afectada=$('#id_persona_afectada').val();
		var id_domicilio_afectada=$('#id_domicilio_afectada').val();
		var calle_afectado=$('#calle_afectado').val();
		var municipioAfectado=$('#municipioAfectado').val();
		var EstadoAfectado=$('#EstadoAfectado').val();
			<?php echo CHtml::ajax(array(
			    'url'=>array('detDetenciones/guardarDetenido'),
			    'data'=> array(
			    	'id_detencion'=>'js:id_detencion',
			    	'id_persona'=>'js:id_persona',
			    	'fecha'=>'js:fecha',
			    	'modus_operandi'=>'js:modus_operandi',
			    	'bienes_afectados'=>'js:bienes_afectados',
			    	'monto_aproximado_afectacion'=>'js:monto_aproximado_afectacion',
			    	'detenido_en'=>'js:detenido_en',
			    	'ocupacion'=>'js:ocupacion',
			    	'id_condicion'=>'js:id_condicion',
			    	'id_identificacion'=>'js:id_identificacion',
			    	'id_banda'=>'js:id_banda',

			    	'nombre'=>'js:nombre',
			    	'ape_pat'=>'js:ape_pat',
			    	'ape_mat'=>'js:ape_mat',
			    	'sexo'=>'js:sexo',
			    	'fecha_nacimiento'=>'js:fecha_nacimiento',
			    	'estado_civil'=>'js:estado_civil',
			    	'nacionalidad'=>'js:nacionalidad',
			    	'señas_particulares'=>'js:señas_particulares',
			    	'lesiones_presenta'=>'js:lesiones_presenta',
			    	'id_escolaridad'=>'js:id_escolaridad',
			    	'edad'=>'js:edad',

			    	'id_municipio'=>'js:id_municipio',
			    	'id_corporacion'=>'js:id_corporacion',

					'id_lugar'=>'js:id_lugar',
			    	'colonia'=>'js:colonia',
			    	'calle'=>'js:calle',
			    	'entre_calle'=>'js:entre_calle',
			    	'y_la_calle'=>'js:y_la_calle',
			    	'a_la_altura'=>'js:a_la_altura',

			    	'id_zona'=>'js:id_zona',
			    	'hora'=>'js:hora',

					'id_descripcion'=>'js:id_descripcion',
			    	'estatura'=>'js:estatura',
			    	'color_piel'=>'js:color_piel',
			    	'ceja'=>'js:ceja',
			    	'color_ojos'=>'js:color_ojos',
			    	'color_pelo'=>'js:color_pelo',
			    	'menton'=>'js:menton',
			    	'frente'=>'js:frente',
			    	'complexion'=>'js:complexion',
			    	
			    	'id_domicilio'=>'js:id_domicilio',
			    	'domCalle'=>'js:domCalle',
			    	'domColonia'=>'js:domColonia',
			    	'domMunicipio'=>'js:domMunicipio',

					'id_objetos_porta'=>'js:id_objetos_porta',
			    	'camisa'=>'js:camisa',
			    	'tenis'=>'js:tenis',
			    	'pulsera'=>'js:pulsera',
			    	'cartera'=>'js:cartera',
			    	'pantalon'=>'js:pantalon',
			    	'zapatos'=>'js:zapatos',
			    	'gorra'=>'js:gorra',
			    	'lentes'=>'js:lentes',
			    	'playera'=>'js:playera',
			    	'botas'=>'js:botas',
			    	'cinturon'=>'js:cinturon',
			    	'sombrero'=>'js:sombrero',
			    	'chamarra'=>'js:chamarra',
			    	'otros'=>'js:otros',
			    	'obDescripcion'=>'js:obDescripcion',

			    	'id_canalizacion'=>'js:id_canalizacion',
			    	'cadena'=>'js:cadena',
			    	'juez_calificador'=>'js:juez_calificador',
			    	'fuero_comun'=>'js:fuero_comun',
			    	'fuero_federal'=>'js:fuero_federal',
			    	'mixta'=>'js:mixta',
			    	'tutelar_menores'=>'js:tutelar_menores',
			    	'migracion'=>'js:migracion',
			    	'canaOtros'=>'js:canaOtros',

			    	'ape_pat_afectado'=>'js:ape_pat_afectado',
			    	'ape_mat_afectado'=>'js:ape_mat_afectado',
			    	'nombre_afectado'=>'js:nombre_afectado',
			    	'sexoAfectado'=>'js:sexoAfectado',
			    	'id_persona_afectada'=>'js:id_persona_afectada',
			    	'id_domicilio_afectada'=>'js:id_domicilio_afectada',
			    	'calle_afectado'=>'js:calle_afectado',
			    	'municipioAfectado'=>'js:municipioAfectado',
			    	'EstadoAfectado'=>'js:EstadoAfectado',
			        'guarda'=>"js:guarda"
			    ),
			    'type'=>'post',
			    'dataType'=>'json',
			    'success'=>"function(data)
			    {	
			        $('#id_detencion').val(data.id_detencion);
					$('#id_persona').val(data.id_persona);
					
					alert('Informacion Guardada');
					window.location='index.php?r=detDetenciones/admin';
			    } "
			    ))
			?>;
			return false; 			
	};
</script>
<script>
	function openSearch(){
		$('#dialogGif').dialog('open');
		$( 'a.ui-dialog-titlebar-close' ).remove();
		<?php echo CHtml::ajax(array(
			'url'=>array('detDetenciones/BuscarDet'),
			'type'=>'post',
			'data'=>'',
			'dataType'=>'json',
			'success'=>"function(data)
			{
				$('#id_persona').val(data.id_persona);
				$('#nombre').val(data.nombre);
				$('#ape_pat').val(data.ape_pat);
				$('#ape_mat').val(data.ape_mat);
				$('#edad').val(data.edad);
				$('#sexo').val(data.sexo);
				$('#estado_civil').val(data.estado_civil);
				$('#domCalle').val(data.calle);	
				$('#domMunicipio').val(data.id_municipio);
				$('#id_domicilio').val(data.id_domicilio);
				$('#domColonia').val(data.id_colonia);
			
				alert('INDIVIDUO ENCONTRADO');
				$('#dialogGif').dialog('close');
			}"
		))
		?>;	
	}
	function addBanda(){
		$('#addBanda').dialog('open');
	}
</script>
<script >
	$(document).ready(function(){
		$('#corporacion').change(function(){
			$('#id_corporacion').val($("#corporacion option:selected").text());
		});
		$('#escolaridad').change(function(){
			$('#id_escolaridad').val($("#escolaridad option:selected").text());
		});

		$('#municipio').change(function(){
			$('#id_municipio').val($("#municipio option:selected").text());
		});
		$('#domMunicipio').change(function(){
			$('#id_DomMunicipio').val($("#domMunicipio option:selected").text());
		});
		$('#estado').change(function(){
			$('#id_estado').val($("#estado option:selected").text());
		});
		$('#zona').change(function(){
			$('#id_zona').val($("#zona option:selected").text());
		});
		$('#identificacion').change(function(){
			$('#id_identificacion').val($("#identificacion option:selected").text());
		});
		$('#detBanda').change(function(){
			$('#id_banda').val($("#detBanda option:selected").text());
		});
		$('#detCondicionesFisicas').change(function(){
			$('#id_condicion').val($("#detCondicionesFisicas option:selected").text());
		});
	});

	function fijarDatos() {
		var id_corporacion=$('#id_corporacion').val();
		var id_escolaridad=$('#id_escolaridad').val();
		var id_municipio=$('#id_municipio').val();
		var id_estado=$('#id_estado').val();

		$('#corporacion option').each(function(){
			if ($(this).text() == id_corporacion)
				$(this).attr('selected','selected');
		});
		$('#escolaridad option').each(function(){
			if ($(this).text() == id_escolaridad)
				$(this).attr('selected','selected');
		});
		$('#municipio option').each(function(){
			if ($(this).text() == id_municipio)
				$(this).attr('selected','selected');
		});
		$('#estado option').each(function(){
			if ($(this).text() == id_estado)
				$(this).attr('selected','selected');
		});

	}

	function non() {
		$('.non').addClass('viw');
		alert('asd');
	}
</script>
<script>
	function addAlias() {
		var id_persona = $('#id_persona').val();
		var alias = $('#alias').val();
		//var table = $('#tbAlias').DataTable();
		if (id_persona=='') {
			alert('PRIMERO DEBES GUARDAR LA DETENCION Y/O AÑADIR UNA PERSONA');
		}else{
			<?php echo CHtml::ajax(array(
				'url'=>array('DetAlias/AddAlias'),
				'data'=>array(
					'id_persona'=>'js:id_persona',
					'alias'=>'js:alias',
				),
				'type'=>'post',
				'dataType'=>'json',
				'success'=>"function(data)
				{
					alert('alias añadido');
					$.fn.yiiGridView.update('alias-grid');
				}"
			))
			?>;
		}
		return false;
	}
	function addMotivos() {
		var id_motivo = $('#motivos').val();
		var id_detencion = $('#id_detencion').val();
		//var table = $('#tbAlias').DataTable();
		if (id_detencion=='') {
			alert('PRIMERO DEBES GUARDAR LA DETENCION Y/O AÑADIR UNA PERSONA');
		}else{
			<?php echo CHtml::ajax(array(
				'url'=>array('DetMotivos/AddMotivo'),
				'data'=>array(
					'id_motivo'=>'js:id_motivo',
					'id_detencion'=>'js:id_detencion',
				),
				'type'=>'post',
				'dataType'=>'json',
				'success'=>"function(data)
				{
					alert('Motivo añadido');
					$.fn.yiiGridView.update('motivo-grid');
				}"
			))
			?>;	
		}
		return false;
	}
</script>
<style>
	.nn{
		visibility: hidden;
	}
	.nn{
		visibility: hidden;
	}
	.viw{
		color: red;
		visibility: visible;
	}
</style>
<?php
	if (isset($detencion->id_detencion)) {
		$id_detencion=$detencion->id_detencion;
		$id_corporacion=$detencion->id_corporacion;
		$id_zona=$detencion->id_zona;
		$id_banda=$detencion->id_banda;

		$detenido_en=$detencion->detenido_en;
		$id_persona=$detencion->id_persona;
		$id_lugar=$detencion->id_lugar;
		$ocupacion=$detencion->ocupacion;
		$id_identificacion=$detencion->id_identificacion;
		$id_condicion=$detencion->id_condicion;
		$id_objetos_porta=$detencion->id_objetos_porta;
		$id_canalizacion=$detencion->id_canalizacion;
		$fecha=$detencion->fecha;
		$hora=$detencion->hora;
		$id_municipio=$detencion->id_municipio;
		if($id_municipio!=''){
			$id_estado=GenEstado::getEstado($id_municipio);
		}else{
			$id_estado='';
		}
		$modus_operandi=$detencion->modus_operandi;
		$bienes_afectados=$detencion->bienes_afectados;
		$monto_aproximado_afectacion=$detencion->monto_aproximado_afectacion;
	}else{
		$id_detencion='';
		$id_corporacion='';
		$id_zona='';
		$id_banda='';
		$detenido_en='';
		$id_persona='';
		$id_lugar='';
		$ocupacion='';
		$id_identificacion='';
		$id_condicion='';
		$id_objetos_porta='';
		$id_canalizacion='';
		date_default_timezone_set("America/Mexico_City");
		$fecha=date('d/m/Y');
		$time=time();
		$hora=date("H:i", $time);
		$id_municipio='';
		$modus_operandi='';
		$bienes_afectados='';
		$monto_aproximado_afectacion='';
	}
	if (isset($detencion->id_persona)) {
		$ape_pat=$persona->ape_pat;
		$ape_mat=$persona->ape_mat;
		$nombre=$persona->nombre;
		$alias='';
		$sexo=$persona->sexo;
		$estado_civil=$persona->estado_civil;
		$edad=$persona->edad;
		$id_escolaridad=$persona->id_escolaridad;
		$fecha_nacimiento=$persona->fecha_nacimiento;
		$nacionalidad=$persona->nacionalidad;
		$señas_particulares=$persona->señas_particulares;
		$lesiones_presenta=$persona->lesiones_presenta;
	}else{
		$ape_pat='';
		$ape_mat='';
		$nombre='';
		$alias='';
		$sexo='';
		$estado_civil='';
		$edad='';
		$id_escolaridad='';
		$fecha_nacimiento='';
		$nacionalidad='';
		$señas_particulares='';
		$lesiones_presenta='';
	}
	if (isset($detMotivos->id_det_motivos)) {
		$id_det_motivos=$detMotivos->id_det_motivos;
	}else{
		$id_det_motivos='';
		$motivos='';
	}
	if (isset($detencion->id_lugar)) {
		$colonia=$lugar->id_colonia;
		$calle=$lugar->calle;
		$num_int=$lugar->num_int;
		$num_ext=$lugar->num_ext;
		$entre_calle=$lugar->entre_calle;
		$y_la_calle=$lugar->y_la_calle;
		$a_la_altura=$lugar->a_la_altura;
	}else{
		$colonia='';
		$calle='';
		$num_int='';
		$num_ext='';
		$entre_calle='';
		$y_la_calle='';
		$a_la_altura='';
	}
	if (isset($persona->id_persona)) {
		$id_domicilio=$domicilio->id_domicilio;
		$domCalle=$domicilio->calle;
		$domColonia=$domicilio->id_colonia;
		$id_DomMunicipio=$domicilio->id_municipio;
		if($id_DomMunicipio!=''){
			$id_domEstado=GenEstado::getEstado($id_DomMunicipio);
		}else{
			$id_domEstado='';
		}
	}else{
		$id_domicilio='';
		$domCalle='';
		$domColonia='';
		$id_DomMunicipio='';
		$id_estado='';
	}
	if (isset($persona->id_persona)) {
		$id_descripcion=$descripcion->id_descripcion;
		$estatura=$descripcion->estatura;
		$color_piel=$descripcion->color_piel;
		$cejas=$descripcion->cejas;
		$color_ojos=$descripcion->color_ojos;
		$color_pelo=$descripcion->color_pelo;
		$menton=$descripcion->menton;
		$frente=$descripcion->frente;
		$complexion=$descripcion->complexion;
	}else{
		$id_descripcion='';
		$estatura='';
		$color_piel='';
		$cejas='';
		$color_ojos='';
		$color_pelo='';
		$menton='';
		$frente='';
		$complexion='';
	}
	if(isset($afectado->id_det_afectado)){
		$id_det_afectado=$afectado->id_det_afectado;
		if (isset($afectado->id_persona)) {
			/*$id_persona_afectada=$afectado->id_persona;
			$ape_pat_afectado=$personaAfectada->ape_pat;
			$ape_mat_afectado=$personaAfectada->ape_mat;
			$nombre_afectado=$personaAfectada->nombre;
			$sexoAfectado=$personaAfectada->sexo;*/
			$id_persona_afectada='';
			$ape_pat_afectado='';
			$ape_mat_afectado='';
			$nombre_afectado='';
			$sexoAfectado='';
		}else{
			$id_persona_afectada='';
			$ape_pat_afectado='';
			$ape_mat_afectado='';
			$nombre_afectado='';
			$sexoAfectado='';
		}
		if (isset($afectado->id_domicilio)) {
			/*$id_domicilio_afectada=$afectado->id_domicilio;
			$calle_afectado=$domicilioAfectada->calle;
			$municipioAfectado=$domicilioAfectada->id_municipio;
			$EstadoAfectado='';*/
			$calle_afectado='';
			$id_domicilio_afectada='';
			$municipioAfectado='';
			$EstadoAfectado='';
		}else{
			$calle_afectado='';
			$id_domicilio_afectada='';
			$municipioAfectado='';
			$EstadoAfectado='';
		}
	}else{
		$id_det_afectado='';
		$id_persona_afectada='';
		$id_domicilio_afectada='';
		$ape_pat_afectado='';
		$calle_afectado='';
		$ape_mat_afectado='';
		$nombre_afectado='';
		$municipioAfectado='';
		$EstadoAfectado='';
		$sexoAfectado='';
	}
 ?>
<!-- dropdowns -->
 <?php echo CHtml::textField('id_zona',$id_zona,array('style'=>'display:none;'));?>
 <?php echo CHtml::textField('id_corporacion',$id_corporacion,array('style'=>'display:none;'));?>
 <?php echo CHtml::textField('id_escolaridad',$id_escolaridad,array('style'=>'display:none;'));?>
 <?php echo CHtml::textField('id_DomMunicipio',$id_DomMunicipio,array('style'=>'display:none;'));?>
 <?php echo CHtml::textField('id_municipio',$id_municipio,array('style'=>'display:none;'));?>
 <?php echo CHtml::textField('id_estado',$id_estado,array('style'=>'display:none;'));?>
 <?php echo CHtml::textField('id_domEstado',$id_estado,array('style'=>'display:none;'));?>
 <?php echo CHtml::textField('id_identificacion',$id_identificacion,array('style'=>'display:none;'));?>
 <?php echo CHtml::textField('id_banda',$id_banda,array('style'=>'display:none;'));?>
<!-- end dropdowns <-->
<!-- foreing key -->
 <?php echo CHtml::textField('id_detencion',$id_detencion,array('style'=>'display:none;'));?>
 <?php echo CHtml::textField('id_persona',$id_persona,array('style'=>'display:none;'));?>
 <?php echo CHtml::textField('id_det_motivos',$id_det_motivos,array('style'=>'display:none;'));?>
 <?php echo CHtml::textField('id_lugar',$id_lugar,array('style'=>'display:none;'));?>
 <?php echo CHtml::textField('id_identificacion',$id_identificacion,array('style'=>'display:none;'));?> 
 <?php echo CHtml::textField('id_condicion',$id_condicion,array('style'=>'display:none;'));?>
 <?php echo CHtml::textField('id_objetos_porta',$id_objetos_porta,array('style'=>'display:none;'));?>
 <?php echo CHtml::textField('id_canalizacion',$id_canalizacion,array('style'=>'display:none;'));?>
 <?php echo CHtml::textField('id_descripcion',$id_descripcion,array('style'=>'display:none;'));?>
 <?php echo CHtml::textField('id_domicilio',$id_domicilio,array('style'=>'display:none;'));?>
 <?php echo CHtml::textField('id_domicilio_afectada',$id_domicilio_afectada,array('style'=>'display:none;'));?>
 <?php echo CHtml::textField('id_persona_afectada',$id_persona_afectada,array('style'=>'display:none;'));?>
<!-- end foreing key -->
<!-- Begin Webcam -->	
	<script type="text/javascript" src="<?php Yii::app()->theme->baseUrl; ?>js/jquery.lightbox-0.5.js"></script>
	<link rel="stylesheet" type="text/css" href="<?php Yii::app()->theme->baseUrl; ?> css/jquery.lightbox-0.5.css" media="screen" />
	<script type="text/javascript" src="js/webcam.js"></script>
    <script >
		webcam.set_api_url( 'test.php' );//PHP adonde va a recibir la imagen y la va a guardar en el servidor
		webcam.set_quality( 150 ); // calidad de la imagen
		webcam.set_shutter_sound( true ); // Sonido de flash
	</script>
	<script>
		webcam.set_hook( 'onComplete', 'my_completion_handler' );
		
		function do_upload() {
			// subir al servidor
			//document.getElementById('upload_results').innerHTML = '<h1>Cargando al servidor...</h1>';
			webcam.upload();
		}
		
		function my_completion_handler(msg) {
			
			if (msg.match(/(http\:\/\/\S+)/)) {
				var image_url = RegExp.$1;//respuesta de text.php que contiene la direccion url de la imagen
				
				// Muestra la imagen en la pantalla
				document.getElementById('upload_results').innerHTML = 
					'<img src="' + image_url + '">'+
					'<form action="gestion_foto.php" method="post">'+
					'<input type="hidden" name="id_foto" id="id_foto" value="' + image_url + '"  /><br>'+
					'<label>Nombre </label><input type="text" name="nombre_foto" id="nombre_foto"/>'+
					'<label>Descripcion </label><input type="text" name="des" id="des"/>'+
				    '<input type="submit" name="button" id="button" value="Enviar" /></form>'
					;
				// reset camera for another shot
				webcam.reset();
			}
			else alert("PHP Error: " + msg);
		}
	</script>
<!-- End Webcam -->
<div class="jumbotron">
	<div class="container" style="text-align: center;">
		<h2>DIRECCIÓN DE SEGURIDAD PÚBLICA MUNICIPAL DE RIOVERDE, S.L.P.</h2>
		<h3>CONTROL DE DETENIDOS</h3>
	</div>
</div>
<div role="tabpanel">
	<!-- Nav tabs -->
	<ul class="nav nav-tabs" role="tablist">
		<li role="presentation" class="active">
			<a href="#home" aria-controls="home" role="tab" data-toggle="tab">
				Datos Generales
			</a>
		</li>
		<?php foreach ($tabPersonaTitle as $tabPersonaTitle) { ?>
			<li role="presentation">
				<a href="#tab<?php echo CHTML::encode($tabPersonaTitle->id_detencion); ?>" aria-controls="tab" role="tab" data-toggle="tab">
					<?php echo CHTML::encode($tabPersonaTitle->fecha); ?>
				</a>
			</li>
		<?php } ?>
	</ul>

	<!-- Tab panes -->
	<div class="tab-content">
		<div role="tabpanel" class="tab-pane active" id="home">
			<div class="table-responsive">
				<table class="table table-hover">
					<tr>
						<th>Nombre</th>
						<td><?php echo $nombre.' '.$ape_pat.' '.$ape_mat; ?></td>
						<th>Sexo</th>
						<td><?php echo $sexo; ?></td>
					</tr>
					<tr>
						<th>Edad</th>
						<td><?php echo $edad; ?></td>
						<th>Fecha de Nacimiento</th>
						<td><?php echo $fecha_nacimiento; ?></td>
					</tr>
					<tr>
						<th>Estado Civil</th>
						<td><?php echo $estado_civil; ?></td>
						<th>Nacionalidad</th>
						<td><?php echo $nacionalidad; ?></td>
					</tr>
					<tr>
						<th>Señas Particulares</th>
						<td><?php echo $señas_particulares; ?></td>
						<td>Ocupacion</td>
						<td><?php echo $ocupacion; ?></td>
					</tr>
					<tr>
						<th>Alias</th>
					</tr>
				<?php foreach ($tabAlias as $tabAlias) { ?>
						<tr>
							<td>
								<?php echo CHTML::encode($tabAlias->alias); ?>
							</td>
						</tr>
					<?php  } ?>
				</table>
			</div>
		</div>
		<?php foreach ($tabPersona as $tabPersona) { ?>
		<?php $tabId_detencion=$tabPersona->id_detencion; ?>
		<?php $tabMotivos = DetMotivos::model()->findAll('id_detencion='.CHTML::encode($tabId_detencion)); ?>
		<?php $tabCanalizaciones = DetCanalizaciones::model()->findAll('id_canalizacion='.CHTML::encode($tabPersona->id_canalizacion)); ?>
		<?php $tabLugar = DetLugarAseguramiento::model()->findAll('id_lugar='.CHTML::encode($tabPersona->id_lugar)); ?>
		<div role="tabpanel" class="tab-pane" id="tab<?php echo CHTML::encode($tabPersona->id_detencion); ?>">
			<div class="table-responsive">
				<table class="table">
					<tr>
						<td>
							<table class="table table-hover">
								<tr>
									<th>Motivos de detencion</th>
								</tr>
								<?php foreach ($tabMotivos as $tabMotivos) { ?>
									<tr>
										<td>
											<?php
											 echo BasMotivos::getMotivos(CHTML::encode($tabMotivos->id_motivo)); ?>
										</td>
									</tr>
								<?php } ?>
							</table>
						</td>
						<td>
							<table class="table table-hover">
								<tr>
									<th>Bienes Afectados</th>
									<td><?php echo CHTML::encode($tabPersona->bienes_afectados); ?></td>
								</tr>
								<tr>
									<th>Monto de Afeccion</th>
									<td><?php echo CHTML::encode($tabPersona->monto_aproximado_afectacion); ?></td>
								</tr>
								<tr>
									<th>Modus Operandis</th>
									<td><?php echo CHTML::encode($tabPersona->modus_operandi); ?></td>
								</tr>
								<tr>
									<th>Detenido en</th>
									<td>
										<?php echo CHTML::encode($tabPersona->detenido_en); ?>
									</td>
								</tr>
							</table>
						</td>
					</tr>
					<tr>
						<td>
							<table class="table table-hover">
								<tr>
									<th>Condicion al momento de la detencion</th>
									<td>
										<?php echo DetCondicionesFisicas::getCondicion(CHTML::encode($tabPersona->id_condicion)); ?>
									</td>
								</tr>
								<tr>
									<th>Banda a la que pertenece</th>
									<td>
										<?php echo DetBandas::getNameBanda(CHTML::encode($tabPersona->id_banda)); ?>
									</td>
								</tr>
								<tr>
									<th>Corporacion que detuvo</th>
									<td>
										<?php echo BasCorporacion::getCorporacion(CHTML::encode($tabPersona->id_corporacion)); ?>
									</td>
								</tr>
								<tr>
									<th colspan="2">Canalización</th>
									<td>
										<?php foreach ($tabCanalizaciones as $tabCanalizaciones) { ?>
											<?php if ($tabCanalizaciones->juez_calificador!=0) { ?>
												<tr>
													<td colspan="2">Juez Calificador</td>
												</tr>
											<?php } ?>
											<?php if ($tabCanalizaciones->fuero_comun!=0) { ?>
												<tr>
													<td colspan="2">Fuero Comun</td>
												</tr>
											<?php } ?>
											<?php if ($tabCanalizaciones->transito_municipal!=0) { ?>
												<tr>
													<td colspan="2">Transito Municipal</td>
												</tr>
											<?php } ?>
											<?php if ($tabCanalizaciones->tutelar_menores!=0) { ?>
												<tr>
													<td colspan="2">Tutelar de Menores</td>
												</tr>
											<?php } ?>
											<?php if ($tabCanalizaciones->fuero_federal!=0) { ?>
												<tr>
													<td colspan="2">Fuero Federal</td>
												</tr>
											<?php } ?>
											<?php if ($tabCanalizaciones->mixta!=0) { ?>
												<tr>
													<td colspan="2">Mixta</td>
												</tr>
											<?php } ?>
											<?php if (isset($tabCanalizaciones->otro)) { ?>
												<tr>
													<td colspan="2">
														<?php echo CHTML::encode($tabCanalizaciones->otro); ?>
													</td>
												</tr>
											<?php } ?>
										<?php } ?>
										</tr>
									</td>
								</tr>
							</table>
						</td>
						<td>
							<table class="table table-hover">
								<tr>
									<th>Lugar de la detencion</th>
								</tr>
									<?php foreach ($tabLugar as $tabLugar) { ?>
									<tr>
										<th>Colonia</th>
										<td>
											<?php echo BasColonia::getColonia(CHTML::encode($tabLugar->id_colonia)); ?>
										</td>
									</tr>
									<tr>
										<th>Calle</th>
										<td>
											<?php echo CHTML::encode($tabLugar->calle); ?>
										</td>
									</tr>
									<tr>
										<th>Entre Calle</th>
										<td>
											<?php echo CHTML::encode($tabLugar->entre_calle); ?>
										</td>
									</tr>
									<tr>
										<th>Y Calle</th>
										<td>
											<?php echo CHTML::encode($tabLugar->y_la_calle); ?>
										</td>
									</tr>
									<tr>
										<th>A la altura de</th>
										<td>
											<?php echo CHTML::encode($tabLugar->a_la_altura); ?>
										</td>
									</tr>
									<?php } ?>
								</tr>
							</table>
						</td>
					</tr>
				</table>
			</div>
		</div>
		<?php } ?>
	</div>
</div>
<br><br>
<table class="table table-striped table-bordered">
	<tr>
		<td>Foto del Detenido</td>
		<td>
			<table width="100%" height="144">
				<tr>
					<td width="100" valign=top>
						<form>

							<input  type=button  value="Configuracion" onClick="webcam.configure()" class="btn btn-primary">
								<br><br>
							<input type=button value="Tomar Fotografía" onClick="webcam.freeze()" class="btn btn-primary">
								<br><br>
							<input type=button value="Guardar" onClick="do_upload()" class="btn btn-primary">
								<br>
								<br>
							<input type=button value="Volver a tomar" onClick="webcam.reset()" class="btn btn-primary">
				   		</form>	
					</td>
			   		 <td width="263" valign=top>
			   		 	<img src="">
						<script>
							document.write( webcam.get_html(320, 240) );//dimensiones de la camara
						</script>
			    	</td>   		 
			   		 <td style="display: none;" width=411>
				    	<div id="upload_results" class="formulario" > </div>
			  		</td>
				</tr>
			</table>
		</td>
		<td>BUSCAR ULTIMA PERSONA REGISTRADA</td>
		<td>
			<button class=" btn btn-3d btn-primary" onclick="openSearch()" value="Imprimir">
				<span class="fa fa-search"></span> Buscar 
			</button>
		</td>
	</tr>
	<tr>
		<td>Zona</td>
		<td>
			<?php 
				echo CHtml::dropDownList(
					'zona',
					$id_zona,
					BasZona::getListZona(),
					array('empty' => ' ','style'=>'width:80%')
				); 
			?>
			<a  onclick="mensajeAdvertencia('mensajeZona',
			'Selecciona la corporacion actual. <b>Obligatorio</b>')"
			 id="mensajeZona" class="glyphicon glyphicon-info-sign" style="font-size: 1.2em;
			 cursor:hand;"></a>
		</td>
		<td>Fecha</td>
		<td>
			<b><label id="fecha"><?php echo $fecha; ?></label></b>
		</td>
	</tr>
	<tr>
		<td>Municipio</td>
		<td>
			<b>RIOVERDE, S.L.P.</b>
		</td>
		<td>Hora</td>
		<td>
			<b><label id="hora"><?php echo $hora; ?></label></b>
		</td>
	</tr>
	<tr>
		<td>Corporacion</td>
		<td colspan="3">
			<?php 
				echo CHtml::dropDownList(
					'corporacion',
					$id_corporacion,
					BasCorporacion::getListCorporacion(),
					array('empty' => '','style'=>'width:80%')
				); 
			?>
			<a  onclick="mensajeAdvertencia('mensajeCorporacion',
			'Selecciona la corporacion actual. <b>Obligatorio</b>')"
			 id="mensajeCorporacion" class="glyphicon glyphicon-info-sign" style="font-size: 1.2em;
			 cursor:hand;"></a>
		</td>
	</tr>
	<tr>
		<td>Detenido en</td>
		<td colspan="3">
			<?php 
				echo CHtml::dropDownList(
					'detenido_en',
					$detenido_en,
				array('AUXILIO'=>'AUXILIO',
					'RECORRIDO'=>'RECORRIDO',
					'OPERATIVO'=>'OPERATIVO'
				),
				array('empty' => ' ','style'=>'width:80%')
			 );
			?>
			<a  onclick="mensajeAdvertencia('mensajedetenido_en',
			'Selecciona en donde fue detenido. <b>Obligatorio</b>')"
			 id="mensajedetenido_en" class="glyphicon glyphicon-info-sign" style="font-size: 1.2em;
			 cursor:hand;"></a>
		</td>
	</tr>
</table>
<table class="table table-striped table-bordered">
	<tr>
		<td colspan="2">
			<h3><b>DATOS DEL ASEGURADO</b></h3>
		</td>
		<td class="non">
			<h3><b>MOTIVOS DEL ASEGURAMIENTO</b></h3>
		</td>
	</tr>
	<tr>
		<td>Apellido Paterno</td>
		<td>
			<?php echo CHtml::textField('ape_pat',
				$ape_pat,
				array('onkeypress'=>'letras("ape_pat")'));?>
				<a  onclick="mensajeAdvertencia('mensajePat',
				'Ingresar el apellido paterno. <b>Obligatorio</b>')"
				 id="mensajePat" class="glyphicon glyphicon-info-sign" style="font-size: 1.2em;
				 cursor:hand;"></a>
		</td>
		<td class="non">Motivos del aseguramiento</td>
		<td class="non">
			<?php 
				echo CHtml::dropDownList(
					'motivos',
					$motivos,
					BasMotivos::getListMotivos(),
					array('empty' => ' ','style'=>'width:50%')
				); 
			?>
			<a class="fa fa-plus btn btn-sm" onclick="addMotivos()"></a>
			<a  onclick="mensajeAdvertencia('mensajeMotivos',
			'Selecciona el motivo de detencion y a continuacion presione el boton +. <b>Obligatorio</b>')"
			 id="mensajeMotivos" class="glyphicon glyphicon-info-sign" style="font-size: 1.2em;
			 cursor:hand;"></a>
			 <br>
			<?php $this->widget('zii.widgets.grid.CGridView', array(
				'id'=>'motivo-grid',
				'itemsCssClass'=>"table table-stripped table-hover table-bordered",
				'pager'=>array("htmlOptions"=>array("class"=>"pagination")),
				'summaryText'=>'',
				'dataProvider'=>$dataProMotivo,
				//'filter'=>$model,
				'columns'=>array(
					'motivo'
				),
			)); ?>
		</td>
	</tr>
	<tr>
		<td>
			Apellido Materno
		</td>
		<td>
			<?php echo CHtml::textField('ape_mat',
				$ape_mat,
				array('onkeypress'=>'letras("ape_mat")'));?>
				<a  onclick="mensajeAdvertencia('mensajeMat',
				'Ingresar el apellido paterno. <b>Obligatorio</b>')"
				 id="mensajeMat" class="glyphicon glyphicon-info-sign" style="font-size: 1.2em;
				 cursor:hand;"></a>
		</td>
	</tr>
	<tr>
		<td>
			Nombre
		</td>
		<td>
			<?php echo CHtml::textField('nombre',
				$nombre,
				array('onkeypress'=>'letras("nombre")'));?>
				<a  onclick="mensajeAdvertencia('mensajeMat',
				'Ingresar el apellido paterno. <b>Obligatorio</b>')"
				 id="mensajeMat" class="glyphicon glyphicon-info-sign" style="font-size: 1.2em;
				 cursor:hand;"></a>
		</td>
		<td>
			<h3><b>LUGAR DEL ASEGURAMIENTO</b></h3>
		</td>		
	</tr>
	<tr>
		<td class="non">
			Alias
		</td>
		<td class="non">
			<?php echo CHtml::textField('alias',
				$alias);?>
				<a class="fa fa-plus btn btn-sm" onclick="addAlias()"></a>
				<a  onclick="mensajeAdvertencia('mensajeAlias',
				'Ingresar el alias del sujeto y acontinuacion presione el boton +. <b>Obligatorio</b>')"
				 id="mensajeAlias" class="glyphicon glyphicon-info-sign" style="font-size: 1.2em;
				 cursor:hand;"></a>
				 <br>
				<?php $this->widget('zii.widgets.grid.CGridView', array(
					'id'=>'alias-grid',
					'itemsCssClass'=>"table table-stripped table-hover table-bordered",
					'pager'=>array("htmlOptions"=>array("class"=>"pagination")),
					'summaryText'=>'',
					'dataProvider'=>$dataProALias,
					//'filter'=>$model,
					'columns'=>array(
						'alias',
					),
				)); ?>
		</td>
		<td>Colonia</td>
		<td>
			<input type="text" style="display:none;" id="colonia">
			<?php 
				$this->widget('zii.widgets.jui.CJuiAutocomplete', array(
					'id'=>'coloniaAseguramiento',
					'name'=>'coloniaAseguramiento',
					'source'=>$this->createUrl('DetDetenciones/listadoColonia'),
					'options'=>array(
						'delay'=>300,
						'minLength'=>1,
						'showAnim'=>'fold',
						'select'=>"js:function(event,ui){
							$('#colonia').val(ui.item.id);								
						}"
						),
						'htmlOptions'=>array(
							'style'=>'margin-top:10px;'
							),
							
						)); 
				?> 
		</td>
	</tr>
	<tr>
		<td>Sexo</td>
		<td>
			<?php 
				echo CHtml::dropDownList(
					'sexo',
					$sexo,
				array('empty' => '',
					'HOMBRE'=>'HOMBRE',
					'MUJER'=>'MUJER'
				)
			 );
			?>
			<a  onclick="mensajeAdvertencia('mensajeSexo',
			'Selecciona el sexo del detenido. <b>Obligatorio</b>')"
			 id="mensajeSexo" class="glyphicon glyphicon-info-sign" style="font-size: 1.2em;
			 cursor:hand;"></a>
		</td>
		<td>Calle</td>
		<td>
			<?php echo CHtml::textField('calle',
				$calle,
				array('onkeypress'=>'vacios("calle")'));?>
				<a  onclick="mensajeAdvertencia('mensajeCalle',
				'Ingresar la calle donde se realizo la aprension. <b>Obligatorio</b>')"
				 id="mensajeCalle" class="glyphicon glyphicon-info-sign" style="font-size: 1.2em;
				 cursor:hand;"></a>
		</td>
	</tr>
	<tr>
		<td>Edad</td>
		<td>
			<input type="number" id="edad" value="<?php echo $edad; ?>">
			<a  onclick="mensajeAdvertencia('mensajeEdad',
			'Ingresar la edad del detenido. <b>Obligatorio</b>')"
			 id="mensajeEdad" class="glyphicon glyphicon-info-sign" style="font-size: 1.2em;
			 cursor:hand;"></a>
		</td>
		<td>Entre Calle</td>
		<td>
			<?php echo CHtml::textField('entre_calle',
				$entre_calle,
				array('onkeypress'=>'vacios("entre_calle")'));?>
				<a  onclick="mensajeAdvertencia('mensajeEntreCalle',
				'Ingresar entre que calles se realizo la aprension. <b>Obligatorio</b>')"
				 id="mensajeEntreCalle" class="glyphicon glyphicon-info-sign" style="font-size: 1.2em;
				 cursor:hand;"></a>
		</td>
	</tr>
	<tr>
		<td>Estado Civil</td>
		<td>
			<?php 
				echo CHtml::dropDownList(
					'estado_civil',
					$estado_civil,
				array('empty' => '',
					'SOLTERO/A'=>'SOLTERO/A',
					'COMPROMETIDO/A'=>'COMPROMETIDO/A',
					'CASADO/A'=>'CASADO/A',
					'DIVORCIADO/A'=>'DIVORCIADO/A',
					'VIUDO/A'=>'VIUDO/A'
				)
			 );
			?>
				<a  onclick="mensajeAdvertencia('mensajeCivil',
				'Ingresar el estado civil del detenido. <b>Obligatorio</b>')"
				 id="mensajeCivil" class="glyphicon glyphicon-info-sign" style="font-size: 1.2em;
				 cursor:hand;"></a>
		</td>
		<td>Y Calle</td>
		<td>
			<?php echo CHtml::textField('y_la_calle',
				$y_la_calle,
				array('onkeypress'=>'vacios("y_la_calle")'));?>
				<a  onclick="mensajeAdvertencia('mensajeYCalle',
				'Ingresar entre que calles se realizo la aprension. <b>Obligatorio</b>')"
				 id="mensajeYCalle" class="glyphicon glyphicon-info-sign" style="font-size: 1.2em;
				 cursor:hand;"></a>
		</td>
	</tr>
	<tr>
		<td>Ocupacion</td>
		<td>
			<?php echo CHtml::textField('ocupacion',
				$ocupacion,
				array('onkeypress'=>'letras("ocupacion")'));?>
				<a  onclick="mensajeAdvertencia('mensajeOcupacion',
				'Ingresar la ocupacion del detenido. <b>Obligatorio</b>')"
				 id="mensajeOcupacion" class="glyphicon glyphicon-info-sign" style="font-size: 1.2em;
				 cursor:hand;"></a>
		</td>
		<td>A La Altura De</td>
		<td>
			<?php echo CHtml::textField('a_la_altura',
				$a_la_altura,
				array('onkeypress'=>'vacios("a_la_altura")'));?>
				<a  onclick="mensajeAdvertencia('mensajeAltura',
				'Ingresar la calle donde se realizo la aprension. <b>Obligatorio</b>')"
				 id="mensajeAltura" class="glyphicon glyphicon-info-sign" style="font-size: 1.2em;
				 cursor:hand;"></a>
		</td>
	</tr>
	<tr>
		<td>Escolaridad</td>
		<td>
			<?php 
				echo CHtml::dropDownList(
					'escolaridad',
					$id_escolaridad,
					GenEscolaridad::getListEscolaridad(),
					array('empty' => ' ','style'=>'width:80%')
				); 
			?>
				<a  onclick="mensajeAdvertencia('mensajeEscolar',
				'Ingresar la escolaridad del detenido. <b>Obligatorio</b>')"
				 id="mensajeEscolar" class="glyphicon glyphicon-info-sign" style="font-size: 1.2em;
				 cursor:hand;"></a>
		</td>
		<td>Municipio</td>
		<td>
			<?php 
				echo CHtml::dropDownList(
					'municipio',
					'',
					GenMunicipio::getListMunicipio(),
					array('empty' => ' ','style'=>'width:80%')
				); 
			?>
				<a  onclick="mensajeAdvertencia('mensajeDomMuni',
				'Ingresar el domicilio del detenido (Municipio). <b>Obligatorio</b>')"
				 id="mensajeDomMuni" class="glyphicon glyphicon-info-sign" style="font-size: 1.2em;
				 cursor:hand;"></a>
		</td>
		<td>Estado</td>
		<td>
			<?php 
				echo CHtml::dropDownList(
					'estado',
					'',
					GenEstado::getListEstado(),
					array('empty' => ' ','style'=>'width:80%')
				); 
			?>
				<a  onclick="mensajeAdvertencia('mensajeDomMuni',
				'Ingresar el domicilio del detenido (Municipio). <b>Obligatorio</b>')"
				 id="mensajeDomMuni" class="glyphicon glyphicon-info-sign" style="font-size: 1.2em;
				 cursor:hand;"></a>
		</td>
	</tr>
	<tr>
		<td>Fecha de Nacimiento</td>
		<td>
			<?php 
			 	$this->widget('zii.widgets.jui.CJuiDatePicker', array(
				'name'=>'fecha_nacimiento',
			 	'value'=>$fecha_nacimiento,
			 	'language' => 'es',
			 	'options'=>array(
			 		'autoSize'=>false,
			 		'defaultDate'=>$fecha_nacimiento,
			 		//'dateFormat'=>'dd/mm/yy',
			 		'buttonImage'=>Yii::app()->baseUrl.'/images/calendar.gif',
					"changeYear"=>true,
					"maxDate"=>"Y",
					"minDate"=>"-100Y",
					"showButtonPanel"=>true,
					"yearRange"=>"-80:-5",
			 		"dateFormat"=>"yy/mm/dd",
			 		'buttonImageOnly'=>true,
			 		'buttonText'=>'Fecha',
			 		'changeMonth' => true,
			 		'selectOtherMonths'=>true,
			 		'showAnim'=>'slide',
			 		'showButtonPanel'=>true,
			 		'showOn'=>'button',
			 		'showOtherMonths'=>true,
			 		),
			 	));
		 	?>
		</td>
		<td colspan="2"  class="noon">
			<h3><b>OBJETOS ASEGURADOS Y CARACTERISTICAS</b></h3>
		</td>		
	</tr>
	<tr>
		<td>Identificacion</td>
		<td>
			<?php 
				echo CHtml::dropDownList(
					'identificacion',
					$id_identificacion,
					DetIdentificacion::getListIdentificacion(),
					array('empty' => ' ','style'=>'width:80%')
				); 
			?>
				<a  onclick="mensajeAdvertencia('mensajeIdentificacion',
				'Ingresar la escolaridad del detenido. <b>Obligatorio</b>')"
				 id="mensajeIdentificacion" class="glyphicon glyphicon-info-sign" style="font-size: 1.2em;
				 cursor:hand;"></a>
		</td>
		<td>
			<script>
				function addObjeto() {
					var objeto =$('#objeto').val();
					var caracteristicas =$('#objetoDescripcion').val();
					var id_detencion =$('#id_detencion').val();
					var id_persona =$('#id_persona').val();
					if (id_persona=='' || id_detencion=='') {
						alert('PRIMERO DEBES GUARDAR LA DETENCION Y/O AÑADIR UNA PERSONA');
					}else{
						<?php echo CHtml::ajax(array(
							'url'=>array('detObjetos/AddObjeto'),
							'type'=>'post',
							'data'=>array(
								'objeto'=>'js:objeto',
								'id_detencion'=>'js:id_detencion',
								'id_persona'=>'js:id_persona',
								'caracteristicas'=>'js:caracteristicas',
							),
							'dataType'=>'json',
							'success'=>"function(data)
							{
								$.fn.yiiGridView.update('objetos-grid');
								alert('SE HA AÑADIDO EL OBJETO');
							}"
						))
						?>;
					}
				}
			</script>
			<table class="table table-striped table-bordered">
				<tr>
					<td class="noon">Objeto</td>
					<td  class="noon">
						<?php echo CHtml::textField('objeto',
							'',
							array('onkeypress'=>'vacios("objeto")'));?>
							<a  onclick="mensajeAdvertencia('mensajeObjeto',
							'Ingresar los objetos asegurados, armas, vehiculos, etc. <b>Obligatorio</b>')"
							 id="mensajeObjeto" class="glyphicon glyphicon-info-sign" style="font-size: 1.2em;
							 cursor:hand;"></a>
						<a class="fa fa-plus btn btn-sm" onclick="addObjeto()"></a>
					</td>
				</tr>
				<tr>
					<td class="non">Descripcion</td>
					<td class="non">
						<textarea id="objetoDescripcion"></textarea>
					</td>
					<td>
					</td>
				</tr>
				<tr>
					<td colspan="3">
						<?php $this->widget('zii.widgets.grid.CGridView', array(
							'id'=>'objetos-grid',
							'itemsCssClass'=>"table table-stripped table-hover table-bordered",
							'pager'=>array("htmlOptions"=>array("class"=>"pagination")),
							'summaryText'=>'',
							'dataProvider'=>$dataProObjeto,
							//'filter'=>$model,
							'columns'=>array(
								'objeto',
								'caracteristicas'
							),
						)); ?>
					</td>
				</tr>
			</table>
		</td>
	</tr>
	<tr>
		<td>Nacionalidad</td>
		<td>
			<?php echo CHtml::textField('nacionalidad',
				$nacionalidad,
				array('onkeypress'=>'letras("nacionalidad")'));?>
				<a  onclick="mensajeAdvertencia('mensajeNacionalidad',
				'Ingresar la nacionalidad del detenido. <b>Obligatorio</b>')"
				 id="mensajeNacionalidad" class="glyphicon glyphicon-info-sign" style="font-size: 1.2em;
				 cursor:hand;"></a>
		</td>
	</tr>
	<tr>
		<td colspan="4">
			<h2><b>DOMICILIO DEL ASEGURADO</b></h2>
		</td>
	</tr>
	<tr>
		<td>Calle</td>
		<td>
			<?php echo CHtml::textField('domCalle',
				$domCalle,
				array('onkeypress'=>'vacios("domCalle")'));?>
				<a  onclick="mensajeAdvertencia('mensajeDomCalle',
				'Ingresar el domicilio del detenido (Calle). <b>Obligatorio</b>')"
				 id="mensajeDomCalle" class="glyphicon glyphicon-info-sign" style="font-size: 1.2em;
				 cursor:hand;"></a>
		</td>
		<td>Colonia</td>
		<td>
			<input type="text" style="display:none;" id="domColonia">
			<?php 
				$this->widget('zii.widgets.jui.CJuiAutocomplete', array(
					'id'=>'domiColonia',
					'name'=>'domiColonia',
					'source'=>$this->createUrl('DetDetenciones/listadoColonia'),
					'options'=>array(
						'delay'=>300,
						'minLength'=>1,
						'showAnim'=>'fold',
						'select'=>"js:function(event,ui){
							$('#domColonia').val(ui.item.id);								
						}"
						),
						'htmlOptions'=>array(
							'style'=>'margin-top:10px;'
							),
							
						)); 
				?> 
		</td>
	</tr>
	<tr>
		<td>Municipio</td>
		<td>
			<?php 
				echo CHtml::dropDownList(
					'domMunicipio',
					'',
					GenMunicipio::getListMunicipio(),
					array('empty' => ' ','style'=>'width:80%')
				); 
			?>
				<a  onclick="mensajeAdvertencia('mensajeDomMuni',
				'Ingresar el domicilio del detenido (Municipio). <b>Obligatorio</b>')"
				 id="mensajeDomMuni" class="glyphicon glyphicon-info-sign" style="font-size: 1.2em;
				 cursor:hand;"></a>
		</td>
		<td>Estado</td>
		<td>
			<?php 
				echo CHtml::dropDownList(
					'domEstado',
					'',
					GenEstado::getListEstado(),
					array('empty' => ' ','style'=>'width:80%')
				); 
			?>
				<a  onclick="mensajeAdvertencia('mensajeDomEstado',
				'Ingresar el domicilio del detenido (Estado). <b>Obligatorio</b>')"
				 id="mensajeDomEstado" class="glyphicon glyphicon-info-sign" style="font-size: 1.2em;
				 cursor:hand;"></a>
		</td>
	</tr>
	<tr>
		<td colspan="4">
			<h2><b>BANDA A LA QUE PERTENECE</b></h2>
		</td>
	</tr>
	<tr>
		<td colspan="2">Nombre</td>
		<td colspan="2">
			<?php 
				echo CHtml::dropDownList(
					'detBanda',
					$id_banda,
					DetBandas::getListBanda()
				); 
			?>
			<a class="fa fa-plus btn btn-sm" onclick="addBanda()"></a>
				<a  onclick="mensajeAdvertencia('mensajeBanda',
				'<b>En caso de no encontrar la banda deseada presionar + para agregarla</b>')"
				 id="mensajeBanda" class="glyphicon glyphicon-info-sign" style="font-size: 1.2em;
				 cursor:hand;"></a>
		</td>
	</tr>
</table>
<table class="table table-striped table-bordered">
	<tr>
		<td colspan="2">
			<h2><b>CARACTERISTICAS DEL ASEGURADO</b></h2>
		</td>
	</tr>
	<tr>
		<td>Condiciones Fisicas</td>
		<td>
			<?php 
				echo CHtml::dropDownList(
					'detCondicionesFisicas',
					$id_condicion,
					DetCondicionesFisicas::getListCondicion(),
					array('empty' => ' ','style'=>'width:50%')
				); 
			?>
				<a  onclick="mensajeAdvertencia('mensajeCondicion',
				'Selecciona el estado en el que se encuentra el detenido. <b>Obligatorio</b>')"
				 id="mensajeCondicion" class="glyphicon glyphicon-info-sign" style="font-size: 1.2em;
				 cursor:hand;"></a>
		</td>
	</tr>
	<tr>
		<td>Tatuajes, Marca Fisica o Señas Particulares</td>
		<td>
			<textarea id="señas_particulares"> <?php echo $señas_particulares; ?></textarea>
		</td>
	</tr>
	<tr>
		<td>Lesiones que presenta</td>
		<td>
			<textarea id="lesiones_presenta"><?php echo $lesiones_presenta; ?></textarea>
		</td>
	</tr>
</table>
<br>
<table class="table table-striped table-bordered">
	<tr>
		<td colspan="2">Descripcion Fisica:</td>					
	</tr>
	<tr>					
		<td>Estatura</td>
		<td>
			<?php echo CHtml::textField('estatura',
				$estatura,
				array('onkeypress'=>'vacios("estatura")'));?>
				<a  onclick="mensajeAdvertencia('mensajeEstatura',
				'Ingresar la estatura del detenido. <b>Obligatorio</b>')"
				 id="mensajeEstatura" class="glyphicon glyphicon-info-sign" style="font-size: 1.2em;
				 cursor:hand;"></a>
		</td>
		<td>Color de Piel</td>
		<td>
			<?php echo CHtml::textField('color_piel',
				$color_piel,
				array('onkeypress'=>'letras("color_piel")'));?>
				<a  onclick="mensajeAdvertencia('mensajePiel',
				'Ingresar el color de piel del detenido. <b>Obligatorio</b>')"
				 id="mensajePiel" class="glyphicon glyphicon-info-sign" style="font-size: 1.2em;
				 cursor:hand;"></a>
		</td>
	</tr>
	<tr>
		<td>Cejas</td>
		<td>
			<?php echo CHtml::textField('cejas',
				$cejas,
				array('onkeypress'=>'letras("cejas")'));?>
				<a  onclick="mensajeAdvertencia('mensajeCejas',
				'Ingresar la cantidad de ceja del detenido. <b>Obligatorio</b>')"
				 id="mensajeCejas" class="glyphicon glyphicon-info-sign" style="font-size: 1.2em;
				 cursor:hand;"></a>
		</td>
		<td>Color de Ojos</td>
		<td>
			<?php echo CHtml::textField('color_ojos',
				$color_ojos,
				array('onkeypress'=>'letras("color_ojos")'));?>
				<a  onclick="mensajeAdvertencia('mensajeOjos',
				'Ingresar el color de ojos del detenido. <b>Obligatorio</b>')"
				 id="mensajeOjos" class="glyphicon glyphicon-info-sign" style="font-size: 1.2em;
				 cursor:hand;"></a>
		</td>
	</tr>
	<tr>
		<td>Color de Pelo</td>
		<td>
			<?php echo CHtml::textField('color_pelo',
				$color_pelo,
				array('onkeypress'=>'letras("color_pelo")'));?>
				<a  onclick="mensajeAdvertencia('mensajePelo',
				'Ingresar el color de pelo del detenido. <b>Obligatorio</b>')"
				 id="mensajePelo" class="glyphicon glyphicon-info-sign" style="font-size: 1.2em;
				 cursor:hand;"></a>
		</td>
		<td>Menton</td>
		<td>
			<?php echo CHtml::textField('menton',
				$menton,
				array('onkeypress'=>'letras("menton")'));?>
				<a  onclick="mensajeAdvertencia('mensajeMenton',
				'Ingresar la de menton del detenido. <b>Obligatorio</b>')"
				 id="mensajeMenton" class="glyphicon glyphicon-info-sign" style="font-size: 1.2em;
				 cursor:hand;"></a>
		</td>
	</tr>
	<tr>
		<td>Frente</td>
		<td>
			<?php echo CHtml::textField('frente',
				$frente,
				array('onkeypress'=>'letras("frente")'));?>
				<a  onclick="mensajeAdvertencia('mensajeFrente',
				'Ingresar el tipo de frente del detenido. <b>Obligatorio</b>')"
				 id="mensajeFrente" class="glyphicon glyphicon-info-sign" style="font-size: 1.2em;
				 cursor:hand;"></a>
		</td>
		<td>Complexion</td>
		<td>
			<?php echo CHtml::textField('complexion',
				$complexion,
				array('onkeypress'=>'letras("complexion")'));?>
				<a  onclick="mensajeAdvertencia('mensajeComplexion',
				'Ingresar la complexion del detenido. <b>Obligatorio</b>')"
				 id="mensajeComplexion" class="glyphicon glyphicon-info-sign" style="font-size: 1.2em;
				 cursor:hand;"></a>
		</td>
	</tr>
</table>
<?php 

function checados($check){
	if($check==1) return true; else false;// Revisa si los checks estan marcados
}
	if (isset($detencion->id_objetos_porta)) {
		$camisa=checados($objetosPorta->camisa);
		$tenis=checados($objetosPorta->tenis);
		$pulsera=checados($objetosPorta->pulsera);
		$cartera=checados($objetosPorta->cartera);
		$pantalon=checados($objetosPorta->pantalon);
		$zapatos=checados($objetosPorta->zapatos);
		$gorra=checados($objetosPorta->gorra);
		$lentes=checados($objetosPorta->lentes);
		$playera=checados($objetosPorta->playera);
		$botas=checados($objetosPorta->botas);
		$cinturon=checados($objetosPorta->cinturon);
		$chamarra=checados($objetosPorta->chamarra);
		$sombrero=checados($objetosPorta->sombrero);
		$cadena=checados($objetosPorta->cadena);
		$otros=checados($objetosPorta->otros);
		$obDescripcion=$objetosPorta->descripcion;
	}else{
		$camisa=false;
		$tenis=false;
		$pulsera=false;
		$cartera=false;
		$pantalon=false;
		$zapatos=false;
		$gorra=false;
		$lentes=false;
		$playera=false;
		$botas=false;
		$cinturon=false;
		$chamarra=false;
		$sombrero=false;
		$cadena=false;
		$otros=false;
		$obDescripcion='';
	}
	if (isset($detencion->id_canalizacion)) {
		$juez_calificador=checados($canalizacion->juez_calificador);
		$fuero_comun=checados($canalizacion->fuero_comun);
		$fuero_federal=checados($canalizacion->fuero_federal);
		$mixta=checados($canalizacion->mixta);
		$tutelar_menores=checados($canalizacion->tutelar_menores);
		$migracion=checados($canalizacion->migracion);
		$canaOtros=$canalizacion->otro;
	}else{
		$juez_calificador=false;
		$fuero_comun=false;
		$fuero_federal=false;
		$mixta=false;
		$tutelar_menores=false;
		$migracion=false;
		$canaOtros='';
	}
 ?>
<table class="table table-striped table-bordered">
	<tr>
		<td colspan="8">
			<h2><b>OBJETOS QUE PORTA EL DETENIDO</b></h2>
		</td>
	</tr>
	<tr>
		<td>
			<?php echo CHtml::checkBox('camisa',$camisa); ?>
		</td>
		<td>CAMISA</td>
		<td>
			<?php echo CHtml::checkBox('tenis',$tenis); ?>
		</td>
		<td>TENIS</td>
		<td>
			<?php echo CHtml::checkBox('pulsera',$pulsera); ?>
		</td>
		<td>PULSERA</td>
		<td>
			<?php echo CHtml::checkBox('cartera',$cartera); ?>
		</td>
		<td>CARTERA</td>
	</tr>
	<tr>
		<td>
			<?php echo CHtml::checkBox('pantalon',$pantalon); ?>
		</td>
		<td>PANTALON</td>
		<td>
			<?php echo CHtml::checkBox('zapatos',$zapatos); ?>
		</td>
		<td>ZAPATOS</td>
		<td>
			<?php echo CHtml::checkBox('gorra',$gorra); ?>
		</td>
		<td>GORRA</td>
		<td>
			<?php echo CHtml::checkBox('lentes',$lentes); ?>
		</td>
		<td>LENTES</td>
	</tr>
	<tr>
		<td>
			<?php echo CHtml::checkBox('playera',$playera); ?>
		</td>
		<td>PLAYERA</td>
		<td>
			<?php echo CHtml::checkBox('botas',$botas); ?>
		</td>
		<td>BOTAS</td>
		<td>
			<?php echo CHtml::checkBox('cinturon',$cinturon); ?>
		</td>
		<td>CINTURON</td>
		<td>
			<?php echo CHtml::checkBox('sombrero',$sombrero); ?>

		</td>
		<td>SOMBRERO</td>
	</tr>
	<tr>
		<td>
			<?php echo CHtml::checkBox('chamarra',$chamarra); ?>
		</td>
		<td>CHAMARRA</td>
		<td>
			<?php echo CHtml::checkBox('otros',$otros); ?>
		</td>
		<td>OTROS</td>
		<td colspan="4">
			<textarea id="obDescripcion"><?php echo $obDescripcion; ?></textarea>
		</td>
	</tr>
	<tr>
		<td>
			<?php echo CHtml::checkBox('cadena',$cadena); ?>
		</td>
		<td>CADENA</td>
	</tr>
</table>

<table class="table table-striped table-bordered">
	<tr>
		<td colspan="2">
			<h2><b>Canalización</b></h2>
		</td>
	</tr>
	<tr>
		<td>
			<?php echo CHtml::checkBox('juez_calificador',$juez_calificador); ?>
		</td>
		<td>JUEZ CALIFICADOR</td>
		<td>
			<?php echo CHtml::checkBox('fuero_comun',$fuero_comun); ?>
		</td>
		<td>M.P. FUERO COMUN</td>
	</tr>
	<tr>
		<td>
			<?php echo CHtml::checkBox('fuero_federal',$fuero_federal); ?>
		</td>
		<td>M.P. FUERO FEDERAL</td>
		<td>
			<?php echo CHtml::checkBox('mixta',$mixta); ?>
		</td>
		<td>MIXTA</td>
	</tr>
	<tr>
		<td>
			<?php echo CHtml::checkBox('tutelar_menores',$tutelar_menores); ?>
		</td>
		<td>CONSEJO TUTELAR DE MENORES</td>
		<td>
			<?php echo CHtml::checkBox('migracion',$migracion); ?>
		</td>
		<td>INSTITUTO NACIONAL DE MIGRACION</td>
	</tr>
	<tr>
		<td>
			OTROS
		</td>
		<td>
			<textarea id="canaOtros"><?php echo $canaOtros; ?></textarea>
		</td>
	</tr>
</table>

<table class="table table-striped table-bordered">
	<tr>
		<td colspan="2">
			<h2><b>MODUS OPERANDIS</b></h2>
		</td>
	</tr>
	<tr>
		<td>Descripcion</td>
		<td>
			<textarea id="modus_operandi"> <?php echo $modus_operandi; ?></textarea>
		</td>
	</tr>
	<tr>
		<td>Bienes Afectados</td>
		<td>
			<textarea id="bienes_afectados"><?php echo $bienes_afectados; ?></textarea>
		</td>
	</tr>
	<tr>
		<td>Monto Aproximado de la Afeccion</td>
		<td>
			<?php echo CHtml::textField('monto_aproximado_afectacion',
				$monto_aproximado_afectacion,
				array('onkeypress'=>'vacios("monto_aproximado_afectacion")'));?>
				<a  onclick="mensajeAdvertencia('mensajeMontoAprox',
				'Ingresar la complexion del detenido. <b>Obligatorio</b>')"
				 id="mensajeMontoAprox" class="glyphicon glyphicon-info-sign" style="font-size: 1.2em;
				 cursor:hand;"></a>
		</td>
	</tr>
</table>

<table class="table table-striped table-bordered">
	<tr>
		<td colspan="2">
			<h2><b>DATOS DEL AFECTADO</b></h2>
		</td>
		<td colspan="2">
			<h2><b>DOMICILIO DEL AFECTADO</b></h2>
		</td>
	</tr>
	<tr>
		<td>Apellido Paterno</td>
		<td>
			<?php echo CHtml::textField('ape_pat_afectado',
				$ape_pat_afectado,
				array('onkeypress'=>'letras("ape_pat_afectado")'));?>
				<a  onclick="mensajeAdvertencia('mensajeApePatAfec',
				'Ingresar la complexion del detenido. <b>Obligatorio</b>')"
				 id="mensajeApePatAfec" class="glyphicon glyphicon-info-sign" style="font-size: 1.2em;
				 cursor:hand;"></a>
		</td>
		<td>Calle</td>
		<td>
			<?php echo CHtml::textField('calle_afectado',
				$calle_afectado,
				array('onkeypress'=>'letras("calle_afectado")'));?>
				<a  onclick="mensajeAdvertencia('mensajeCalleAectado',
				'Ingresar la complexion del detenido. <b>Obligatorio</b>')"
				 id="mensajeCalleAectado" class="glyphicon glyphicon-info-sign" style="font-size: 1.2em;
				 cursor:hand;"></a>
		</td>
	</tr>
	<tr>
		<td>Apellido Materno</td>
		<td>
			<?php echo CHtml::textField('ape_mat_afectado',
				$ape_mat_afectado,
				array('onkeypress'=>'letras("ape_mat_afectado")'));?>
				<a  onclick="mensajeAdvertencia('mensajeApeMatAfec',
				'Ingresar la complexion del detenido. <b>Obligatorio</b>')"
				 id="mensajeApeMatAfec" class="glyphicon glyphicon-info-sign" style="font-size: 1.2em;
				 cursor:hand;"></a>
		</td>
		<td>Colonia</td>
		<td>
			<input type="text" style="display:none;" id="id_coloniaAfectado">
			<?php 
				$this->widget('zii.widgets.jui.CJuiAutocomplete', array(
					'id'=>'coloniaAfectado',
					'name'=>'coloniaAfectado',
					'source'=>$this->createUrl('DetDetenciones/listadoColonia'),
					'options'=>array(
						'delay'=>300,
						'minLength'=>1,
						'showAnim'=>'fold',
						'select'=>"js:function(event,ui){
							$('#id_coloniaAfectado').val(ui.item.id);								
						}"
						),
						'htmlOptions'=>array(
							'style'=>'margin-top:10px;'
							),
							
						)); 
				?> 
		</td>
	</tr>
	<tr>
		<td>Nombre</td>
		<td>
			<?php echo CHtml::textField('nombre_afectado',
				$nombre_afectado,
				array('onkeypress'=>'letras("nombre_afectado")'));?>
				<a  onclick="mensajeAdvertencia('mensajeApeMatAfec',
				'Ingresar la complexion del detenido. <b>Obligatorio</b>')"
				 id="mensajeApeMatAfec" class="glyphicon glyphicon-info-sign" style="font-size: 1.2em;
				 cursor:hand;"></a>
		</td>
		<td>Estado</td>
		<td>
			<?php 
				echo CHtml::dropDownList(
					'EstadoAfectado',
					$EstadoAfectado,
					GenEstado::getListEstado(),
					array('empty' => ' ',
						'style'=>'width:80%',
						/*'ajax'=>array(
						  'type'=>'POST',
						  'url'=>CController::createUrl('GenEstado/change'),
						  'update'=>'#'.CHtml::activeId($model,'id_nivel_dos'),
						  'beforeSend' => 'function(){
						   $("#Registro_id_nivel_dos").find("option").remove();
						   $("#Registro_id_nivel_tres").find("option").remove();
						   }',  
						)*/
					)
				); 
			?>
				<a  onclick="mensajeAdvertencia('mensajeAfecMuni',
				'Ingresar el domicilio del afectado (Municipio). <b>Obligatorio</b>')"
				 id="mensajeAfecMuni" class="glyphicon glyphicon-info-sign" style="font-size: 1.2em;
				 cursor:hand;"></a>
		</td>
	</tr>
	<tr>
		<td>Sexo</td>
		<td>
			<?php 
				echo CHtml::dropDownList(
					'sexoAfectado',
					$sexoAfectado,
				array('empty' => '',
					'HOMBRE'=>'HOMBRE',
					'MUJER'=>'MUJER'
				)
			 );
			?>
			<a  onclick="mensajeAdvertencia('mensajeSexoAfectado',
			'Selecciona el sexo del detenido. <b>Obligatorio</b>')"
			 id="mensajeSexoAfectado" class="glyphicon glyphicon-info-sign" style="font-size: 1.2em;
			 cursor:hand;"></a>
		</td>
		<td>Municipio</td>
		<td>
			<?php 
				echo CHtml::dropDownList(
					'municipioAfectado',
					$municipioAfectado,
					GenMunicipio::getListMunicipio(),
					array('empty' => ' ','style'=>'width:80%')
				); 
			?>
				<a  onclick="mensajeAdvertencia('mensajeAfecMuni',
				'Ingresar el domicilio del afectado (Municipio). <b>Obligatorio</b>')"
				 id="mensajeAfecMuni" class="glyphicon glyphicon-info-sign" style="font-size: 1.2em;
				 cursor:hand;"></a>
		</td>
	</tr>
</table>
<script>
	function addOficial() {
		var id_oficial =$('#oficial').val();
		var id_detencion =$('#id_detencion').val();
		var responsable =$('#responsable').val();
		if (id_detencion=='') {
			alert('PRIMERO DEBES GUARDAR LA DETENCION');
		}else{
			<?php echo CHtml::ajax(array(
				'url'=>array('detDetencionesOficiales/AddOficial'),
				'type'=>'post',
				'data'=>array(
					'id_oficial'=>'js:id_oficial',
					'id_detencion'=>'js:id_detencion',
					'responsable'=>'js:responsable',
				),
				'dataType'=>'json',
				'success'=>"function(data)
				{
					$.fn.yiiGridView.update('oficiales-grid');
					alert('SE HA AÑADIDO AL OFICIAL');
				}"
			))
			?>;
		}
	}
</script>
<table class="table table-striped table-bordered">
	<tr>
		<td colspan="3">
			<h2><b>OFICIALES QUE PARTICIPARON EN LA DETENCION</b></h2>
		</td>
	</tr>
	<tr>
		<td>Oficial</td>
		<td>
			<input type="text" style="display:none;" id="oficial">
			<?php 
				$this->widget('zii.widgets.jui.CJuiAutocomplete', array(
					'id'=>'addOficial',
					'name'=>'addOficial',
					'source'=>$this->createUrl('DetDetencionesOficiales/ListadoOficiales'),
					'options'=>array(
						'delay'=>300,
						'minLength'=>1,
						'showAnim'=>'fold',
						'select'=>"js:function(event,ui){
							$('#oficial').val(ui.item.id);								
						}"
						),
						'htmlOptions'=>array(
							'style'=>'width:100%;'
							),
							
						)); 
				?> 
		</td>
		<td>
			<?php 
				echo CHtml::dropDownList(
					'responsable',
					'',
				array(
					'RESPONSABLE'=>'RESPONSABLE',
					'AUXILIAR'=>'AUXILIAR',
					'TESTIGO'=>'TESTIGO'
				)
			 );
			?>
		</td>
		<td>
			<button class=" btn btn-3d btn-primary" onclick="addOficial()">
				<span class="fa fa-plus"></span> Agregar Oficial
			</button>
		</td>
	</tr>
	<tr>
		<td colspan="3">
			<?php $this->widget('zii.widgets.grid.CGridView', array(
				'id'=>'oficiales-grid',
				'dataProvider'=>$dataProOficiales,
				//'filter'=>$model,
				'columns'=>array(
					//'id_det_oficiales',
					'nombre',
					'responsable',
					//'id_oficial',
					//'id_detencion',
				),
			)); ?>
		</td>
	</tr>
</table>
<?php if(!isset($detencion->id_detencion)) { ?>
	<button id="guardarDetenido" class="btn btn-success" value="guardar" onclick="guardarDetenido()">Guardar</button>
<?php } else { ?>
	<button id="guardarDetenido" class="btn btn-success" value="actualizar" onclick="guardarDetenido()">Actualizar</button>
<?php } ?>


<?php 
	$this->beginWidget('zii.widgets.jui.CJuiDialog', array(
		'id'=>'dialogGif',
		'options'=>array(
			'title'=>'BUSCANDO DETENIDO',
			'autoOpen'=>false,
			'modal'=>true,
			'width'=>450,
			'height'=>450,
			'resizable'=>false,
			'position'=>'{my:"bottom", at: "center", of:button}',
		),
	));	
?>
<img src="<?php echo Yii::app()->baseUrl.'/images/load.gif';?>" class="img-responsive">


<?php $this->endWidget();?>

<script>
	function addBandaNew() {
		var nombre_banda = $('#nombre_banda').val();
		var id_colonia_banda = $('#id_colonia_banda').val();

		<?php echo CHtml::ajax(array(
			'url'=>array('detBandas/addBanda'),
			'data'=>array(
				'nombre_banda'=>'js:nombre_banda',
				'id_colonia_banda'=>'js:id_colonia_banda',
			),
			'type'=>'post',
			'dataType'=>'json',
			'success'=>"function(data)
			{
				alert('Banda añadida')
				$('#addBanda').dialog('close');
                $('#detBanda').empty();
                $('#detBanda').append(new Option(' ', ''));
				$.each(data.banda, function(value,key) {
					$('#detBanda').append($('<option></option>')
						.attr('value', value).text(key));
				});
			}"
		))
		?>;	
		return false;
	}	
</script>
<?php 
	$this->beginWidget('zii.widgets.jui.CJuiDialog', array(
		'id'=>'addBanda',
		'options'=>array(
			'title'=>'Añadir Banda',
			'autoOpen'=>false,
			'modal'=>true,
			'width'=>450,
			'height'=>450,
			'resizable'=>false,
			'position'=>'{my:"bottom", at: "center", of:button}',
		),
	));	
?>
<div class="divForForm">
	<div class="table-responsive">
		<table class="table table-hover">
			<thead>
				<tr>
					<th>BANDA</th>
				</tr>
			</thead>
			<tbody>
				<tr>
					<td>
						<?php echo CHtml::textField('nombre_banda',
							'',
							array('onkeypress'=>'vacios("nombre_banda")'));?>
					</td>
				</tr>
			</tbody>
			<thead>
				<tr>
					<th>COLONIA DONDE OPERA</th>
				</tr>
			</thead>
			<tbody>
				<tr>
					<td>
						<input type="text" style="display:none;" id="id_colonia_banda">
						<?php 
							$this->widget('zii.widgets.jui.CJuiAutocomplete', array(
								'id'=>'colonia_banda',
								'name'=>'colonia_banda',
								'source'=>$this->createUrl('DetDetenciones/listadoColonia'),
								'options'=>array(
									'delay'=>300,
									'minLength'=>1,
									'showAnim'=>'fold',
									'select'=>"js:function(event,ui){
										$('#id_colonia_banda').val(ui.item.id);								
									}"
									),
									'htmlOptions'=>array(
										'style'=>'margin-top:10px;'
										),
										
									)); 
							?>
					</td>
				</tr>
			</tbody>
			<tr>
				<td>
					<button id="addBandaSave" class="btn btn-success" onclick="addBandaNew()">Guardar</button>
				</td>
			</tr>
		</table>
	</div>
</div>


<?php $this->endWidget();?>
<!-- Begin combos -->
	<script>
		/*$('#EstadoAfectado').change(function(){
			var id_estado=$('#EstadoAfectado').val();
			<?php /*echo CHtml::ajax(array(
				'url'=>array('genEstado/change'),
				'data'=>array(
					'id_estado'=>'js:id_estado'
				),
				'type'=>'post',
				'dataType'=>'json',
				'success'=>"function(data)
				{
	                $('#municipioAfectado').empty();
	                $('#municipioAfectado').append(new Option(' ', ''));
	                var value=data.id_municipio;
	                var key=data.municipio;
					$.each(data.municipios, function(value,key) {
						$('#municipioAfectado').append($('<option></option>')
							.attr('value', value).text(key));
					});
				}"
			))*/
			?>;	
		});
			$('#municipioAfectado').change(function(){
				var id_municipio=$('#municipioAfectado').val();
				alert(id_municipio);
			});*/
	</script>
<!-- End combos -->
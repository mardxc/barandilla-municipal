<?php
/* @var $this DetDetencionesController */
/* @var $model DetDetenciones */

$this->breadcrumbs=array(
	'Det Detenciones'=>array('index'),
	$model->id_detencion=>array('view','id'=>$model->id_detencion),
	'Update',
);

$this->menu=array(
	array('label'=>'List DetDetenciones', 'url'=>array('index')),
	array('label'=>'Create DetDetenciones', 'url'=>array('create')),
	array('label'=>'View DetDetenciones', 'url'=>array('view', 'id'=>$model->id_detencion)),
	array('label'=>'Manage DetDetenciones', 'url'=>array('admin')),
);
?>

<h1>Update DetDetenciones <?php echo $model->id_detencion; ?></h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>
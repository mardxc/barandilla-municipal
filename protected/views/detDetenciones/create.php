<?php
/* @var $this DetDetencionesController */
/* @var $model DetDetenciones */

$this->breadcrumbs=array(
	'Det Detenciones'=>array('index'),
	'Create',
);

$this->menu=array(
	array('label'=>'List DetDetenciones', 'url'=>array('index')),
	array('label'=>'Manage DetDetenciones', 'url'=>array('admin')),
);
?>

<h1>Create DetDetenciones</h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>
<script>
	function addDetenido(){
		window.location="index.php?r=detDetenciones/registrar&id_detencion=";
	}
</script>
<br><br><br><br>
<br>
<br>
<a id="addDetenido"   onclick="addDetenido()" style="text-align:rigth;cursor:hand;left:0%;" class="btn btn-success bt-large">
	<i class="fa fa-plus"> Nuevo Registro</i>
</a>
<?php $this->widget('zii.widgets.grid.CGridView', array(
	'id'=>'det-detencion-grid',
	'itemsCssClass'=>"table table-stripped table-hover table-bordered",
	'pager'=>array("htmlOptions"=>array("class"=>"pagination")),
	'emptyText'=>'No existen resultados en esta busqueda',
	'summaryText'=>'{start}-{end} de {count} Detenidos',
	'dataProvider'=>$model->search(),
	'filter'=>$model,
	'columns'=>array(
		array(
					'name'=>'id_persona',
					'header'=>'Nombre',
					'value'=>'PerPersonas::pName($data->id_persona)',
				),
		'detenido_en',
		'fecha',
		'hora',
		array(
					'name'=>'id_banda',
					'header'=>'Banda',
					'value'=>'DetBandas::getNameBanda($data->id_banda)',
					//'filter'=>DetBandas::getListBanda(),
				),		
		/*array(
					'name'=>'motivo',
					'header'=>'Motivos',
					'value'=>'DetMotivos::getMotivo($data->id_detencion)',
					//'filter'=>DetMotivos::getMotivo(),
				),*/
		array(
			'class'=>'CButtonColumn',
			'header'=>'ACCIONES',
			//'htmlOptions' => array('style'=>'color:green;'),
			'template'=>'{detalles}',
			'deleteConfirmation'=>('Desear borrar el registro?'),
				'buttons'=>array(
					/*'delete' => array( //botón para la acción nueva
						'options' => array('rel' => 'tooltip', 
							'data-toggle' => 'tooltip', 
							'title' => Yii::t('app', 'Eliminar'),
							'id'=>'eliminar'
						),
	                	'label' => '<i class="fa fa-times fa-2x"></i>',
	                	'imageUrl' => false,								    								    
						'deleteConfirmation'=>'Esta seguro que desea borrar el registro?',

					),*/
					'detalles' => array( //botón para los detalles
						'options' => array('rel' => 'tooltip', 
							'data-toggle' => 'tooltip', 
							'title' => Yii::t('app', 'Detalles'),
							'id'=>'informacion'
						),
	                	'label' => '<i class="fa fa-refresh fa-2x"></i>',
	                	'imageUrl' => false,
						'url'=>'Yii::app()->createUrl("detDetenciones/registrar", array("id_detencion"=>$data->id_detencion))',
					),
				),
		),
	),
)); ?>

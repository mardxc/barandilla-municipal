<?php
/* @var $this GenMunicipioController */
/* @var $model GenMunicipio */

$this->breadcrumbs=array(
	'Gen Municipios'=>array('index'),
	'Create',
);

$this->menu=array(
	array('label'=>'List GenMunicipio', 'url'=>array('index')),
	array('label'=>'Manage GenMunicipio', 'url'=>array('admin')),
);
?>

<h1>Create GenMunicipio</h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>
<?php
/* @var $this GenMunicipioController */
/* @var $model GenMunicipio */

$this->breadcrumbs=array(
	'Gen Municipios'=>array('index'),
	$model->id_municipio=>array('view','id'=>$model->id_municipio),
	'Update',
);

$this->menu=array(
	array('label'=>'List GenMunicipio', 'url'=>array('index')),
	array('label'=>'Create GenMunicipio', 'url'=>array('create')),
	array('label'=>'View GenMunicipio', 'url'=>array('view', 'id'=>$model->id_municipio)),
	array('label'=>'Manage GenMunicipio', 'url'=>array('admin')),
);
?>

<h1>Update GenMunicipio <?php echo $model->id_municipio; ?></h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>
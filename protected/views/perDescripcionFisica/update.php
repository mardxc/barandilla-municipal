<?php
/* @var $this PerDescripcionFisicaController */
/* @var $model PerDescripcionFisica */

$this->breadcrumbs=array(
	'Per Descripcion Fisicas'=>array('index'),
	$model->id_descripcion=>array('view','id'=>$model->id_descripcion),
	'Update',
);

$this->menu=array(
	array('label'=>'List PerDescripcionFisica', 'url'=>array('index')),
	array('label'=>'Create PerDescripcionFisica', 'url'=>array('create')),
	array('label'=>'View PerDescripcionFisica', 'url'=>array('view', 'id'=>$model->id_descripcion)),
	array('label'=>'Manage PerDescripcionFisica', 'url'=>array('admin')),
);
?>

<h1>Update PerDescripcionFisica <?php echo $model->id_descripcion; ?></h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>
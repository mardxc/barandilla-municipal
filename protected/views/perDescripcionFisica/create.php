<?php
/* @var $this PerDescripcionFisicaController */
/* @var $model PerDescripcionFisica */

$this->breadcrumbs=array(
	'Per Descripcion Fisicas'=>array('index'),
	'Create',
);

$this->menu=array(
	array('label'=>'List PerDescripcionFisica', 'url'=>array('index')),
	array('label'=>'Manage PerDescripcionFisica', 'url'=>array('admin')),
);
?>

<h1>Create PerDescripcionFisica</h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>
<?php
/* @var $this PerDescripcionFisicaController */
/* @var $model PerDescripcionFisica */
/* @var $form CActiveForm */
?>

<div class="form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'per-descripcion-fisica-form',
	'enableAjaxValidation'=>false,
)); ?>

	<p class="note">Fields with <span class="required">*</span> are required.</p>

	<?php echo $form->errorSummary($model); ?>

	<div class="row">
		<?php echo $form->labelEx($model,'estatura'); ?>
		<?php echo $form->textField($model,'estatura',array('size'=>10,'maxlength'=>10)); ?>
		<?php echo $form->error($model,'estatura'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'color_piel'); ?>
		<?php echo $form->textField($model,'color_piel',array('size'=>10,'maxlength'=>10)); ?>
		<?php echo $form->error($model,'color_piel'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'cejas'); ?>
		<?php echo $form->textField($model,'cejas',array('size'=>10,'maxlength'=>10)); ?>
		<?php echo $form->error($model,'cejas'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'color_ojos'); ?>
		<?php echo $form->textField($model,'color_ojos',array('size'=>10,'maxlength'=>10)); ?>
		<?php echo $form->error($model,'color_ojos'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'color_pelo'); ?>
		<?php echo $form->textField($model,'color_pelo',array('size'=>10,'maxlength'=>10)); ?>
		<?php echo $form->error($model,'color_pelo'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'menton'); ?>
		<?php echo $form->textField($model,'menton',array('size'=>15,'maxlength'=>15)); ?>
		<?php echo $form->error($model,'menton'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'frente'); ?>
		<?php echo $form->textField($model,'frente',array('size'=>10,'maxlength'=>10)); ?>
		<?php echo $form->error($model,'frente'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'complexion'); ?>
		<?php echo $form->textField($model,'complexion',array('size'=>10,'maxlength'=>10)); ?>
		<?php echo $form->error($model,'complexion'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'id_persona'); ?>
		<?php echo $form->textField($model,'id_persona'); ?>
		<?php echo $form->error($model,'id_persona'); ?>
	</div>

	<div class="row buttons">
		<?php echo CHtml::submitButton($model->isNewRecord ? 'Create' : 'Save'); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- form -->
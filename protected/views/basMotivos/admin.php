
<h1> Motivos</h1>
<a class="btn btn-primary" href="index.php?r=basMotivos/create">
	<i class="fa fa-plus"></i>&nbsp Motivo
</a>


<?php $this->widget('zii.widgets.grid.CGridView', array(
	'id'=>'bas-motivos-grid',
	'dataProvider'=>$model->search(),
	'pager'=>array("htmlOptions"=>array("class"=>"pagination")), 
	'emptyText'=>"No existen resultados en esta búsqueda", 
	'summaryText'=>'{start}-{end} de {count} Motivos',
	'filter'=>$model,
	'columns'=>array(
		'motivo',
		'descripcion',
		'gravedad',
		array(
			'class'=>'CButtonColumn',
			'template'=>'{update}{delete}',
			'deleteConfirmation'=>('¿Desea borrar el registro?'),
		),
	),
)); ?>

<?php
/* @var $this DetLugarAseguramientoController */
/* @var $model DetLugarAseguramiento */

$this->breadcrumbs=array(
	'Det Lugar Aseguramientos'=>array('index'),
	'Create',
);

$this->menu=array(
	array('label'=>'List DetLugarAseguramiento', 'url'=>array('index')),
	array('label'=>'Manage DetLugarAseguramiento', 'url'=>array('admin')),
);
?>

<h1>Create DetLugarAseguramiento</h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>
<?php
/* @var $this DetLugarAseguramientoController */
/* @var $model DetLugarAseguramiento */
/* @var $form CActiveForm */
?>

<div class="form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'det-lugar-aseguramiento-form',
	'enableAjaxValidation'=>false,
)); ?>

	<p class="note">Fields with <span class="required">*</span> are required.</p>

	<?php echo $form->errorSummary($model); ?>

	<div class="row">
		<?php echo $form->labelEx($model,'colonia'); ?>
		<?php echo $form->textField($model,'colonia',array('size'=>30,'maxlength'=>30)); ?>
		<?php echo $form->error($model,'colonia'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'calle'); ?>
		<?php echo $form->textField($model,'calle',array('size'=>30,'maxlength'=>30)); ?>
		<?php echo $form->error($model,'calle'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'num_int'); ?>
		<?php echo $form->textField($model,'num_int'); ?>
		<?php echo $form->error($model,'num_int'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'num_ext'); ?>
		<?php echo $form->textField($model,'num_ext'); ?>
		<?php echo $form->error($model,'num_ext'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'entre_calle'); ?>
		<?php echo $form->textField($model,'entre_calle',array('size'=>30,'maxlength'=>30)); ?>
		<?php echo $form->error($model,'entre_calle'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'y_la_calle'); ?>
		<?php echo $form->textField($model,'y_la_calle',array('size'=>30,'maxlength'=>30)); ?>
		<?php echo $form->error($model,'y_la_calle'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'a_la_altura'); ?>
		<?php echo $form->textField($model,'a_la_altura',array('size'=>30,'maxlength'=>30)); ?>
		<?php echo $form->error($model,'a_la_altura'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'id_barrio'); ?>
		<?php echo $form->textField($model,'id_barrio'); ?>
		<?php echo $form->error($model,'id_barrio'); ?>
	</div>

	<div class="row buttons">
		<?php echo CHtml::submitButton($model->isNewRecord ? 'Create' : 'Save'); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- form -->
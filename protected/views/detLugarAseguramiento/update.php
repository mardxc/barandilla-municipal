<?php
/* @var $this DetLugarAseguramientoController */
/* @var $model DetLugarAseguramiento */

$this->breadcrumbs=array(
	'Det Lugar Aseguramientos'=>array('index'),
	$model->id_lugar=>array('view','id'=>$model->id_lugar),
	'Update',
);

$this->menu=array(
	array('label'=>'List DetLugarAseguramiento', 'url'=>array('index')),
	array('label'=>'Create DetLugarAseguramiento', 'url'=>array('create')),
	array('label'=>'View DetLugarAseguramiento', 'url'=>array('view', 'id'=>$model->id_lugar)),
	array('label'=>'Manage DetLugarAseguramiento', 'url'=>array('admin')),
);
?>

<h1>Update DetLugarAseguramiento <?php echo $model->id_lugar; ?></h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>
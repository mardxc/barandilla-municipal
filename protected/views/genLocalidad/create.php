<?php
/* @var $this GenLocalidadController */
/* @var $model GenLocalidad */

$this->breadcrumbs=array(
	'Gen Localidads'=>array('index'),
	'Create',
);

$this->menu=array(
	array('label'=>'List GenLocalidad', 'url'=>array('index')),
	array('label'=>'Manage GenLocalidad', 'url'=>array('admin')),
);
?>

<h1>Create GenLocalidad</h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>
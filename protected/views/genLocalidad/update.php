<?php
/* @var $this GenLocalidadController */
/* @var $model GenLocalidad */

$this->breadcrumbs=array(
	'Gen Localidads'=>array('index'),
	$model->id_localidad=>array('view','id'=>$model->id_localidad),
	'Update',
);

$this->menu=array(
	array('label'=>'List GenLocalidad', 'url'=>array('index')),
	array('label'=>'Create GenLocalidad', 'url'=>array('create')),
	array('label'=>'View GenLocalidad', 'url'=>array('view', 'id'=>$model->id_localidad)),
	array('label'=>'Manage GenLocalidad', 'url'=>array('admin')),
);
?>

<h1>Update GenLocalidad <?php echo $model->id_localidad; ?></h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>
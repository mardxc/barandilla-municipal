<?php
/* @var $this DetCondicionesFisicasController */
/* @var $model DetCondicionesFisicas */

$this->breadcrumbs=array(
	'Det Condiciones Fisicases'=>array('index'),
	$model->id_condicion=>array('view','id'=>$model->id_condicion),
	'Update',
);

$this->menu=array(
	array('label'=>'List DetCondicionesFisicas', 'url'=>array('index')),
	array('label'=>'Create DetCondicionesFisicas', 'url'=>array('create')),
	array('label'=>'View DetCondicionesFisicas', 'url'=>array('view', 'id'=>$model->id_condicion)),
	array('label'=>'Manage DetCondicionesFisicas', 'url'=>array('admin')),
);
?>

<h1>Update DetCondicionesFisicas <?php echo $model->id_condicion; ?></h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>
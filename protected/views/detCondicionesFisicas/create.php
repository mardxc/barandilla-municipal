<?php
/* @var $this DetCondicionesFisicasController */
/* @var $model DetCondicionesFisicas */

$this->breadcrumbs=array(
	'Det Condiciones Fisicases'=>array('index'),
	'Create',
);

$this->menu=array(
	array('label'=>'List DetCondicionesFisicas', 'url'=>array('index')),
	array('label'=>'Manage DetCondicionesFisicas', 'url'=>array('admin')),
);
?>

<h1>Create DetCondicionesFisicas</h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>
<script>
	    function imprimeListado(){
			var id_zona=$('#zona').val();
			var edadInicial=$('#edadInicial').val();
			var edadFinal=$('#edadFinal').val();
			var estado_civil=$('#estado_civil').val();
			var horaInicial=$('#horaInicial').val();
			var horaFinal=$('#horaFinal').val();
			var ocupacion=$('#ocupacion').val();
			var id_motivo=$('#motivos').val();
			var id_corporacion=$('#corporacion').val();
			var id_canalizacion=$('#canalizacion').val();
			var id_colonia=$('#colonia').val();
	        
	        if(id_zona!=''){
	            window.location="index.php?r=reporte/imprimeListado&id_zona="+id_zona+"&edadInicial="+edadInicial+"&edadFinal="+edadFinal+"&estado_civil="+estado_civil+"&horaInicial="+horaInicial+"&horaFinal="+horaFinal+"&ocupacion="+ocupacion+"&id_motivo="+id_motivo+
	            "&id_corporacion="+id_corporacion+"&id_canalizacion="+id_canalizacion+"&id_colonia="+id_colonia;
	        }else{
	        	alert('Selecciona la zona');
	        }
	    }

	    function executeReport() {
			var zona=$('#zona').val();
	    	$.fn.yiiGridView.update('per-grid',{data:{"id_detencion":id_detencion}});
	    }
</script>
<script>
	function printDet() {
		var id_detencion=  $.fn.yiiGridView.getChecked('print-grid', 'id_detencion');
		window.location="index.php?r=reporte/pdf&id_detencion="+id_detencion;
		return false;
	}
</script>
	<div class="jumbotron">
		<div class="container">
			<h1>Create Your Report</h1>
		</div>
		<table class="table table-bordered table-striped">
			<tr>
				<td colspan="2">Edad</td>
				<td>Estado Civil</td>
				<td colspan="2">Hora de Entrada</td>
			</tr>
			<tr>
				<td>
					<input type="number" id="edadInicial">
				</td>
				<td>
					<input type="number" id="edadFinal">
				</td>
				<td>
					<?php 
						echo CHtml::dropDownList(
							'estado_civil',
							'',
						array('empty' => '',
							'SOLTERO/A'=>'SOLTERO/A',
							'COMPROMETIDO/A'=>'COMPROMETIDO/A',
							'CASADO/A'=>'CASADO/A',
							'DIVORCIADO/A'=>'DIVORCIADO/A',
							'VIUDO/A'=>'VIUDO/A'
						)
					 );
					?>
				</td>
				<td>
					<input type="time" id="horaInicial">
				</td>
				<td>
					<input type="time" id="horaFinal">
				</td>
			</tr>
			<tr>
				<td>Ocupacion</td>
				<td>Motivo de Detencion</td>
				<td>Corporacion</td>
				<td>Zona de Detencion</td>
				<td>Disposicion</td>
			</tr>
			<tr>
				<td><?php echo CHtml::textField('ocupacion','');?></td>
				<td>
					<?php 
						echo CHtml::dropDownList(
							'motivos',
							'',
							BasMotivos::getListMotivos()
						); 
					?>
				</td>
				<td>
					<?php 
						echo CHtml::dropDownList(
							'corporacion',
							'',
							BasCorporacion::getListCorporacion()
						); 
					?>
				</td>
				<td>
					<?php 
						echo CHtml::dropDownList(
							'zona',
							'',
							BasZona::getListZona()
						); 
					?>
				</td>
				<td>
					<?php 
						echo CHtml::dropDownList(
							'canalizacion',
							'',
						array('empty' => '',
							'fuero_comun'=>'FUERO COMUN',
							'fuero_federal'=>'FUERO FEDERAL',
							'juez_calificador'=>'JUEZ CALIFICADOR',
							'migracion'=>'MOGRACION',
							'mixta'=>'MIXTA',
							'transito_municipal'=>'TRANSITO MUNICIPAL',
							'tutelar_menores'=>'TUTELAR DE MENORES',
							'otro'=>'OTRO',
						)
					 );
					?>
				</td>
			</tr>
			<tr>
				<td colspan="2">Lugar de Detencion (Colonia)</td>
				<td colspan="3">
					<?php 
						echo CHtml::dropDownList(
							'colonia',
							'',
							BasColonia::getListColonia()
						); 
					?>
				</td>
			</tr>
			<tr>
				<td colspan="2">
					<a onclick="imprimeListado()" class="btn btn-success"><i class="fa fa-check"></i> Ejecutar Reporte</a>
				</td>
				<td colspan="2">
					<button class=" btn btn-3d btn-primary" onclick="printDet()" value="Imprimir">
	                    <span class="fa fa-download"></span> Imprimir
	                  </button>
				</td>
			</tr>
		</table>
	</div>
<button onclick="select()"></button>
	<?php $this->widget('zii.widgets.grid.CGridView', array(
		'id'=>'print-grid',
		'dataProvider'=>$dataProviderPro,
		//'itemsCssClass'=>"table table-stripped table-hover table-bordered",
		'pager'=>array("htmlOptions"=>array("class"=>"pagination")),
		'emptyText'=>'No existen resultados en esta busqueda',
		'summaryText'=>'',
		'columns'=>array(
			array(
				'id'=>'id_detencion',
				'header'=>'Marque para Imprimir',
				'class'=>'CCheckBoxColumn',
				'selectableRows'=>'2',
			),
			'nombre',
			'edad',
			'fecha_det',
			'hora_det',
			'ocupacion',
			'motivo_det',
			'corporacion',
			'zona_det',
			'canalizacion',
			/*'calle_dom',
			'colonia_dom',
			'municipio_dom',*/
			'civil',
			/*'colonia_det',
			'calle_det',
			'entre_calle_det',
			'y_la_calle_det',
			'a_la_altura_det',*/
			'colonia_det',
		),
	)); ?>
</div>
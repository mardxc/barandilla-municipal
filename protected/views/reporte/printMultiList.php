<link href="<?php echo Yii::app()->theme->baseUrl; ?>/css/bootstrap.min.css">
<style>	
	table{
		margin-left: 20px;
	    width:100%;
	}
	table, th, td {
	    border-collapse: collapse;
	    border: 1px solid black;
	}
	th, td {
	    padding: 5px;
	    text-align: center;
	}
	table tr:nth-child(even) {
	    background-color: #eee;
	}
	table tr:nth-child(odd) {
	   background-color:#fff;
	}
	table th {
	    background-color: black;
	    color: white;
	}
	h1{
		text-align: center;
	}
</style>
	<!--<img src="<?php// echo Yii::app()->request->baseUrl.'/images/logo_policia.JPG';?>" class="img-responsive">-->
<?php 
	for ($i=0; $i <count($arraId) ; $i++) { 

		$detencion = DetDetenciones::model()->findByAttributes(array('id_detencion'=>$arraId[$i]));

		$persona = PerPersonas::model()->findByAttributes(array('id_persona'=>$detencion->id_persona));
		//$lugar = DetLugarAseguramiento::model()->findByAttributes(array('id_lugar'=>$detencion->id_lugar));
		$domicilio = PerDomicilios::model()->findByAttributes(array('id_persona'=>$detencion->id_persona));
		$descripcionFisica = PerDescripcionFisica::model()->findByAttributes(array('id_persona'=>$persona->id_persona));

		$objetosPorta = DetObjetosPorta::model()->findByAttributes(array('id_objetos_porta'=>$detencion->id_objetos_porta));
?>
<br><br>
	<h1>DIRECCIÓN DE SEGURIDAD PÚBLICA MUNICIPAL DE RIOVERDE, S.L.P.</h1>
		<table class="table table-hover table-striped ">
			<tbody>
				<tr>
					<th colspan="6"><b>Datos del Detenido</b></th>
				</tr>
				<tr>
					<td><b>Nombre</b></td>
					<td colspan="5"><?php echo $persona->nombre.' '.$persona->ape_pat.' '.$persona->ape_mat;; ?></td>
				</tr>
				<tr>
					<td><b>Domicilio</b></td>
					<td><b>Calle</b></td>
					<td><?php echo $domicilio->calle; ?></td>
					<td><b>Colonia</b></td>
					<td colspan="2"><?php //echo BasColonia::getColonia($domicilio->id_colonia); ?></td>
				</tr>
				<tr>
					<td><b>Sexo</b></td>
					<td><?php echo $persona->sexo; ?></td>
					<td><b>Edad</b></td>
					<td><?php echo $persona->edad; ?></td>
					<td><b>Estatura</b></td>
					<td><?php echo $descripcionFisica->estatura; ?></td>
				</tr>
				<tr>
					<td><b>Color de Piel</b></td>
					<td><?php echo $descripcionFisica->color_piel; ?></td>
					<td><b>Color de Pelo</b></td>
					<td><?php echo $descripcionFisica->color_pelo; ?></td>
					<td><b>Color de Ojos</b></td>
					<td><?php echo $descripcionFisica->color_ojos; ?></td>
				</tr>
				<tr>
					<td><b>Complexion</b></td>
					<td><?php echo $descripcionFisica->complexion; ?></td>
					<td><b>Menton</b></td>
					<td><?php echo $descripcionFisica->menton; ?></td>
					<td><b>Frente</b></td>
					<td><?php echo $descripcionFisica->frente; ?></td>
				</tr>
				<tr>
					<td><b>Cejas</b></td>
					<td><?php echo $descripcionFisica->cejas; ?></td>.
					<td><b>Señas Particulares</b></td>
					<td colspan="3"><?php echo $persona->señas_particulares; ?></td>
				</tr>
				<tr>
					<th colspan="6">Objetos que Porta</th>
				</tr>
				<tr>
					<td><?php echo CHtml::checkBox('camisa',$objetosPorta->camisa); ?> Camisa</td>
					<td><?php echo CHtml::checkBox('pantalon',$objetosPorta->pantalon); ?> Pantalon</td>
					<td><?php echo CHtml::checkBox('playera',$objetosPorta->playera); ?> Playera</td>
					<td><?php echo CHtml::checkBox('chamarra',$objetosPorta->chamarra); ?> Chamarra</td>
					<td colspan="2"><?php echo CHtml::checkBox('tenis',$objetosPorta->tenis); ?> Tenis</td>
				</tr>
				<tr>
					<td><?php echo CHtml::checkBox('zapatos',$objetosPorta->zapatos); ?> Zapatos</td>
					<td><?php echo CHtml::checkBox('botas',$objetosPorta->botas); ?> Botas</td>
					<td><?php echo CHtml::checkBox('sombrero',$objetosPorta->sombrero); ?> Sombrero</td>
					<td><?php echo CHtml::checkBox('cadena',$objetosPorta->cadena); ?> Cadena</td>
					<td colspan="2"><?php echo CHtml::checkBox('pulsera',$objetosPorta->pulsera); ?> Pulsera</td>

				</tr>
				<tr>
					<td><?php echo CHtml::checkBox('gorra',$objetosPorta->gorra); ?> Gorra</td>
					<td><?php echo CHtml::checkBox('cinturon',$objetosPorta->cinturon); ?> Cinturon</td>
					<td><?php echo CHtml::checkBox('cartera',$objetosPorta->cartera); ?> Cartera</td>
					<td colspan="3"><?php echo CHtml::checkBox('lentes',$objetosPorta->lentes); ?> Lentes</td>
				</tr>
				<tr>
					<td><input type="checkbox" name="otros" value=""> otros</td>
					<td colspan="5"><?php  ?></td>
				</tr>
			</tbody>
			<tbody>
				<tr>
					<th colspan="6"><b>DONDE</b></th>
				</tr>
				<tr>
					<td><b>Ciudad</b></td>
					<td><?php  ?></td>
					<td><b>Colonia</b></td>
					<td><?php  ?></td>
					<td><b>Zona</b></td>
					<td><?php  ?></td>
				</tr>
				<tr>
					<td><b>Calle</b></td>
					<td><?php  ?></td>
					<td><b>Entre Calle</b></td>
					<td><?php  ?></td>
					<td><b>Y Calle</b></td>
					<td><?php  ?></td>
				</tr>
				<tr>
					<td><b>A la Altura de</b></td>
					<td colspan="5"><?php  ?></td>
				</tr>
			</tbody>
			<tbody>
				<tr>
					<th colspan="6"><b>CUANDO:</b></th>
				</tr>
				<tr>
					<td><b>Fecha del Procesamiento</b></td>
					<td colspan="2"><?php  ?></td>
					<td><b>Hora del Procesamiento</b></td>
					<td colspan="2"><?php  ?></td>
				</tr>
				<tr>
					<td><b>Detenido en</b></td>
					<td colspan="2"><?php  ?></td>
					<td><b>Corporacion que intervino</b></td>
					<td colspan="2"><?php  ?></td>
				</tr>
			</tbody>
			<tbody>
				<tr>
					
				</tr>
			</tbody>
		</table>

<br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br>
<p style="text-align: center; padding: 45%;">
	Pagina <?php echo $i+1 ?> de <?php echo count($arraId) ?>
</p>
<?php } ?>
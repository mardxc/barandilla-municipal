<?php 
	function checados($check){
		if($check==1) return true; else false;// Revisa si los checks estan marcados
	}
 ?>
 <?php 
	 setlocale(LC_ALL,"es_ES");
	 $pdf = Yii::createComponent('application.extensions.MPDF57.mpdf');
	 $html='
<link rel="stylesheet" type="text/css" href="'.Yii::app()->theme->baseUrl.'/css/bootstrap.min.css">
<style>
	table,th,tr{
		text-align: center;
	}
	td img{
		margin-left: 25%;
		width: 50%;
	}
	td h1{
		text-align: initial;
	}
</style>
<meta charset="utf-8">
	<table>
		<tr>
			<td>
							
			</td>
			<td>
				<h1>DIRECCIÓN DE SEGURIDAD PÚBLICA MUNICIPAL DE RIOVERDE, S.L.P.</h1>				
			</td>
		</tr>
	</table>
	</div>
	<table class="table table-hover table-striped">
		<thead>
			<tr>
				<th colspan="6">REPORTE POLICIAL</th>
			</tr>
		</thead>
		<tbody>
			<tr>
				<td colspan="6"><b>Datos del Detenido</b></td>
			</tr>
			<tr>
				<td>Nombre</td>
				<td colspan="5">'.$persona->nombre.' '.$persona->ape_pat.' '.$persona->ape_mat.'</td>
			</tr>
			<tr>
				<td>Domicilio</td>
				<td>Calle: </td>
				<td>'.$domicilio->calle .'</td>
				<td>Colonia</td>
				<td>'. $domicilio->id_colonia.'</td>
			</tr>
			<tr>
				<td>Sexo</td>
				<td>'. $persona->sexo.'</td>
				<td>Edad</td>
				<td>'. $persona->edad.'</td>
				<td>Estatura</td>
				<td>'. $descripcionFisica->estatura.'</td>
			</tr>
			<tr>
				<td>Color de Piel</td>
				<td>'. $descripcionFisica->color_piel.'</td>
				<td>Color de Pelo</td>
				<td>'. $descripcionFisica->color_pelo.'</td>
				<td>Color de Ojos</td>
				<td>'. $descripcionFisica->color_ojos.'</td>
			</tr>
			<tr>
				<td>Complexion</td>
				<td>'. $descripcionFisica->complexion .'</td>
				<td>Menton</td>
				<td>'. $descripcionFisica->menton .'</td>
				<td>Frente</td>
				<td>'. $descripcionFisica->frente .'</td>
			</tr>
			<tr>
				<td>Cejas</td>
				<td>'. $descripcionFisica->cejas .'</td>.
				<td>Señas Particulares</td>
				<td colspan="2">'. $persona->señas_particulares .'</td>
			</tr>
			<tr>
				<th colspan="6">Objetos que Porta</th>
			</tr>
			<tr>
				<td>'. CHtml::checkBox('camisa',$objetosPorta->camisa) .' Camisa</td>
				<td>'. CHtml::checkBox('pantalon',$objetosPorta->pantalon) .' Pantalon</td>
				<td>'. CHtml::checkBox('playera',$objetosPorta->playera) .' Playera</td>
				<td>'. CHtml::checkBox('chamarra',$objetosPorta->chamarra) .' Chamarra</td>
				<td>'. CHtml::checkBox('tenis',$objetosPorta->tenis) .' Tenis</td>
			</tr>
			<tr>
				<td>'. CHtml::checkBox('zapatos',$objetosPorta->zapatos) .' Zapatos</td>
				<td>'. CHtml::checkBox('botas',$objetosPorta->botas) .' Botas</td>
				<td>'. CHtml::checkBox('sombrero',$objetosPorta->sombrero) .' Sombrero</td>
				<td>'. CHtml::checkBox('cadena',$objetosPorta->cadena) .' Cadena</td>
				<td>'. CHtml::checkBox('pulsera',$objetosPorta->pulsera) .' Pulsera</td>

			</tr>
			<tr>
				<td>'. CHtml::checkBox('gorra',$objetosPorta->gorra) .' Gorra</td>
				<td>'. CHtml::checkBox('cinturon',$objetosPorta->cinturon) .' Cinturon</td>
				<td>'. CHtml::checkBox('cartera',$objetosPorta->cartera) .' Cartera</td>
				<td>'. CHtml::checkBox('lentes',$objetosPorta->lentes) .' Lentes</td>
			</tr>
			<tr>
				<td>'. CHtml::checkBox('otros',$objetosPorta->otros) .' Otros</td>
				<td colspan="4">'. $objetosPorta->descripcion .'</td>
			</tr>
		</tbody>
		<tbody>
			<tr>
				<td colspan="6"><b>DONDE</b></td>
			</tr>
			<tr>
				<td>Ciudad:</td>
				<td><?php  ?></td>
				<td>Colonia</td>
				<td><?php  ?></td>
				<td>Zona</td>
				<td><?php  ?></td>
			</tr>
			<tr>
				<td>Calle</td>
				<td><?php  ?></td>
				<td>Entre Calle</td>
				<td><?php  ?></td>
				<td>Y Calle</td>
				<td><?php  ?></td>
			</tr>
			<tr>
				<td>A la Altura de</td>
				<td colspan="4"><?php  ?></td>
			</tr>
		</tbody>
		<tbody>
			<tr>
				<td colspan="6"><b>CUANDO:</b></td>
			</tr>
			<tr>
				<td>Fecha del Procesamiento</td>
				<td colspan="2"><?php  ?></td>
				<td>Hora del Procesamiento</td>
				<td colspan="2"><?php  ?></td>
			</tr>
			<tr>
				<td>Detenido en</td>
				<td colspan="2"><?php  ?></td>
				<td>Corporacion que intervino</td>
				<td colspan="2"><?php  ?></td>
			</tr>
		</tbody>
		<tbody>
			<tr>
				
			</tr>
		</tbody>
	</table>

';
$mpdf=new mPDF('win-1252','LETTER','','',15,15,25,12,5,7);
$mpdf->WriteHTML($html);
$mpdf->Output('Ficha-Detenido.pdf','D');
exit;
?>
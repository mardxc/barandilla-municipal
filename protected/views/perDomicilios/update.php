<?php
/* @var $this PerDomiciliosController */
/* @var $model PerDomicilios */

$this->breadcrumbs=array(
	'Per Domicilioses'=>array('index'),
	$model->id_domicilio=>array('view','id'=>$model->id_domicilio),
	'Update',
);

$this->menu=array(
	array('label'=>'List PerDomicilios', 'url'=>array('index')),
	array('label'=>'Create PerDomicilios', 'url'=>array('create')),
	array('label'=>'View PerDomicilios', 'url'=>array('view', 'id'=>$model->id_domicilio)),
	array('label'=>'Manage PerDomicilios', 'url'=>array('admin')),
);
?>

<h1>Update PerDomicilios <?php echo $model->id_domicilio; ?></h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>
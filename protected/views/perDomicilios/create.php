<?php
/* @var $this PerDomiciliosController */
/* @var $model PerDomicilios */

$this->breadcrumbs=array(
	'Per Domicilioses'=>array('index'),
	'Create',
);

$this->menu=array(
	array('label'=>'List PerDomicilios', 'url'=>array('index')),
	array('label'=>'Manage PerDomicilios', 'url'=>array('admin')),
);
?>

<h1>Create PerDomicilios</h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>
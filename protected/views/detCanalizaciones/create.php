<?php
/* @var $this DetCanalizacionesController */
/* @var $model DetCanalizaciones */

$this->breadcrumbs=array(
	'Det Canalizaciones'=>array('index'),
	'Create',
);

$this->menu=array(
	array('label'=>'List DetCanalizaciones', 'url'=>array('index')),
	array('label'=>'Manage DetCanalizaciones', 'url'=>array('admin')),
);
?>

<h1>Create DetCanalizaciones</h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>
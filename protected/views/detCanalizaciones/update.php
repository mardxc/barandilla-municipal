<?php
/* @var $this DetCanalizacionesController */
/* @var $model DetCanalizaciones */

$this->breadcrumbs=array(
	'Det Canalizaciones'=>array('index'),
	$model->id_canalizacion=>array('view','id'=>$model->id_canalizacion),
	'Update',
);

$this->menu=array(
	array('label'=>'List DetCanalizaciones', 'url'=>array('index')),
	array('label'=>'Create DetCanalizaciones', 'url'=>array('create')),
	array('label'=>'View DetCanalizaciones', 'url'=>array('view', 'id'=>$model->id_canalizacion)),
	array('label'=>'Manage DetCanalizaciones', 'url'=>array('admin')),
);
?>

<h1>Update DetCanalizaciones <?php echo $model->id_canalizacion; ?></h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>
<?php

/**
 * This is the model class for table "det_condiciones_fisicas".
 *
 * The followings are the available columns in table 'det_condiciones_fisicas':
 * @property integer $id_condicion
 * @property string $condicion
 *
 * The followings are the available model relations:
 * @property DetDetenciones[] $detDetenciones
 */
class DetCondicionesFisicas extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return DetCondicionesFisicas the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'det_condiciones_fisicas';
	}

	public static function getListCondicion(){
		return CHtml::listData(DetCondicionesFisicas::model()->findAll(),'id_condicion','condicion');
	}
	public static function getCondicion($id_condicion){
		$ciclo=DetCondicionesFisicas::model()->find('id_condicion='.$id_condicion);
		return $ciclo['condicion'];
	}
	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('condicion', 'required'),
			array('condicion', 'length', 'max'=>20),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('id_condicion, condicion', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'detDetenciones' => array(self::HAS_MANY, 'DetDetenciones', 'id_condicion'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id_condicion' => 'Id Condicion',
			'condicion' => 'Condicion',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id_condicion',$this->id_condicion);
		$criteria->compare('condicion',$this->condicion,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
}
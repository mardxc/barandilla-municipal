<?php

/**
 * This is the model class for table "det_motivos".
 *
 * The followings are the available columns in table 'det_motivos':
 * @property integer $id_det_motivos
 * @property integer $id_motivo
 * @property integer $id_detencion
 *
 * The followings are the available model relations:
 * @property BasMotivos $idMotivo
 * @property DetDetenciones $idDetencion
 */
class DetMotivos extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return DetMotivos the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
	public static function dataProMotivo($id_detencion)
	{
		$sql="SELECT DISTINCT
			(dm.id_det_motivos),
		    bm.motivo AS motivo,
		    dm.id_detencion
		FROM
		    det_motivos dm
		inner join
		bas_motivos bm on dm.id_motivo=bm.id_motivo
		WHERE
		    id_detencion = $id_detencion";

		$dataProMotivo=new CSqlDataProvider($sql,
			array(
				'keyField'=>'id_det_motivos',
				'pagination'=>array('pageSize'=>3,)
			)
		);
	}
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'det_motivos';
	}

	public static function listMotivos($id_detencion){
		$sql="SELECT 
				*
			FROM
				det_motivos
				WHERE
				id_detencion=$id_detencion
			ORDER BY id_det_motivos DESC";
		$dataProMotivo=DetMotivos::model()->findBySql($sql);
		return $dataProMotivo;
	}
	public static function getMotivo($id_detencion)
	{
		$res=DetMotivos::model()->find('id_detencion='.$id_detencion);
		//$ress=BasMotivos::model()->find('id_motivo='.$res['id_motivo']);
		return $res['id_motivo'] ;
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('id_motivo, id_detencion', 'required'),
			array('id_motivo, id_detencion', 'numerical', 'integerOnly'=>true),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('id_det_motivos, id_motivo, id_detencion', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'idMotivo' => array(self::BELONGS_TO, 'BasMotivos', 'id_motivo'),
			'idDetencion' => array(self::BELONGS_TO, 'DetDetenciones', 'id_detencion'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id_det_motivos' => 'Id Det Motivos',
			'id_motivo' => 'Id Motivo',
			'id_detencion' => 'Id Detencion',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id_det_motivos',$this->id_det_motivos);
		$criteria->compare('id_motivo',$this->id_motivo);
		$criteria->compare('id_detencion',$this->id_detencion);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
}
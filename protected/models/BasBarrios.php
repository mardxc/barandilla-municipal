<?php

/**
 * This is the model class for table "bas_barrios".
 *
 * The followings are the available columns in table 'bas_barrios':
 * @property integer $id_barrio
 * @property string $barrio
 * @property integer $id_colonia
 * @property integer $id_municipio
 *
 * The followings are the available model relations:
 * @property GenMunicipio $idMunicipio
 * @property DetLugarAseguramiento[] $detLugarAseguramientos
 */
class BasBarrios extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return BasBarrios the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'bas_barrios';
	}

	public static function getListBarrio(){
			return CHtml::listData(BasBarrios::model()->findAll(),'id_barrio','barrio');
		}
		
	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('barrio, id_colonia, id_municipio', 'required'),
			array('id_colonia, id_municipio', 'numerical', 'integerOnly'=>true),
			array('barrio', 'length', 'max'=>30),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('id_barrio, barrio, id_colonia, id_municipio', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'idMunicipio' => array(self::BELONGS_TO, 'GenMunicipio', 'id_municipio'),
			'detLugarAseguramientos' => array(self::HAS_MANY, 'DetLugarAseguramiento', 'id_barrio'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id_barrio' => 'Id Barrio',
			'barrio' => 'Barrio',
			'id_colonia' => 'Id Colonia',
			'id_municipio' => 'Id Municipio',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id_barrio',$this->id_barrio);
		$criteria->compare('barrio',$this->barrio,true);
		$criteria->compare('id_colonia',$this->id_colonia);
		$criteria->compare('id_municipio',$this->id_municipio);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
}
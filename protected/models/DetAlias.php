<?php

/**
 * This is the model class for table "det_alias".
 *
 * The followings are the available columns in table 'det_alias':
 * @property integer $id_det_alias
 * @property integer $id_persona
 * @property string $alias
 */
class DetAlias extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return DetAlias the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'det_alias';
	}
	public static function dataProALias($id_persona)
	{
		$sql="SELECT 
		    da.alias,
		    da.id_det_alias
		FROM
		    det_alias da
		WHERE
		    da.id_persona=$id_persona
			ORDER BY id_det_alias DESC";
		$dataProALias=new CSqlDataProvider($sql,
			array(
				'keyField'=>'id_det_alias',
				'pagination'=>array('pageSize'=>3,)
			)
		);
		return $dataProALias;
	}
	public static function listAlias($id_persona){
		$sql="SELECT 
		    da.alias,
		    da.id_det_alias
		FROM
		    det_alias da
		WHERE
		    da.id_persona=$id_persona
			ORDER BY id_det_alias DESC";
		$dataProALias=DetAlias::model()->findBySql($sql);
		return $dataProALias['alias'];
	}
	public static function getAlias($id_persona)
	{
		$res=DetAlias::model()->find('id_persona='.$id_persona);
		return $res['alias'];
	}
	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('id_persona, alias', 'required'),
			array('id_persona', 'numerical', 'integerOnly'=>true),
			array('alias', 'length', 'max'=>45),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('id_det_alias, id_persona, alias', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id_det_alias' => 'Id Det Alias',
			'id_persona' => 'Id Persona',
			'alias' => 'Alias',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id_det_alias',$this->id_det_alias);
		$criteria->compare('id_persona',$this->id_persona);
		$criteria->compare('alias',$this->alias,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
}
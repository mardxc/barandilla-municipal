<?php

/**
 * This is the model class for table "det_objetos".
 *
 * The followings are the available columns in table 'det_objetos':
 * @property integer $id_objeto
 * @property string $objeto
 * @property string $caracteristicas
 * @property integer $id_detencion
 *
 * The followings are the available model relations:
 * @property DetDetencionesAfectado[] $detDetencionesAfectados
 */
class DetObjetos extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return DetObjetos the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'det_objetos';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('objeto, caracteristicas, id_detencion', 'required'),
			array('id_detencion', 'numerical', 'integerOnly'=>true),
			array('objeto', 'length', 'max'=>15),
			array('caracteristicas', 'length', 'max'=>30),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('id_objeto, objeto, caracteristicas, id_detencion', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'detDetencionesAfectados' => array(self::HAS_MANY, 'DetDetencionesAfectado', 'id_objetos'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id_objeto' => 'Id Objeto',
			'objeto' => 'Objeto',
			'caracteristicas' => 'Caracteristicas',
			'id_detencion' => 'Id Detencion',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id_objeto',$this->id_objeto);
		$criteria->compare('objeto',$this->objeto,true);
		$criteria->compare('caracteristicas',$this->caracteristicas,true);
		$criteria->compare('id_detencion',$this->id_detencion);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
}
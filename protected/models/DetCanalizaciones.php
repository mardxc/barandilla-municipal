<?php

/**
 * This is the model class for table "det_canalizaciones".
 *
 * The followings are the available columns in table 'det_canalizaciones':
 * @property integer $id_canalizacion
 * @property integer $juez_calificador
 * @property integer $fuero_comun
 * @property integer $transito_municipal
 * @property integer $tutelar_menores
 * @property integer $migracion
 * @property string $otro
 * @property integer $mixta
 * @property integer $fuero_federal
 *
 * The followings are the available model relations:
 * @property DetDetenciones[] $detDetenciones
 */
class DetCanalizaciones extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return DetCanalizaciones the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'det_canalizaciones';
	}
	public static function trueOrFalse($value){
		if($value==="true") return 1; else return 0;
	}
	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('juez_calificador, fuero_comun, transito_municipal, tutelar_menores, migracion, mixta, fuero_federal', 'numerical', 'integerOnly'=>true),
			array('otro', 'length', 'max'=>45),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('id_canalizacion, juez_calificador, fuero_comun, transito_municipal, tutelar_menores, migracion, otro, mixta, fuero_federal', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'detDetenciones' => array(self::HAS_MANY, 'DetDetenciones', 'id_canalizacion'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id_canalizacion' => 'Id Canalizacion',
			'juez_calificador' => 'Juez Calificador',
			'fuero_comun' => 'Fuero Comun',
			'transito_municipal' => 'Transito Municipal',
			'tutelar_menores' => 'Tutelar Menores',
			'migracion' => 'Migracion',
			'otro' => 'Otro',
			'mixta' => 'Mixta',
			'fuero_federal' => 'Fuero Federal',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id_canalizacion',$this->id_canalizacion);
		$criteria->compare('juez_calificador',$this->juez_calificador);
		$criteria->compare('fuero_comun',$this->fuero_comun);
		$criteria->compare('transito_municipal',$this->transito_municipal);
		$criteria->compare('tutelar_menores',$this->tutelar_menores);
		$criteria->compare('migracion',$this->migracion);
		$criteria->compare('otro',$this->otro,true);
		$criteria->compare('mixta',$this->mixta);
		$criteria->compare('fuero_federal',$this->fuero_federal);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
}
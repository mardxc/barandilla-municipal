<?php

/**
 * This is the model class for table "det_dinero".
 *
 * The followings are the available columns in table 'det_dinero':
 * @property integer $id_dinero
 * @property integer $billetes
 * @property string $monedas
 * @property string $dolares
 * @property string $otros
 * @property string $total
 *
 * The followings are the available model relations:
 * @property DetDetenciones[] $detDetenciones
 */
class DetDinero extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return DetDinero the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'det_dinero';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('total', 'required'),
			array('billetes', 'numerical', 'integerOnly'=>true),
			array('monedas, dolares, otros, total', 'length', 'max'=>8),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('id_dinero, billetes, monedas, dolares, otros, total', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'detDetenciones' => array(self::HAS_MANY, 'DetDetenciones', 'id_dinero'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id_dinero' => 'Id Dinero',
			'billetes' => 'Billetes',
			'monedas' => 'Monedas',
			'dolares' => 'Dolares',
			'otros' => 'Otros',
			'total' => 'Total',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id_dinero',$this->id_dinero);
		$criteria->compare('billetes',$this->billetes);
		$criteria->compare('monedas',$this->monedas,true);
		$criteria->compare('dolares',$this->dolares,true);
		$criteria->compare('otros',$this->otros,true);
		$criteria->compare('total',$this->total,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
}
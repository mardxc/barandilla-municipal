<?php

/**
 * This is the model class for table "det_bandas".
 *
 * The followings are the available columns in table 'det_bandas':
 * @property integer $id_bandas
 * @property string $banda
 * @property string $colonia
 *
 * The followings are the available model relations:
 * @property DetDetenciones[] $detDetenciones
 */
class DetBandas extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return DetBandas the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'det_bandas';
	}
	public static function getListBanda(){
		return CHtml::listData(DetBandas::model()->findAll(),'id_bandas','banda');
	}
	public static function getNameBanda($id_bandas){
		$banda=DetBandas::model()->find('id_bandas='.$id_bandas);
		return $banda['banda'];
	}
	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('banda, colonia', 'required'),
			array('banda', 'length', 'max'=>30),
			array('colonia', 'length', 'max'=>20),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('id_bandas, banda, colonia', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'detDetenciones' => array(self::HAS_MANY, 'DetDetenciones', 'id_banda'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id_bandas' => 'Id Bandas',
			'banda' => 'Banda',
			'colonia' => 'Colonia',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id_bandas',$this->id_bandas);
		$criteria->compare('banda',$this->banda,true);
		$criteria->compare('colonia',$this->colonia,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
}
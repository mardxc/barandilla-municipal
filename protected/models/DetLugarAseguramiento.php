<?php

/**
 * This is the model class for table "det_lugar_aseguramiento".
 *
 * The followings are the available columns in table 'det_lugar_aseguramiento':
 * @property integer $id_lugar
 * @property integer $id_colonia
 * @property string $calle
 * @property integer $num_int
 * @property integer $num_ext
 * @property string $entre_calle
 * @property string $y_la_calle
 * @property string $a_la_altura
 * @property integer $id_barrio
 *
 * The followings are the available model relations:
 * @property DetDetenciones[] $detDetenciones
 * @property BasBarrios $idBarrio
 */
class DetLugarAseguramiento extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return DetLugarAseguramiento the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'det_lugar_aseguramiento';
	}
	
	public static function getLugarAseguramiento(){
			$sql="SELECT 
				    id_lugar
				FROM
				    det_lugar_aseguramiento
				ORDER BY id_lugar DESC
				LIMIT 1";
				$lugar=DetLugarAseguramiento::model()->findBySql($sql);
				return $lugar['id_lugar'];
		}
	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('id_colonia, num_int, num_ext, id_barrio', 'numerical', 'integerOnly'=>true),
			array('calle, entre_calle, y_la_calle, a_la_altura', 'length', 'max'=>30),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('id_lugar, id_colonia, calle, num_int, num_ext, entre_calle, y_la_calle, a_la_altura, id_barrio', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'detDetenciones' => array(self::HAS_MANY, 'DetDetenciones', 'id_lugar'),
			'idBarrio' => array(self::BELONGS_TO, 'BasBarrios', 'id_barrio'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id_lugar' => 'Id Lugar',
			'id_colonia' => 'Id Colonia',
			'calle' => 'Calle',
			'num_int' => 'Num Int',
			'num_ext' => 'Num Ext',
			'entre_calle' => 'Entre Calle',
			'y_la_calle' => 'Y La Calle',
			'a_la_altura' => 'A La Altura',
			'id_barrio' => 'Id Barrio',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id_lugar',$this->id_lugar);
		$criteria->compare('id_colonia',$this->id_colonia);
		$criteria->compare('calle',$this->calle,true);
		$criteria->compare('num_int',$this->num_int);
		$criteria->compare('num_ext',$this->num_ext);
		$criteria->compare('entre_calle',$this->entre_calle,true);
		$criteria->compare('y_la_calle',$this->y_la_calle,true);
		$criteria->compare('a_la_altura',$this->a_la_altura,true);
		$criteria->compare('id_barrio',$this->id_barrio);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
}
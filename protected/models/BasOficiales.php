<?php

/**
 * This is the model class for table "bas_oficiales".
 *
 * The followings are the available columns in table 'bas_oficiales':
 * @property integer $id_oficial
 * @property string $grado
 * @property string $nombre
 * @property string $ape_pat
 * @property string $ape_mat
 * @property integer $telefono
 * @property string $sexo
 * @property string $fecha_nacimiento
 * @property string $estado_civil
 * @property string $email
 *
 * The followings are the available model relations:
 * @property DetDetencionesOficiales[] $detDetencionesOficiales
 */
class BasOficiales extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return BasOficiales the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	public function beforeSave(){
		if ($this->fecha_nacimiento <> '') {
			list($d2, $m2, $y2) = explode('/', $this->fecha_nacimiento);
			$mk2=mktime(0, 0, 0, $m2, $d2, $y2);
			$this->fecha_nacimiento = date ('Y-m-d', $mk2);
		}
		return parent::beforeSave();
	}

	public function afterFind(){
		if ($this->fecha_nacimiento <> '') {
			list ($y, $m, $d) = explode('-', $this->fecha_nacimiento);
			$mk=mktime(0, 0, 0, $m, $d, $y);
			$this->fecha_nacimiento = date ('d/m/Y', $mk);
		}
		return parent::afterFind();
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'bas_oficiales';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('grado, nombre, ape_pat, ape_mat, telefono, sexo, fecha_nacimiento, estado_civil, email', 'required'),
			array('telefono', 'numerical', 'integerOnly'=>true),
			array('telefono','length','max'=>10),
			array('grado', 'length', 'max'=>15),
			array('nombre, ape_pat, ape_mat, email', 'length', 'max'=>20),
			array('sexo, estado_civil', 'length', 'max'=>10),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('id_oficial, grado, nombre, ape_pat, ape_mat, telefono, sexo, fecha_nacimiento, estado_civil, email', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'detDetencionesOficiales' => array(self::HAS_MANY, 'DetDetencionesOficiales', 'id_oficial'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id_oficial' => 'Id Oficial',
			'grado' => 'Grado',
			'nombre' => 'Nombre',
			'ape_pat' => 'Ape Pat',
			'ape_mat' => 'Ape Mat',
			'telefono' => 'Telefono',
			'sexo' => 'Sexo',
			'fecha_nacimiento' => 'Fecha Nacimiento',
			'estado_civil' => 'Estado Civil',
			'email' => 'Email',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id_oficial',$this->id_oficial);
		$criteria->compare('grado',$this->grado,true);
		$criteria->compare('nombre',$this->nombre,true);
		$criteria->compare('ape_pat',$this->ape_pat,true);
		$criteria->compare('ape_mat',$this->ape_mat,true);
		$criteria->compare('telefono',$this->telefono);
		$criteria->compare('sexo',$this->sexo,true);
		$criteria->compare('fecha_nacimiento',$this->fecha_nacimiento,true);
		$criteria->compare('estado_civil',$this->estado_civil,true);
		$criteria->compare('email',$this->email,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
}
<?php

/**
 * This is the model class for table "bas_colonia".
 *
 * The followings are the available columns in table 'bas_colonia':
 * @property integer $id_colonia
 * @property string $colonia
 */
class BasColonia extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return BasColonia the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'bas_colonia';
	}
	public static function getListColonia(){
		return CHtml::listData(BasColonia::model()->findAll(),'id_colonia','colonia');
	}
	public static function getColonia($id_colonia){
		$ciclo=BasColonia::model()->find('id_colonia='.$id_colonia);
		return $ciclo['colonia'];
	}
	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('colonia', 'required'),
			array('colonia', 'length', 'max'=>45),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('id_colonia, colonia', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id_colonia' => 'Id Colonia',
			'colonia' => 'Colonia',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id_colonia',$this->id_colonia);
		$criteria->compare('colonia',$this->colonia,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
}
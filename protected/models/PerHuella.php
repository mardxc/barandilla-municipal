<?php

/**
 * This is the model class for table "per_huella".
 *
 * The followings are the available columns in table 'per_huella':
 * @property integer $id_huella
 * @property string $huella
 * @property integer $id_persona
 */
class PerHuella extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return PerHuella the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'per_huella';
	}

	public static function getHuella(){
		$sql="SELECT 
			    id_persona
			FROM
			    per_huella ph
			ORDER BY ph.id_huella DESC
			LIMIT 1";
		$dataProMotivo=PerHuella::model()->findBySql($sql);
		return $dataProMotivo['id_persona'];
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('huella, id_persona', 'required'),
			array('id_persona', 'numerical', 'integerOnly'=>true),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('id_huella, huella, id_persona', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id_huella' => 'Id Huella',
			'huella' => 'Huella',
			'id_persona' => 'Id Persona',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id_huella',$this->id_huella);
		$criteria->compare('huella',$this->huella,true);
		$criteria->compare('id_persona',$this->id_persona);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
}
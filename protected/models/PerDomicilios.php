<?php

/**
 * This is the model class for table "per_domicilios".
 *
 * The followings are the available columns in table 'per_domicilios':
 * @property integer $id_domicilio
 * @property string $calle
 * @property integer $id_colonia
 * @property string $fecha
 * @property integer $id_municipio
 * @property integer $id_persona
 * @property string $status
 *
 * The followings are the available model relations:
 * @property GenMunicipio $idMunicipio
 * @property PerPersonas $idPersona
 */
class PerDomicilios extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return PerDomicilios the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'per_domicilios';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('id_colonia, id_municipio, id_persona', 'numerical', 'integerOnly'=>true),
			array('calle, status', 'length', 'max'=>30),
			array('fecha', 'safe'),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('id_domicilio, calle, id_colonia, fecha, id_municipio, id_persona, status', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'idMunicipio' => array(self::BELONGS_TO, 'GenMunicipio', 'id_municipio'),
			'idPersona' => array(self::BELONGS_TO, 'PerPersonas', 'id_persona'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id_domicilio' => 'Id Domicilio',
			'calle' => 'Calle',
			'id_colonia' => 'Id Colonia',
			'fecha' => 'Fecha',
			'id_municipio' => 'Id Municipio',
			'id_persona' => 'Id Persona',
			'status' => 'Status',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id_domicilio',$this->id_domicilio);
		$criteria->compare('calle',$this->calle,true);
		$criteria->compare('id_colonia',$this->id_colonia);
		$criteria->compare('fecha',$this->fecha,true);
		$criteria->compare('id_municipio',$this->id_municipio);
		$criteria->compare('id_persona',$this->id_persona);
		$criteria->compare('status',$this->status,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
}
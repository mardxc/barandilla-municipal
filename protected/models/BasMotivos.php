<?php

/**
 * This is the model class for table "bas_motivos".
 *
 * The followings are the available columns in table 'bas_motivos':
 * @property integer $id_motivo
 * @property string $motivo
 * @property string $descripcion
 * @property string $gravedad
 *
 * The followings are the available model relations:
 * @property DetMotivos[] $detMotivoses
 */
class BasMotivos extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return BasMotivos the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'bas_motivos';
	}
	public static function getMotivo($id_motivo)
	{
		$sql="SELECT 
			    motivo
			FROM
			    bas_motivos
			  WHERE
			  	id_motivo=$id_motivo
			ORDER BY id_motivo DESC
			LIMIT 1";
			$motivo=BasMotivos::model()->findBySql($sql);
			return $motivo['motivo'];

	}
	public static function getMotivos($id_motivo)
	{
		$res=BasMotivos::model()->find('id_motivo='.$id_motivo);
		//$ress=BasMotivos::model()->find('id_motivo='.$res['id_motivo']);
		return $res['motivo'] ;
	}
	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('motivo, descripcion, gravedad', 'required'),
			array('motivo, descripcion, gravedad', 'length', 'max'=>20),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('id_motivo, motivo, descripcion, gravedad', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'detMotivoses' => array(self::HAS_MANY, 'DetMotivos', 'id_motivo'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id_motivo' => 'Id Motivo',
			'motivo' => 'Motivo',
			'descripcion' => 'Descripcion',
			'gravedad' => 'Gravedad',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public static function getListMotivos(){
		return CHtml::listData(BasMotivos::model()->findAll(),'id_motivo','motivo');
	}
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id_motivo',$this->id_motivo);
		$criteria->compare('motivo',$this->motivo,true);
		$criteria->compare('descripcion',$this->descripcion,true);
		$criteria->compare('gravedad',$this->gravedad,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
}
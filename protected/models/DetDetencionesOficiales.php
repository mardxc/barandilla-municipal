<?php

/**
 * This is the model class for table "det_detenciones_oficiales".
 *
 * The followings are the available columns in table 'det_detenciones_oficiales':
 * @property integer $id_det_oficiales
 * @property string $responsable
 * @property integer $id_oficial
 * @property integer $id_detencion
 *
 * The followings are the available model relations:
 * @property DetDetenciones $idDetencion
 * @property BasOficiales $idOficial
 */
class DetDetencionesOficiales extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return DetDetencionesOficiales the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'det_detenciones_oficiales';
	}
	public static function dataProOficiales($id_detencion)
	{
		$sql="SELECT DISTINCT
		    (ddo.id_det_oficiales),
		    ddo.responsable,
		    CONCAT(bo.nombre,
		            ' ',
		            bo.ape_pat,
		            ' ',
		            bo.ape_mat) AS nombre,
		    ddo.id_detencion
		FROM
		    det_detenciones_oficiales ddo
		        INNER JOIN
		    bas_oficiales bo ON ddo.id_oficial = bo.id_oficial
		WHERE
		    id_detencion = $id_detencion";

		$dataProOficiales=new CSqlDataProvider($sql,
			array(
				'keyField'=>'id_detencion',
				'pagination'=>array('pageSize'=>3,)
			)
		);
	}
	/**
	 * @return array validation rules for model attributes.
	 */
	public static function getOficial($id_oficial)
	{
		$res=BasOficiales::model()->find('id_oficial='.$id_oficial);
		return $res['nombre'] . ' ' . $res['ape_pat'] . ' ' . $res['ape_mat'];
	}
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('responsable, id_oficial, id_detencion', 'required'),
			array('id_oficial, id_detencion', 'numerical', 'integerOnly'=>true),
			array('responsable', 'length', 'max'=>30),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('id_det_oficiales, responsable, id_oficial, id_detencion', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'idDetencion' => array(self::BELONGS_TO, 'DetDetenciones', 'id_detencion'),
			'idOficial' => array(self::BELONGS_TO, 'BasOficiales', 'id_oficial'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id_det_oficiales' => 'Id Det Oficiales',
			'responsable' => 'Responsable',
			'id_oficial' => 'Id Oficial',
			'id_detencion' => 'Id Detencion',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id_det_oficiales',$this->id_det_oficiales);
		$criteria->compare('responsable',$this->responsable,true);
		$criteria->compare('id_oficial',$this->id_oficial);
		$criteria->compare('id_detencion',$this->id_detencion);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
}
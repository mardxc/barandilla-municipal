<?php

/**
 * This is the model class for table "gen_municipio".
 *
 * The followings are the available columns in table 'gen_municipio':
 * @property integer $id_municipio
 * @property string $municipio
 * @property integer $id_estado
 *
 * The followings are the available model relations:
 * @property BasBarrios[] $basBarrioses
 * @property DetDetenciones[] $detDetenciones
 * @property GenLocalidad[] $genLocalidads
 * @property GenEstado $idEstado
 * @property PerDomicilios[] $perDomicilioses
 */
class GenMunicipio extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return GenMunicipio the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'gen_municipio';
	}
	public static function getMunicipio($id_estado){
		/*$sql="SELECT DISTINCT
			    (gm.id_municipio),
			    gm.municipio
			FROM
			    gen_municipio gm,
			    gen_estado ge
			WHERE
			    gm.id_estado = $id_estado";
			$estado=GenMunicipio::model()->findBySql($sql);
			return $estado;*/
			$ciclo=GenMunicipio::model()->find('id_estado='.$id_estado);
			return $ciclo;
	}
	public static function getListMunicipio(){
		return CHtml::listData(GenMunicipio::model()->findAll(),'id_municipio','municipio');
	}
	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('municipio, id_estado', 'required'),
			array('id_estado', 'numerical', 'integerOnly'=>true),
			array('municipio', 'length', 'max'=>30),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('id_municipio, municipio, id_estado', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'basBarrioses' => array(self::HAS_MANY, 'BasBarrios', 'id_municipio'),
			'detDetenciones' => array(self::HAS_MANY, 'DetDetenciones', 'id_municipio'),
			'genLocalidads' => array(self::HAS_MANY, 'GenLocalidad', 'id_municipio'),
			'idEstado' => array(self::BELONGS_TO, 'GenEstado', 'id_estado'),
			'perDomicilioses' => array(self::HAS_MANY, 'PerDomicilios', 'id_municipio'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id_municipio' => 'Id Municipio',
			'municipio' => 'Municipio',
			'id_estado' => 'Id Estado',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id_municipio',$this->id_municipio);
		$criteria->compare('municipio',$this->municipio,true);
		$criteria->compare('id_estado',$this->id_estado);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
}
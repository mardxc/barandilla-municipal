<?php

/**
 * This is the model class for table "bas_corporacion".
 *
 * The followings are the available columns in table 'bas_corporacion':
 * @property integer $id_corporacion
 * @property string $corporacion
 * @property string $descripcion
 *
 * The followings are the available model relations:
 * @property DetDetenciones[] $detDetenciones
 */
class BasCorporacion extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return BasCorporacion the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'bas_corporacion';
	}
	public static function getCorporacion($id_corporacion){
		$ciclo=BasCorporacion::model()->find('id_corporacion='.$id_corporacion);
		return $ciclo['corporacion'];
	}
	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('corporacion, descripcion', 'required'),
			array('corporacion, descripcion', 'length', 'max'=>30),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('id_corporacion, corporacion, descripcion', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'detDetenciones' => array(self::HAS_MANY, 'DetDetenciones', 'id_corporacion'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id_corporacion' => 'Id Corporacion',
			'corporacion' => 'Corporacion',
			'descripcion' => 'Descripcion',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public static function getListCorporacion(){
		return CHtml::listData(BasCorporacion::model()->findAll(),'id_corporacion','corporacion');
	}
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id_corporacion',$this->id_corporacion);
		$criteria->compare('corporacion',$this->corporacion,true);
		$criteria->compare('descripcion',$this->descripcion,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
}
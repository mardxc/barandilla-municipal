<?php

/**
 * This is the model class for table "det_objetos_porta".
 *
 * The followings are the available columns in table 'det_objetos_porta':
 * @property integer $id_objetos_porta
 * @property string $descripcion
 * @property integer $camisa
 * @property integer $pantalon
 * @property integer $playera
 * @property integer $chamarra
 * @property integer $tenis
 * @property integer $zapatos
 * @property integer $botas
 * @property integer $sombrero
 * @property integer $cadena
 * @property integer $pulsera
 * @property integer $gorra
 * @property integer $cinturon
 * @property integer $cartera
 * @property integer $lentes
 * @property integer $otros
 *
 * The followings are the available model relations:
 * @property DetDetenciones[] $detDetenciones
 */
class DetObjetosPorta extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return DetObjetosPorta the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'det_objetos_porta';
	}
	public static function trueOrFalse($value){
		if($value==="true") return 1; else return 0;
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('camisa, pantalon, playera, chamarra, tenis, zapatos, botas, sombrero, cadena, pulsera, gorra, cinturon, cartera, lentes, otros', 'numerical', 'integerOnly'=>true),
			array('descripcion', 'length', 'max'=>1000),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('id_objetos_porta, descripcion, camisa, pantalon, playera, chamarra, tenis, zapatos, botas, sombrero, cadena, pulsera, gorra, cinturon, cartera, lentes, otros', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'detDetenciones' => array(self::HAS_MANY, 'DetDetenciones', 'id_objetos_porta'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id_objetos_porta' => 'Id Objetos Porta',
			'descripcion' => 'Descripcion',
			'camisa' => 'Camisa',
			'pantalon' => 'Pantalon',
			'playera' => 'Playera',
			'chamarra' => 'Chamarra',
			'tenis' => 'Tenis',
			'zapatos' => 'Zapatos',
			'botas' => 'Botas',
			'sombrero' => 'Sombrero',
			'cadena' => 'Cadena',
			'pulsera' => 'Pulsera',
			'gorra' => 'Gorra',
			'cinturon' => 'Cinturon',
			'cartera' => 'Cartera',
			'lentes' => 'Lentes',
			'otros' => 'Otros',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id_objetos_porta',$this->id_objetos_porta);
		$criteria->compare('descripcion',$this->descripcion,true);
		$criteria->compare('camisa',$this->camisa);
		$criteria->compare('pantalon',$this->pantalon);
		$criteria->compare('playera',$this->playera);
		$criteria->compare('chamarra',$this->chamarra);
		$criteria->compare('tenis',$this->tenis);
		$criteria->compare('zapatos',$this->zapatos);
		$criteria->compare('botas',$this->botas);
		$criteria->compare('sombrero',$this->sombrero);
		$criteria->compare('cadena',$this->cadena);
		$criteria->compare('pulsera',$this->pulsera);
		$criteria->compare('gorra',$this->gorra);
		$criteria->compare('cinturon',$this->cinturon);
		$criteria->compare('cartera',$this->cartera);
		$criteria->compare('lentes',$this->lentes);
		$criteria->compare('otros',$this->otros);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
}
<?php

/**
 * This is the model class for table "per_descripcion_fisica".
 *
 * The followings are the available columns in table 'per_descripcion_fisica':
 * @property integer $id_descripcion
 * @property string $estatura
 * @property string $color_piel
 * @property string $cejas
 * @property string $color_ojos
 * @property string $color_pelo
 * @property string $menton
 * @property string $frente
 * @property string $complexion
 * @property integer $id_persona
 */
class PerDescripcionFisica extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return PerDescripcionFisica the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'per_descripcion_fisica';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('id_persona', 'numerical', 'integerOnly'=>true),
			array('estatura, color_piel, cejas, color_ojos, color_pelo, frente, complexion', 'length', 'max'=>10),
			array('menton', 'length', 'max'=>15),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('id_descripcion, estatura, color_piel, cejas, color_ojos, color_pelo, menton, frente, complexion, id_persona', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id_descripcion' => 'Id Descripcion',
			'estatura' => 'Estatura',
			'color_piel' => 'Color Piel',
			'cejas' => 'Cejas',
			'color_ojos' => 'Color Ojos',
			'color_pelo' => 'Color Pelo',
			'menton' => 'Menton',
			'frente' => 'Frente',
			'complexion' => 'Complexion',
			'id_persona' => 'Id Persona',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id_descripcion',$this->id_descripcion);
		$criteria->compare('estatura',$this->estatura,true);
		$criteria->compare('color_piel',$this->color_piel,true);
		$criteria->compare('cejas',$this->cejas,true);
		$criteria->compare('color_ojos',$this->color_ojos,true);
		$criteria->compare('color_pelo',$this->color_pelo,true);
		$criteria->compare('menton',$this->menton,true);
		$criteria->compare('frente',$this->frente,true);
		$criteria->compare('complexion',$this->complexion,true);
		$criteria->compare('id_persona',$this->id_persona);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
}
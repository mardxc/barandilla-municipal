<?php

/**
 * This is the model class for table "det_detenciones".
 *
 * The followings are the available columns in table 'det_detenciones':
 * @property integer $id_detencion
 * @property string $fecha
 * @property string $modus_operandi
 * @property string $bienes_afectados
 * @property string $monto_aproximado_afectacion
 * @property string $detenido_en
 * @property string $ocupacion
 * @property integer $id_condicion
 * @property integer $id_identificacion
 * @property integer $id_banda
 * @property integer $id_persona
 * @property integer $id_dinero
 * @property integer $id_objetos_porta
 * @property integer $id_canalizacion
 * @property integer $id_municipio
 * @property integer $id_corporacion
 * @property integer $id_lugar
 * @property integer $id_zona
 * @property string $hora
 *
 * The followings are the available model relations:
 * @property DetBandas $idBanda
 * @property DetCanalizaciones $idCanalizacion
 * @property BasCorporacion $idCorporacion
 * @property DetDinero $idDinero
 * @property DetIdentificacion $idIdentificacion
 * @property DetLugarAseguramiento $idLugar
 * @property GenMunicipio $idMunicipio
 * @property DetObjetosPorta $idObjetosPorta
 * @property PerPersonas $idPersona
 * @property BasZona $idZona
 * @property DetCondicionesFisicas $idCondicion
 * @property DetDetencionesAfectado[] $detDetencionesAfectados
 * @property DetDetencionesOficiales[] $detDetencionesOficiales
 * @property DetMotivos[] $detMotivoses
 */
class DetDetenciones extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return DetDetenciones the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'det_detenciones';
	}
	public function beforeSave()
	{
		if($this->fecha <> '')
	    {
			list($d2, $m2, $y2) = explode('/', $this->fecha);
	        $mk2=mktime(0, 0, 0, $m2, $d2, $y2);
	        $this->fecha = date ('Y-m-d', $mk2);
		}
		return parent::beforeSave();
	}

	public function afterFind ()
	{
	    if($this->fecha <> '')
	    {               
	        list($y, $m, $d) = explode('-', $this->fecha);
	        $mk=mktime(0, 0, 0, $m, $d, $y);
	        $this->fecha = date ('d/m/Y', $mk);
	    }
	    return parent::afterFind ();
	}
	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('id_condicion, id_identificacion, id_banda, id_persona, id_dinero, id_objetos_porta, id_canalizacion, id_municipio, id_corporacion, id_lugar, id_zona', 'numerical', 'integerOnly'=>true),
			array('modus_operandi, detenido_en', 'length', 'max'=>30),
			array('bienes_afectados', 'length', 'max'=>100),
			array('monto_aproximado_afectacion', 'length', 'max'=>8),
			array('ocupacion', 'length', 'max'=>20),
			array('hora', 'length', 'max'=>45),
			array('fecha', 'safe'),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('id_detencion, fecha, modus_operandi, bienes_afectados, monto_aproximado_afectacion, detenido_en, ocupacion, id_condicion, id_identificacion, id_banda, id_persona, id_dinero, id_objetos_porta, id_canalizacion, id_municipio, id_corporacion, id_lugar, id_zona, hora', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'idBanda' => array(self::BELONGS_TO, 'DetBandas', 'id_banda'),
			'idCanalizacion' => array(self::BELONGS_TO, 'DetCanalizaciones', 'id_canalizacion'),
			'idCorporacion' => array(self::BELONGS_TO, 'BasCorporacion', 'id_corporacion'),
			'idDinero' => array(self::BELONGS_TO, 'DetDinero', 'id_dinero'),
			'idIdentificacion' => array(self::BELONGS_TO, 'DetIdentificacion', 'id_identificacion'),
			'idLugar' => array(self::BELONGS_TO, 'DetLugarAseguramiento', 'id_lugar'),
			'idMunicipio' => array(self::BELONGS_TO, 'GenMunicipio', 'id_municipio'),
			'idObjetosPorta' => array(self::BELONGS_TO, 'DetObjetosPorta', 'id_objetos_porta'),
			'idPersona' => array(self::BELONGS_TO, 'PerPersonas', 'id_persona'),
			'idZona' => array(self::BELONGS_TO, 'BasZona', 'id_zona'),
			'idCondicion' => array(self::BELONGS_TO, 'DetCondicionesFisicas', 'id_condicion'),
			'detDetencionesAfectados' => array(self::HAS_MANY, 'DetDetencionesAfectado', 'id_detencion'),
			'detDetencionesOficiales' => array(self::HAS_MANY, 'DetDetencionesOficiales', 'id_detencion'),
			'detMotivoses' => array(self::HAS_MANY, 'DetMotivos', 'id_detencion'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id_detencion' => 'Id Detencion',
			'fecha' => 'Fecha',
			'modus_operandi' => 'Modus Operandi',
			'bienes_afectados' => 'Bienes Afectados',
			'monto_aproximado_afectacion' => 'Monto Aproximado Afectacion',
			'detenido_en' => 'Detenido En',
			'ocupacion' => 'Ocupacion',
			'id_condicion' => 'Id Condicion',
			'id_identificacion' => 'Id Identificacion',
			'id_banda' => 'Id Banda',
			'id_persona' => 'Id Persona',
			'id_dinero' => 'Id Dinero',
			'id_objetos_porta' => 'Id Objetos Porta',
			'id_canalizacion' => 'Id Canalizacion',
			'id_municipio' => 'Id Municipio',
			'id_corporacion' => 'Id Corporacion',
			'id_lugar' => 'Id Lugar',
			'id_zona' => 'Id Zona',
			'hora' => 'Hora',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id_detencion',$this->id_detencion);
		$criteria->compare('fecha',$this->fecha,true);
		$criteria->compare('modus_operandi',$this->modus_operandi,true);
		$criteria->compare('bienes_afectados',$this->bienes_afectados,true);
		$criteria->compare('monto_aproximado_afectacion',$this->monto_aproximado_afectacion,true);
		$criteria->compare('detenido_en',$this->detenido_en,true);
		$criteria->compare('ocupacion',$this->ocupacion,true);
		$criteria->compare('id_condicion',$this->id_condicion);
		$criteria->compare('id_identificacion',$this->id_identificacion);
		$criteria->compare('id_banda',$this->id_banda);
		$criteria->compare('id_persona',$this->id_persona);
		$criteria->compare('id_dinero',$this->id_dinero);
		$criteria->compare('id_objetos_porta',$this->id_objetos_porta);
		$criteria->compare('id_canalizacion',$this->id_canalizacion);
		$criteria->compare('id_municipio',$this->id_municipio);
		$criteria->compare('id_corporacion',$this->id_corporacion);
		$criteria->compare('id_lugar',$this->id_lugar);
		$criteria->compare('id_zona',$this->id_zona);
		$criteria->compare('hora',$this->hora,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
}
<?php

/**
 * This is the model class for table "det_detenciones_afectado".
 *
 * The followings are the available columns in table 'det_detenciones_afectado':
 * @property integer $id_det_afectado
 * @property integer $id_objetos
 * @property integer $id_detencion
 * @property integer $id_persona
 * @property integer $id_domicilio
 *
 * The followings are the available model relations:
 * @property DetDetenciones $idDetencion
 * @property DetObjetos $idObjetos
 */
class DetDetencionesAfectado extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return DetDetencionesAfectado the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'det_detenciones_afectado';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('id_objetos, id_detencion, id_persona, id_domicilio', 'required'),
			array('id_objetos, id_detencion, id_persona, id_domicilio', 'numerical', 'integerOnly'=>true),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('id_det_afectado, id_objetos, id_detencion, id_persona, id_domicilio', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'idDetencion' => array(self::BELONGS_TO, 'DetDetenciones', 'id_detencion'),
			'idObjetos' => array(self::BELONGS_TO, 'DetObjetos', 'id_objetos'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id_det_afectado' => 'Id Det Afectado',
			'id_objetos' => 'Id Objetos',
			'id_detencion' => 'Id Detencion',
			'id_persona' => 'Id Persona',
			'id_domicilio' => 'Id Domicilio',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id_det_afectado',$this->id_det_afectado);
		$criteria->compare('id_objetos',$this->id_objetos);
		$criteria->compare('id_detencion',$this->id_detencion);
		$criteria->compare('id_persona',$this->id_persona);
		$criteria->compare('id_domicilio',$this->id_domicilio);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
}
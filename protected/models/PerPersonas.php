<?php

/**
 * This is the model class for table "per_personas".
 *
 * The followings are the available columns in table 'per_personas':
 * @property integer $id_persona
 * @property string $nombre
 * @property string $ape_pat
 * @property string $ape_mat
 * @property integer $telefono
 * @property string $sexo
 * @property string $fecha_nacimiento
 * @property string $estado_civil
 * @property string $nacionalidad
 * @property string $señas_particulares
 * @property string $lesiones_presenta
 * @property integer $id_escolaridad
 * @property integer $edad
 * @property string $foto
 */
class PerPersonas extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return PerPersonas the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'per_personas';
	}
	public static function pName($id_persona){
			$res=PerPersonas::model()->find('id_persona='.$id_persona);
			return $res['nombre'] . ' ' . $res['ape_pat'] . ' ' . $res['ape_mat'];
		}

		public function beforeSave()
		{
			if($this->fecha_nacimiento <> '')
		    {
				list($d2, $m2, $y2) = explode('/', $this->fecha_nacimiento);
		        $mk2=mktime(0, 0, 0, $m2, $d2, $y2);
		        $this->fecha_nacimiento = date ('Y-m-d', $mk2);
			}
			return parent::beforeSave();
		}

		public function afterFind ()
		{
		    if($this->fecha_nacimiento <> '')
		    {               
		        list($y, $m, $d) = explode('-', $this->fecha_nacimiento);
		        $mk=mktime(0, 0, 0, $m, $d, $y);
		        $this->fecha_nacimiento = date ('d/m/Y', $mk);
		    }
		    return parent::afterFind ();
		}
		public static function getPersona(){
			$sql="SELECT 
				    id_persona
				FROM
				    per_personas
				ORDER BY id_persona DESC
				LIMIT 1";
				$persona=PerPersonas::model()->findBySql($sql);
				return $persona['id_persona'];
		}
	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('telefono, id_escolaridad, edad', 'numerical', 'integerOnly'=>true),
			array('nombre, ape_pat, ape_mat, nacionalidad', 'length', 'max'=>20),
			array('sexo, estado_civil', 'length', 'max'=>10),
			array('señas_particulares, lesiones_presenta', 'length', 'max'=>200),
			array('foto', 'length', 'max'=>45),
			array('fecha_nacimiento', 'safe'),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('id_persona, nombre, ape_pat, ape_mat, telefono, sexo, fecha_nacimiento, estado_civil, nacionalidad, señas_particulares, lesiones_presenta, id_escolaridad, edad, foto', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id_persona' => 'Id Persona',
			'nombre' => 'Nombre',
			'ape_pat' => 'Ape Pat',
			'ape_mat' => 'Ape Mat',
			'telefono' => 'Telefono',
			'sexo' => 'Sexo',
			'fecha_nacimiento' => 'Fecha Nacimiento',
			'estado_civil' => 'Estado Civil',
			'nacionalidad' => 'Nacionalidad',
			'señas_particulares' => 'Señas Particulares',
			'lesiones_presenta' => 'Lesiones Presenta',
			'id_escolaridad' => 'Id Escolaridad',
			'edad' => 'Edad',
			'foto' => 'Foto',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id_persona',$this->id_persona);
		$criteria->compare('nombre',$this->nombre,true);
		$criteria->compare('ape_pat',$this->ape_pat,true);
		$criteria->compare('ape_mat',$this->ape_mat,true);
		$criteria->compare('telefono',$this->telefono);
		$criteria->compare('sexo',$this->sexo,true);
		$criteria->compare('fecha_nacimiento',$this->fecha_nacimiento,true);
		$criteria->compare('estado_civil',$this->estado_civil,true);
		$criteria->compare('nacionalidad',$this->nacionalidad,true);
		$criteria->compare('señas_particulares',$this->señas_particulares,true);
		$criteria->compare('lesiones_presenta',$this->lesiones_presenta,true);
		$criteria->compare('id_escolaridad',$this->id_escolaridad);
		$criteria->compare('edad',$this->edad);
		$criteria->compare('foto',$this->foto,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
}
<?php

/**
 * This is the model class for table "gen_localidad".
 *
 * The followings are the available columns in table 'gen_localidad':
 * @property integer $id_localidad
 * @property string $localidad
 * @property integer $id_municipio
 *
 * The followings are the available model relations:
 * @property GenMunicipio $idMunicipio
 */
class GenLocalidad extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return GenLocalidad the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'gen_localidad';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('localidad, id_municipio', 'required'),
			array('id_municipio', 'numerical', 'integerOnly'=>true),
			array('localidad', 'length', 'max'=>30),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('id_localidad, localidad, id_municipio', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'idMunicipio' => array(self::BELONGS_TO, 'GenMunicipio', 'id_municipio'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id_localidad' => 'Id Localidad',
			'localidad' => 'Localidad',
			'id_municipio' => 'Id Municipio',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id_localidad',$this->id_localidad);
		$criteria->compare('localidad',$this->localidad,true);
		$criteria->compare('id_municipio',$this->id_municipio);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
}
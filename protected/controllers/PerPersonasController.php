<?php

class PerPersonasController extends Controller
{
	/**
	 * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
	 * using two-column layout. See 'protected/views/layouts/column2.php'.
	 */
	public $layout='//layouts/column2';

	/**
	 * @return array action filters
	 */
	public function filters()
	{
		return array(array('CrugeAccessControlFilter'));
	}

	/**
	 * Specifies the access control rules.
	 * This method is used by the 'accessControl' filter.
	 * @return array access control rules
	 */
	public function accessRules()
	{
		return array(
			array('allow',  // allow all users to perform 'index' and 'view' actions
				'actions'=>array('index','view'),
				'users'=>array('*'),
			),
			array('allow', // allow authenticated user to perform 'create' and 'update' actions
				'actions'=>array('create','update','aseguradoForm','guardarAll'),
				'users'=>array('@'),
			),
			array('allow', // allow admin user to perform 'admin' and 'delete' actions
				'actions'=>array('admin','delete'),
				'users'=>array('admin'),
			),
			array('deny',  // deny all users
				'users'=>array('*'),
			),
		);
	}


public function actionGuardar(){
		$id_persona=$_POST['id_persona'];
		$accion=$_POST['guarda'];

		$nombre=$_POST['nombre'];
		$ape_pat=$_POST['ape_pat'];
		$ape_mat=$_POST['ape_mat'];
		$telefono=$_POST['telefono'];
		$sexo=$_POST['sexo'];
		$fecha_nacimiento=$_POST['fecha_nacimiento'];
		$estado_civil=$_POST['estado_civil'];
		$nacionalidad=$_POST['nacionalidad'];
		$señas_particulares=$_POST['señas_particulares'];
		$lesiones_presenta=$_POST['lesiones_presenta'];


				
		$datos=array(
			'nombre'=>$nombre,
			'ape_pat'=>$ape_pat,
			'ape_mat'=>$ape_mat,
			'telefono'=>$telefono,
			'sexo'=>$sexo,
			'fecha_nacimiento'=>$fecha_nacimiento,
			'estado_civil'=>$estado_civil,
			'nacionalidad'=>$colonia,
			'señas_particulares'=>$señas_particulares,
			'lesiones_presenta'=>$lesiones_presenta


		);
		/* Save */
		if($accion=='guardar'){

			$persona=new perPersonas;

				$persona->nombre				=		$nombre;
				$persona->ape_pat			=		$ape_pat;
				$persona->ape_mat			=		$ape_mat;
				$persona->telefono			=		$telefono;
				$persona->sexo				=		$sexo;
				$persona->fecha_nacimiento			=		$fecha_nacimiento;
				$persona->estado_civil		=		$estado_civil;
				$persona->nacionalidad			=		$nacionalidad;
				$persona->señas_particulares	=		$señas_particulares;
			$persona->lesiones_presenta	=		$lesiones_presenta;


			$persona->save();

			echo CJSON::encode(array(
				'status'=>'guardar',
				'id_persona'=>$persona->id_persona,
				'datos'=>$datos
				))
			;

		/* Update */
		}elseif ($accion=='actualizar') {

			$persona = perPersonas::model()->findByAttributes(array('id_alumno'=>$id_alumno));

				$persona->nombre				=		$nombre;
				$persona->ape_pat			=		$ape_pat;
				$persona->ape_mat			=		$ape_mat;
				$persona->telefono			=		$telefono;
				$persona->sexo				=		$sexo;
				$persona->fecha_nacimiento			=		$fecha_nacimiento;
				$persona->estado_civil		=		$estado_civil;
				$persona->nacionalidad			=		$nacionalidad;
				$persona->señas_particulares	=		$señas_particulares;
				$persona->lesiones_presenta	=		$lesiones_presenta;

			
			$persona->SaveAttributes(array(
				'nombre',
				'ape_pat',
				'ape_mat',
				'telefono',
				'sexo',
				'fecha_nacimiento',
				'estado_civil',
				'nacionalidad',
				'señas_particulares',
				'lesiones_presenta'
				)
			);
			
			echo CJSON::encode(array(
				'status'=>'actualizar',
				'id_persona'=>$persona['id_persona'],
				'datos'=>$datos
				)
			);
		}
	}

	public function actionGuardarAll(){
			$id_persona=$_POST['id_persona'];
			$accion=$_POST['guarda'];

		$nombre=$_POST['nombre'];
		$ape_pat=$_POST['ape_pat'];
		$ape_mat=$_POST['ape_mat'];
		$telefono=$_POST['telefono'];
		$sexo=$_POST['sexo'];
		$fecha_nacimiento=$_POST['fecha_nacimiento'];
		$estado_civil=$_POST['estado_civil'];
		$colonia=$_POST['colonia'];
		$nacionalidad=$_POST['nacionalidad'];
		$señas_particulares=$_POST['señas_particulares'];
		$lesiones_presenta=$_POST['lesiones_presenta'];

		$estatura=$_POST['estatura'];
		$color_piel=$_POST['color_piel'];
		$cejas=$_POST['cejas'];
		$color_ojos=$_POST['color_ojos'];
		$color_pelo=$_POST['color_pelo'];
		$menton=$_POST['menton'];
		$frente=$_POST['frente'];
		$complexion=$_POST['complexion'];

		$id_objeto_porta=$_POST['id_objeto_porta'];
		$descripcion=$_POST['descripcion'];
		$camisa=$_POST['camisa'];
		$pantalon=$_POST['pantalon'];
		$playera=$_POST['playera'];
		$chamarra=$_POST['chamarra'];
		$tenis=$_POST['tenis'];
		$zapatos=$_POST['zapatos'];
		$botas=$_POST['botas'];
		$cadena=$_POST['cadena'];
		$Sombrero=$_POST['Sombrero'];
		$pulsera=$_POST['pulsera'];
		$gorra=$_POST['gorra'];
		$cinturon=$_POST['cinturon'];
		$cartera=$_POST['cartera'];
		$lentes=$_POST['lentes'];
		$otros=$_POST['otros'];

		$id_identificaciones=$_POST['id_identificaciones'];
		$identificacion=$_POST['identificacion'];

		$id_dinero=$_POST['id_dinero'];
		$billetes=$_POST['billetes'] ;
		$monedas=$_POST['monedas'];
		$dolares=$_POST['dolares'];
		$otros=$_POST['otros'];
		$total=$_POST['total'];

		$id_motivos=$_POST['id_motivos'];
		$motivo=$_POST['motivo'];
		$descripcion=$_POST['descripcion'];
		$gravedad=$_POST['gravedad'];


					
			$datos=array(
				'nombre'=>$nombre,
				'ape_pat'=>$ape_pat,
				'ape_mat'=>$ape_mat,
				'telefono'=>$telefono,
				'sexo'=>$sexo,
				'fecha_nacimiento'=>$fecha_nacimiento,
				'estado_civil'=>$estado_civil,
				'colonia'=>$colonia,
				'nacionalidad'=>$nacionalidad,
				'señas_particulares'=>$señas_particulares,
				'lesiones_presenta'=>$lesiones_presenta,

				'estatura'=>$estatura,
				'color_piel'=>$color_piel,
				'cejas'=>$cejas,
				'color_ojos'=>$color_ojos,
				'color_pelo'=>$color_pelo,
				'menton'=>$menton,
				'frente'=>$frente,
				'complexion'=>$complexion,

				'id_objeto_porta'=>$id_objeto_porta,
				'descripcion'=>$descripcion,
				'camisa'=>$camisa,
				'pantalon'=>$pantalon,
				'playera'=>$playera,
				'chamarra'=>$chamarra,
				'tenis'=>$tenis,
				'zapatos'=>$zapatos,
				'botas'=>$botas,
				'cadena'=>$cadena,
				'Sombrero'=>$Sombrero,
				'pulsera'=>$pulsera,
				'gorra'=>$gorra,
				'cinturon'=>$cinturon,
				'cartera'=>$cartera,
				'lentes'=>$lentes,
				'otros'=>$otros,

				'id_identificaciones'=>$id_identificaciones,
				'identificacion'=>$identificacion,

				'id_dinero'=>$id_dinero,
				'billetes'=>$billetes,
				'monedas'=>$monedas,
				'dolares'=>$dolares,
				'otros'=>$otros,
				'total'=>$total,

				'id_motivos'=>$id_motivos,
				'motivo'=>$motivo,
				'descripcion'=>$descripcion,
				'gravedad'=>$gravedad


			);
			/* Save */
			if($accion=='guardar'){

				$persona=new perPersonas;

					$persona->nombre				=		$nombre;
					$persona->nombre=$nombre;
					$persona->ape_pat=$ape_pat;
					$persona->ape_mat=$ape_mat;
					$persona->telefono=$telefono;
					$persona->sexo=$sexo;
					$persona->fecha_nacimiento=$fecha_nacimiento;
					$persona->estado_civil=$estado_civil;
					$persona->colonia=$colonia;
					$persona->nacionalidad=$nacionalidad;
					$persona->señas_particulares=$señas_particulares;
					$persona->lesiones_presenta=$lesiones_presenta;

				$descripcion=new PerDescripcionFisica;
					$descripcion->estatura			=$estatura;
					$descripcion->color_piel		=$color_piel;
					$descripcion->cejas				=$cejas;
					$descripcion->color_ojos		=$color_ojos;
					$descripcion->color_pelo		=$color_pelo;
					$descripcion->menton			=$menton;
					$descripcion->frente			=$frente;
					$descripcion->complexion		=$complexion;

				$objetos=new DetObjetosPorta;
					$objetos->id_objeto_porta		=$id_objeto_porta;
					$objetos->descripcion			=$descripcion;
					$objetos->camisa				=$camisa;
					$objetos->pantalon				=$pantalon;
					$objetos->playera				=$playera;
					$objetos->chamarra				=$chamarra;
					$objetos->tenis					=$tenis;
					$objetos->zapatos				=$zapatos;
					$objetos->botas			=$botas;
					$objetos->cadena		=$cadena;
					$objetos->Sombrero		=$Sombrero;
					$objetos->pulsera		=$pulsera;
					$objetos->gorra			=$gorra;
					$objetos->cinturon		=$cinturon;
					$objetos->cartera		=$cartera;
					$objetos->lentes		=$lentes;
					$objetos->otros			=$otros;
				
				$identificacion=new DetIdentificacion;
					$identificacion->id_identificaciones	=$id_identificaciones;
					$identificacion->identificacion 		=$identificacion;
				
				$dinero= new DetDinero;
					$dinero->id_dinero		=$id_dinero;
					$dinero->billetes		=$billetes;
					$dinero->monedas		=$monedas;
					$dinero->dolares		=$dolares;
					$dinero->otros			=$otros;
					$dinero->total			=$total;

				$motivos= new BasMotivos;
					$motivos->id_motivos		=$id_motivos;
					$motivos->motivo			=$motivo;
					$motivos->descripcion		=$descripcion;
					$motivos->graveda			=$graveda;


				$persona->save();
				$descripcion->save();
				$objetos->save();
				$identificacion->save();
				$dinero->save();
				$motivos->save();

				echo CJSON::encode(array(
					'status'=>'guardar',
					'id_persona'=>$persona->id_persona,
					'datos'=>$datos
					))
				;

			/* Update */
			}elseif ($accion=='actualizar') {

				$persona = perPersonas::model()->findByAttributes(array('id_alumno'=>$id_alumno));

					$persona->nombre				=		$nombre;
					$persona->ape_pat			=		$ape_pat;
					$persona->ape_mat			=		$ape_mat;
					$persona->telefono			=		$telefono;
					$persona->sexo				=		$sexo;
					$persona->fecha_nacimiento			=		$fecha_nacimiento;
					$persona->estado_civil		=		$estado_civil;
					$persona->nacionalidad			=		$nacionalidad;
					$persona->señas_particulares	=		$señas_particulares;
					$persona->lesiones_presenta	=		$lesiones_presenta;

				
				$persona->SaveAttributes(array(
					'nombre',
					'ape_pat',
					'ape_mat',
					'telefono',
					'sexo',
					'fecha_nacimiento',
					'estado_civil',
					'nacionalidad',
					'señas_particulares',
					'lesiones_presenta'
					)
				);
				
				echo CJSON::encode(array(
					'status'=>'actualizar',
					'id_persona'=>$persona['id_persona'],
					'datos'=>$datos
					)
				);
			}
		}
	/**
	 * Displays a particular model.
	 * @param integer $id the ID of the model to be displayed
	 */
/*	public function actionView($id)
	{
		$this->render('view',array(
			'model'=>$this->loadModel($id),
		));
	}*/

	public function actionaseguradoForm()
	{
		$this->render('aseguradoForm');
	}

	/**
	 * Creates a new model.
	 * If creation is successful, the browser will be redirected to the 'view' page.
	 */
	public function actionCreate()
	{
		$model=new PerPersonas;

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['PerPersonas']))
		{
			$model->attributes=$_POST['PerPersonas'];
			if($model->save())
				$this->redirect(array('view','id'=>$model->id_persona));
		}

		$this->render('create',array(
			'model'=>$model,
		));
	}

	/**
	 * Updates a particular model.
	 * If update is successful, the browser will be redirected to the 'view' page.
	 * @param integer $id the ID of the model to be updated
	 */
	public function actionUpdate($id)
	{
		$model=$this->loadModel($id);

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['PerPersonas']))
		{
			$model->attributes=$_POST['PerPersonas'];
			if($model->save())
				$this->redirect(array('view','id'=>$model->id_persona));
		}

		$this->render('update',array(
			'model'=>$model,
		));
	}

	/**
	 * Deletes a particular model.
	 * If deletion is successful, the browser will be redirected to the 'admin' page.
	 * @param integer $id the ID of the model to be deleted
	 */
	public function actionDelete($id)
	{
		$this->loadModel($id)->delete();

		// if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
		if(!isset($_GET['ajax']))
			$this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
	}

	/**
	 * Lists all models.
	 */
	public function actionIndex()
	{
		$dataProvider=new CActiveDataProvider('PerPersonas');
		$this->render('index',array(
			'dataProvider'=>$dataProvider,
		));
	}

	/**
	 * Manages all models.
	 */
	public function actionAdmin()
	{
		$model=new PerPersonas('search');
		$model->unsetAttributes();  // clear any default values
		if(isset($_GET['PerPersonas']))
			$model->attributes=$_GET['PerPersonas'];

		$this->render('admin',array(
			'model'=>$model,
		));
	}

	/**
	 * Returns the data model based on the primary key given in the GET variable.
	 * If the data model is not found, an HTTP exception will be raised.
	 * @param integer the ID of the model to be loaded
	 */
	public function loadModel($id)
	{
		$model=PerPersonas::model()->findByPk($id);
		if($model===null)
			throw new CHttpException(404,'The requested page does not exist.');
		return $model;
	}

	/**
	 * Performs the AJAX validation.
	 * @param CModel the model to be validated
	 */
	protected function performAjaxValidation($model)
	{
		if(isset($_POST['ajax']) && $_POST['ajax']==='per-personas-form')
		{
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}
	}
}

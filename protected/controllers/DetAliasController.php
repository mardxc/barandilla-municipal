<?php

class DetAliasController extends Controller
{
	/**
	 * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
	 * using two-column layout. See 'protected/views/layouts/column2.php'.
	 */
	public $layout='//layouts/column2';

	/**
	 * @return array action filters
	 */
	public function filters()
	{
		return array(array('CrugeAccessControlFilter'));
	}

	/**
	 * Specifies the access control rules.
	 * This method is used by the 'accessControl' filter.
	 * @return array access control rules
	 */
	public function accessRules()
	{
		return array(
			array('allow',  // allow all users to perform 'index' and 'view' actions
				'actions'=>array('index','view'),
				'users'=>array('*'),
			),
			array('allow', // allow authenticated user to perform 'create' and 'update' actions
				'actions'=>array('create','update', 'listadoPersona', 'AddAlias'),
				'users'=>array('@'),
			),
			array('allow', // allow admin user to perform 'admin' and 'delete' actions
				'actions'=>array('admin','delete'),
				'users'=>array('admin'),
			),
			array('deny',  // deny all users
				'users'=>array('*'),
			),
		);
	}

	/**
	 * Displays a particular model.
	 * @param integer $id the ID of the model to be displayed
	 */

	public function actionlistadoPersona($term)
	{
		$sql="SELECT id_persona,
				Concat(nombre, ' ',ape_pat, ' ', ape_mat) AS nombre
		 	FROM 
		 		per_personas 
		 	WHERE 
				Concat(nombre, ' ',ape_pat, ' ', ape_mat) LIKE '%".$term."%'";
		
		$data=Yii::app()->db->createCommand($sql)->queryAll();
		$arr=array();

		foreach ($data as $item) {
			$arr[]=array(
				'id'=>$item['id_persona'],
				'value'=>$item['nombre'],
				'label'=>$item['nombre'],
			);
		}
		
		echo CJSON::encode($arr);
	}

	public function actionAddAlias(){
		$id_persona = $_POST['id_persona'];
		$alias = $_POST['alias'];
		
		$aliasDet=new DetAlias();

		$aliasDet->id_persona=$id_persona;
		$aliasDet->alias=$alias;
		$aliasDet->save();
		
		echo CJSON::encode(
			array(
				'aliasDet'=>$aliasDet['id_persona'],
				)
		);
	}

	public function actionView($id)
	{
		$this->render('view',array(
			'model'=>$this->loadModel($id),
		));
	}

	/**
	 * Creates a new model.
	 * If creation is successful, the browser will be redirected to the 'view' page.
	 */
	public function actionCreate()
	{
		$model=new DetAlias;

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['DetAlias']))
		{
			$model->attributes=$_POST['DetAlias'];
			if($model->save())
				$this->redirect(array('view','id'=>$model->id_det_alias));
		}

		$this->render('create',array(
			'model'=>$model,
		));
	}

	/**
	 * Updates a particular model.
	 * If update is successful, the browser will be redirected to the 'view' page.
	 * @param integer $id the ID of the model to be updated
	 */
	public function actionUpdate($id)
	{
		$model=$this->loadModel($id);

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['DetAlias']))
		{
			$model->attributes=$_POST['DetAlias'];
			if($model->save())
				$this->redirect(array('view','id'=>$model->id_det_alias));
		}

		$this->render('update',array(
			'model'=>$model,
		));
	}

	/**
	 * Deletes a particular model.
	 * If deletion is successful, the browser will be redirected to the 'admin' page.
	 * @param integer $id the ID of the model to be deleted
	 */
	public function actionDelete($id)
	{
		$this->loadModel($id)->delete();

		// if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
		if(!isset($_GET['ajax']))
			$this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
	}

	/**
	 * Lists all models.
	 */
	public function actionIndex()
	{
		$dataProvider=new CActiveDataProvider('DetAlias');
		$this->render('index',array(
			'dataProvider'=>$dataProvider,
		));
	}

	/**
	 * Manages all models.
	 */
	public function actionAdmin($id_persona)
	{
		$sql="SELECT 
				    id_det_alias, alias AS Alias
				FROM
    				det_alias
    				WHERE
    				id_persona=$id_persona
				ORDER BY id_det_alias DESC";

			$dataProviderPro = new CSqlDataProvider($sql,
				array(
					'keyField'=>'id_persona',
					'pagination'=>array('pageSize'=>10,)
				)
			);

		if(Yii::app()->request->isAjaxRequest){
		
		$dataProviderPro = null;
			$id_persona=$_GET['id_persona'];
			$sql="SELECT 
				    id_det_alias, alias AS Alias
				FROM
    				det_alias
    			WHERE
    				id_persona=$id_persona
				ORDER BY id_det_alias DESC";

			$dataProviderPro=new CSqlDataProvider($sql,
				array(
					'keyField'=>'id_persona',
					'pagination'=>array('pageSize'=>10,)
				)
			);
			echo CJSON::encode($dataProviderPro);
		}

		$this->render('admin',array(
			'dataProviderPro'=>$dataProviderPro
		));
	}

	/**
	 * Returns the data model based on the primary key given in the GET variable.
	 * If the data model is not found, an HTTP exception will be raised.
	 * @param integer the ID of the model to be loaded
	 */
	public function loadModel($id)
	{
		$model=DetAlias::model()->findByPk($id);
		if($model===null)
			throw new CHttpException(404,'The requested page does not exist.');
		return $model;
	}

	/**
	 * Performs the AJAX validation.
	 * @param CModel the model to be validated
	 */
	protected function performAjaxValidation($model)
	{
		if(isset($_POST['ajax']) && $_POST['ajax']==='det-alias-form')
		{
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}
	}
}

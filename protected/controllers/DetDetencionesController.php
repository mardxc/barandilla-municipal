<?php

class DetDetencionesController extends Controller
{
	/**
	 * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
	 * using two-column layout. See 'protected/views/layouts/column2.php'.
	 */
	public $layout='//layouts/column2';

	/**
	 * @return array action filters
	 */
	public function filters()
	{
		return array(array('CrugeAccessControlFilter'));
	}

	/**
	 * Specifies the access control rules.
	 * This method is used by the 'accessControl' filter.
	 * @return array access control rules
	 */
	public function accessRules()
	{
		return array(
			array('allow',  // allow all users to perform 'index' and 'view' actions
				'actions'=>array('index','view'),
				'users'=>array('*'),
			),
			array('allow', // allow authenticated user to perform 'create' and 'update' actions
				'actions'=>array('create','update','guardarDetenido','registrar','listadoColonia','registrar2','BuscarDet'),
				'users'=>array('@'),
			),
			array('allow', // allow admin user to perform 'admin' and 'delete' actions
				'actions'=>array('admin','delete'),
				'users'=>array('admin'),
			),
			array('deny',  // deny all users
				'users'=>array('*'),
			),
		);
	}

	/**
	 * Displays a particular model.
	 * @param integer $id the ID of the model to be displayed
	 */
	/*public function actionView($id)
	{
		$this->render('view',array(
			'model'=>$this->loadModel($id),
		));
	}*/

	/**
	 * Creates a new model.
	 * If creation is successful, the browser will be redirected to the 'view' page.
	 */
	public function actionListadoColonia($term){
			$sql = "SELECT 
	    				id_colonia,
	    				colonia
					FROM
	    				bas_colonia
					WHERE
	    				colonia LIKE '%".$term."%'";
			
			$data = Yii::app()->db->createCommand($sql)->queryAll();
			$arr = array();

			foreach ($data as $item) {
				$arr[]=array(
					'id'=>$item['id_colonia'],
					'value'=>$item['colonia'],
					'label'=>$item['colonia'],
				);
			}
			
			echo CJSON::encode($arr);
		}

	public function actionBuscarDet(){
		$res=PerHuella::getHuella();
		$persona = PerPersonas::model()->findByAttributes(array('id_persona'=>$res));
		$domicilio = PerDomicilios::model()->findByAttributes(array('id_persona'=>$persona->id_persona));
		echo CJSON::encode(array(
			'id_persona'=>$res,
			'nombre'=>$persona['nombre'],
			'ape_pat'=>$persona['ape_pat'],
			'ape_mat'=>$persona['ape_mat'],
			'sexo'=>$persona['sexo'],
			'edad'=>$persona['edad'],
			'telefono'=>$persona['telefono'],
			'estado_civil'=>$persona['estado_civil'],
			'calle'=>$domicilio['calle'],
			'id_colonia'=>$domicilio['id_colonia'],
			'id_municipio'=>$domicilio['id_municipio'],
			'id_domicilio'=>$domicilio['id_domicilio'],
			)
		);
	}

	public function actionRegistrar($id_detencion)
	{
		if ($id_detencion!='') {
			$criteria = new CDbCriteria;
			$criteria->condition = "id_detencion=".$id_detencion;
			$detencion = DetDetenciones::model()->find($criteria);

			$persona = PerPersonas::model()->findByAttributes(array('id_persona'=>$detencion->id_persona));
			$lugar = DetLugarAseguramiento::model()->findByAttributes(array('id_lugar'=>$detencion->id_lugar));
			$domicilio = PerDomicilios::model()->findByAttributes(array('id_persona'=>$detencion->id_persona));
			$descripcionFisica = PerDescripcionFisica::model()->findByAttributes(array('id_persona'=>$persona->id_persona));

			$objetosPorta = DetObjetosPorta::model()->findByAttributes(array('id_objetos_porta'=>$detencion->id_objetos_porta));
			$canalizacion = DetCanalizaciones::model()->findByAttributes(array('id_canalizacion'=>$detencion->id_canalizacion));
			$afectado = DetDetencionesAfectado::model()->findByAttributes(array('id_detencion'=>$detencion->id_detencion));
			//$alias = DetAlias::model()->findByAttributes(array('id_persona'=>$persona->id_persona));
			//$domicilioAfectada = PerDomicilios::model()->findByAttributes(array('id_domicilio'=>$afectado->id_domicilio));
			//$personaAfectada = PerPersonas::model()->findByAttributes(array('id_persona'=>$afectado->id_persona));
			/*
			if ($alias->id_persona!='') {
				$personaAlias=11;//$alias->id_persona;
			}else{
				$personaAlias=0;
			}*/
			/*$dataProOficiales=DetDetencionesOficiales::dataProOficiales($detencion->id_detencion);
			$dataProALias=DetAlias::dataProALias($persona->id_persona);
			$dataProMotivo=DetMotivos::dataProMotivo($detencion->id_detencion);*/
			$sql="SELECT DISTINCT
			    (ddo.id_det_oficiales),
			    ddo.responsable,
			    CONCAT(bo.nombre,
			            ' ',
			            bo.ape_pat,
			            ' ',
			            bo.ape_mat) AS nombre,
			    ddo.id_detencion
			FROM
			    det_detenciones_oficiales ddo
			        INNER JOIN
			    bas_oficiales bo ON ddo.id_oficial = bo.id_oficial
			WHERE
			    id_detencion = $detencion->id_detencion";

			$dataProOficiales=new CSqlDataProvider($sql,
				array(
					'keyField'=>'id_detencion',
					'pagination'=>array('pageSize'=>3,)
				)
			);

			$sql="SELECT 
			    da.alias,
			    da.id_det_alias
			FROM
			    det_alias da
			WHERE
			    da.id_persona=$persona->id_persona
				ORDER BY id_det_alias DESC";
			$dataProALias=new CSqlDataProvider($sql,
				array(
					'keyField'=>'id_det_alias',
					'pagination'=>array('pageSize'=>3,)
				)
			);
			
			$sql="SELECT DISTINCT
				(dm.id_det_motivos),
			    bm.motivo AS motivo,
			    dm.id_detencion
			FROM
			    det_motivos dm
			inner join
			bas_motivos bm on dm.id_motivo=bm.id_motivo
			WHERE
			    id_detencion = $detencion->id_detencion";
			 $dataProMotivo=new CSqlDataProvider($sql,
			 	array(
			 		'keyField'=>'id_det_motivos',
			 		'pagination'=>array('pageSize'=>3,)
			 	)
			 );

			 $sql="SELECT DISTINCT
			 		(id_objeto),
				    objeto,
				    caracteristicas,
				    id_detencion,
				    id_persona
				FROM
				    det_objetos
				where
				id_detencion = $detencion->id_detencion";
			  $dataProObjeto=new CSqlDataProvider($sql,
			  	array(
			  		'keyField'=>'id_objeto',
			  		'pagination'=>array('pageSize'=>3,)
			  	)
			  );
			//$dataProALias=DetAlias::listAlias($detencion->id_persona);
			//$dataProMotivo=DetMotivos::listMotivos($detencion->id_detencion);
			$tabPersona = DetDetenciones::model()->findAll('id_persona='.$persona->id_persona);
			$tabPersonaTitle = DetDetenciones::model()->findAll('id_persona='.$persona->id_persona);
			$tabAlias = DetAlias::model()->findAll('id_persona='.$persona->id_persona);
			$tabMotivos = DetMotivos::model()->findAll('id_detencion='.$detencion->id_detencion);
			$this->render('registrar',array(
				'detencion'=>$detencion,
				'persona'=>$persona,
				'lugar'=>$lugar,
				'domicilio'=>$domicilio,
				'descripcion'=>$descripcionFisica,
				'objetosPorta'=>$objetosPorta,
				'canalizacion'=>$canalizacion,
				'afectado'=>$afectado,
				//'domicilioAfectada'=>$domicilioAfectada,
				//'personaAfectada'=>$personaAfectada,
				'dataProALias'=>$dataProALias,
				'dataProMotivo'=>$dataProMotivo,
				'dataProOficiales'=>$dataProOficiales,
				'dataProObjeto'=>$dataProObjeto,
				'tabPersona'=>$tabPersona,
				'tabPersonaTitle'=>$tabPersonaTitle,
				'tabAlias'=>$tabAlias,
				//'tabMotivos'=>$tabMotivos,
			));
		}else{
			/*$dataProALias=DetAlias::dataProALias(0);
			$dataProMotivo=DetMotivos::dataProMotivo(0);
			$dataProOficiales=DetDetencionesOficiales::dataProOficiales(0);*/
			//$dataProALias=DetAlias::listAlias(0);
			$sql="SELECT DISTINCT
			    (ddo.id_det_oficiales),
			    ddo.responsable,
			    CONCAT(bo.nombre,
			            ' ',
			            bo.ape_pat,
			            ' ',
			            bo.ape_mat) AS nombre,
			    ddo.id_detencion
			FROM
			    det_detenciones_oficiales ddo
			        INNER JOIN
			    bas_oficiales bo ON ddo.id_oficial = bo.id_oficial
			WHERE
			    id_detencion = 0";

			$dataProOficiales=new CSqlDataProvider($sql,
				array(
					'keyField'=>'id_detencion',
					'pagination'=>array('pageSize'=>3,)
				)
			);

			$sql="SELECT 
			    da.alias,
			    da.id_det_alias
			FROM
			    det_alias da
			WHERE
			    da.id_persona=0
				ORDER BY id_det_alias DESC";
			$dataProALias=new CSqlDataProvider($sql,
				array(
					'keyField'=>'id_det_alias',
					'pagination'=>array('pageSize'=>3,)
				)
			);

			$sql="SELECT DISTINCT
				(dm.id_det_motivos),
			    bm.motivo AS motivo,
			    dm.id_detencion
			FROM
			    det_motivos dm
			inner join
			bas_motivos bm on dm.id_motivo=bm.id_motivo
			WHERE
			    id_detencion = 0";

			$dataProMotivo=new CSqlDataProvider($sql,
				array(
					'keyField'=>'id_det_motivos',
					'pagination'=>array('pageSize'=>3,)
				)
			);

			 $sql="SELECT DISTINCT
			 		(id_objeto),
				    objeto,
				    caracteristicas,
				    id_detencion,
				    id_persona
				FROM
				    det_objetos
				where
				id_detencion = 0";
			  $dataProObjeto=new CSqlDataProvider($sql,
			  	array(
			  		'keyField'=>'id_objeto',
			  		'pagination'=>array('pageSize'=>3,)
			  	)
			  );
			//$dataProMotivo=DetMotivos::listMotivos(0);
			  $tabPersonaTitle = DetDetenciones::model()->findAll('id_persona=0');
			  $tabPersona = DetDetenciones::model()->findAll('id_persona=0');
			  $tabAlias = DetAlias::model()->findAll('id_persona=0');
			  $tabMotivos = DetMotivos::model()->findAll('id_detencion=0');
			$this->render('registrar',array(
				'dataProALias'=>$dataProALias,
				'dataProMotivo'=>$dataProMotivo,
				'dataProOficiales'=>$dataProOficiales,
				'dataProObjeto'=>$dataProObjeto,
				'tabPersona'=>$tabPersona,
				'tabPersonaTitle'=>$tabPersonaTitle,
				'tabAlias'=>$tabAlias,
				//'tabMotivos'=>$tabMotivos,
			));
		}
		//

	}
	public function actionGuardarDetenido()
	{

		$id_detencion=$_POST['id_detencion'];
		$id_persona=$_POST['id_persona'];//
		$id_lugar=$_POST['id_lugar'];
		$id_descripcion=$_POST['id_descripcion'];

		$id_domicilio=$_POST['id_domicilio'];//
		$id_objetos_porta=$_POST['id_objetos_porta'];
		$id_canalizacion=$_POST['id_canalizacion'];

		$fecha=$_POST['fecha'];
		$modus_operandi=$_POST['modus_operandi'];
		$bienes_afectados=$_POST['bienes_afectados'];
		$monto_aproximado_afectacion=$_POST['monto_aproximado_afectacion'];
		$detenido_en=$_POST['detenido_en'];
		$ocupacion=$_POST['ocupacion'];
		$id_condicion=$_POST['id_condicion'];
		$id_identificacion=$_POST['id_identificacion'];
		$id_banda=$_POST['id_banda'];

		$nombre=$_POST['nombre'];
		$ape_pat=$_POST['ape_pat'];
		$ape_mat=$_POST['ape_mat'];
		//$telefono=$_POST['telefono'];
		$sexo=$_POST['sexo'];
		$fecha_nacimiento=$_POST['fecha_nacimiento'];
		$estado_civil=$_POST['estado_civil'];
		$nacionalidad=$_POST['nacionalidad'];
		$señas_particulares=$_POST['señas_particulares'];
		$lesiones_presenta=$_POST['lesiones_presenta'];
		$id_escolaridad=$_POST['id_escolaridad'];
		$edad=$_POST['edad'];

		//$id_dinero=$_POST['id_dinero'];
		$id_objetos_porta=$_POST['id_objetos_porta'];
		$id_canalizacion=$_POST['id_canalizacion'];

		$id_municipio=$_POST['id_municipio'];
		$id_corporacion=$_POST['id_corporacion'];

		$colonia=$_POST['colonia'];
		$calle=$_POST['calle'];
		$entre_calle=$_POST['entre_calle'];
		$y_la_calle=$_POST['y_la_calle'];
		$a_la_altura=$_POST['a_la_altura'];

		$id_zona=$_POST['id_zona'];
		$hora=$_POST['hora'];

		$estatura=$_POST['estatura'];
		$color_piel=$_POST['color_piel'];
		$ceja=$_POST['ceja'];
		$color_ojos=$_POST['color_ojos'];
		$color_pelo=$_POST['color_pelo'];
		$menton=$_POST['menton'];
		$frente=$_POST['frente'];
		$complexion=$_POST['complexion'];

		$domCalle=$_POST['domCalle'];
		$domColonia=$_POST['domColonia'];
		$domMunicipio=$_POST['domMunicipio'];

		$camisa=DetObjetosPorta::trueOrFalse($_POST['camisa']);
		$tenis=DetObjetosPorta::trueOrFalse($_POST['tenis']);
		$pulsera=DetObjetosPorta::trueOrFalse($_POST['pulsera']);
		$cartera=DetObjetosPorta::trueOrFalse($_POST['cartera']);
		$pantalon=DetObjetosPorta::trueOrFalse($_POST['pantalon']);
		$zapatos=DetObjetosPorta::trueOrFalse($_POST['zapatos']);
		$gorra=DetObjetosPorta::trueOrFalse($_POST['gorra']);
		$lentes=DetObjetosPorta::trueOrFalse($_POST['lentes']);
		$playera=DetObjetosPorta::trueOrFalse($_POST['playera']);
		$botas=DetObjetosPorta::trueOrFalse($_POST['botas']);
		$cinturon=DetObjetosPorta::trueOrFalse($_POST['cinturon']);
		$sombrero=DetObjetosPorta::trueOrFalse($_POST['sombrero']);
		$chamarra=DetObjetosPorta::trueOrFalse($_POST['chamarra']);
		$otros=DetObjetosPorta::trueOrFalse($_POST['otros']);
		$obDescripcion=$_POST['obDescripcion'];

		$cadena=DetCanalizaciones::trueOrFalse($_POST['cadena']);
		$juez_calificador=DetCanalizaciones::trueOrFalse($_POST['juez_calificador']);
		$fuero_comun=DetCanalizaciones::trueOrFalse($_POST['fuero_comun']);
		$fuero_federal=DetCanalizaciones::trueOrFalse($_POST['fuero_federal']);
		$mixta=DetCanalizaciones::trueOrFalse($_POST['mixta']);
		$tutelar_menores=DetCanalizaciones::trueOrFalse($_POST['tutelar_menores']);
		$migracion=DetCanalizaciones::trueOrFalse($_POST['migracion']);
		$canaOtros=$_POST['canaOtros'];

		$ape_pat_afectado=$_POST['ape_pat_afectado'];
		$ape_mat_afectado=$_POST['ape_mat_afectado'];
		$nombre_afectado=$_POST['nombre_afectado'];
		$sexoAfectado=$_POST['sexoAfectado'];

		$id_persona_afectada=$_POST['id_persona_afectada'];
		$calle_afectado=$_POST['calle_afectado'];
		$municipioAfectado=$_POST['municipioAfectado'];
		$EstadoAfectado=$_POST['EstadoAfectado'];

		$accion=$_POST['guarda'];

		/*$datos=array(
			'fecha'=>$fecha,
			'detenido_en'=>$detenido_en,
			'ocupacion'=>$ocupacion,
			'ape_pat'=>$ape_pat,
			'ape_mat'=>$ape_mat,
			'nombre'=>$nombre,
			'telefono'=>$telefono,
			'sexo'=>$sexo,
			'fecha_nacimiento'=>$fecha_nacimiento,
			'estado_civil'=>$estado_civil,
			'nacionalidad'=>$nacionalidad,
			'señas_particulares'=>$señas_particulares,
			'lesiones_presenta'=>$lesiones_presenta,
			'id_escolaridad'=>$id_escolaridad
		)*/

		if ($accion=='guardar') {
			$persona=new PerPersonas();
				$persona->nombre=				$nombre;
				$persona->ape_pat=				$ape_pat;
				$persona->ape_mat=				$ape_mat;
				//$persona->telefono=				$telefono;
				$persona->sexo=					$sexo;
				$persona->fecha_nacimiento=		$fecha_nacimiento;
				$persona->estado_civil=			$estado_civil;
				$persona->nacionalidad=			$nacionalidad;
				$persona->señas_particulares=	$señas_particulares;
				$persona->lesiones_presenta=	$lesiones_presenta;
				$persona->id_escolaridad=		$id_escolaridad;
				$persona->edad=					$edad;
			$persona->save();

			$lugarAseguramiento= new DetLugarAseguramiento();
				$lugarAseguramiento->id_colonia		=$colonia;
				$lugarAseguramiento->calle			=$calle;
				$lugarAseguramiento->entre_calle	=$entre_calle;
				$lugarAseguramiento->y_la_calle		=$y_la_calle;
				$lugarAseguramiento->a_la_altura	=$a_la_altura;
			$lugarAseguramiento->save();

			if ($id_persona=='') {
				$id_persona=PerPersonas::getPersona();
			}else{
				$foto=date('YmdHis');
				$persona = PerPersonas::model()->findByAttributes(array('id_persona'=>$id_persona));
				$persona->nombre=					$nombre;
				$persona->ape_pat=					$ape_pat;
				$persona->ape_mat=					$ape_mat;
				//$persona->telefono=				$telefono;
				$persona->sexo=						$sexo;
				//$persona->fecha_nacimiento=		$fecha_nacimiento;
				$persona->estado_civil=				$estado_civil;
				$persona->nacionalidad=				$nacionalidad;
				$persona->señas_particulares=		$señas_particulares;
				$persona->lesiones_presenta=		$lesiones_presenta;
				$persona->id_escolaridad=			$id_escolaridad;
				$persona->edad=						$edad;
				$persona->foto=						$foto;

				$persona->SaveAttributes(array(
					'nombre',
					'ape_pat',
					'ape_mat',
					//'telefono',
					'sexo',
					//'fecha_nacimiento',
					'estado_civil',
					'nacionalidad',
					'señas_particulares',
					'lesiones_presenta',
					'id_escolaridad',
					'edad',
					'foto'
					)
				);
			}
			$id_lugar=DetLugarAseguramiento::getLugarAseguramiento();

			$descripcionFisica = new PerDescripcionFisica();
				$descripcionFisica->estatura			=$estatura;
				$descripcionFisica->color_piel			=$color_piel;
				$descripcionFisica->cejas				=$ceja;
				$descripcionFisica->color_ojos			=$color_ojos;
				$descripcionFisica->color_pelo			=$color_pelo;
				$descripcionFisica->menton				=$menton;
				$descripcionFisica->frente				=$frente;
				$descripcionFisica->complexion			=$complexion;
				$descripcionFisica->id_persona			=$id_persona;
			$descripcionFisica->save();

			if ($id_domicilio=='') {
			$domicilio= new PerDomicilios();
				$domicilio->calle			=$domCalle;
				$domicilio->id_colonia			=$domColonia;
				$domicilio->id_municipio	=$domMunicipio;
				$domicilio->id_persona		=$id_persona;
			$domicilio->save();
			}else{
				$domicilio = PerDomicilios::model()->findByAttributes(array('id_domicilio'=>$id_domicilio));
				$domicilio->calle			=$domCalle;
				$domicilio->id_colonia			=$domColonia;
				$domicilio->id_municipio	=$domMunicipio;
				$domicilio->id_persona		=$id_persona;

				$domicilio->SaveAttributes(array(
					'calle',
					'id_colonia',
					'id_municipio',
					'id_persona'
				));
			}

			$objetosPorta = new DetObjetosPorta();
				$objetosPorta->camisa=			$camisa;
				$objetosPorta->tenis=			$tenis;
				$objetosPorta->pulsera=			$pulsera;
				$objetosPorta->cartera=			$cartera;
				$objetosPorta->pantalon=		$pantalon;
				$objetosPorta->zapatos=			$zapatos;
				$objetosPorta->gorra=			$gorra;
				$objetosPorta->lentes=			$lentes;
				$objetosPorta->playera=			$playera;
				$objetosPorta->botas=			$botas;
				$objetosPorta->cinturon=		$cinturon;
				$objetosPorta->sombrero=		$sombrero;
				$objetosPorta->chamarra=		$chamarra;
				$objetosPorta->otros=			$otros;
				$objetosPorta->descripcion=		$obDescripcion;
				$objetosPorta->cadena=			$cadena;
			$objetosPorta->save();

			$canalizacion= new DetCanalizaciones();
				$canalizacion->juez_calificador		=$juez_calificador;
				$canalizacion->fuero_comun			=$fuero_comun;
				$canalizacion->fuero_federal		=$fuero_federal;
				$canalizacion->mixta				=$mixta;
				$canalizacion->tutelar_menores		=$tutelar_menores;
				$canalizacion->migracion			=$migracion;
				$canalizacion->otro					=$canaOtros;
			$canalizacion->save();

			$detencion=new DetDetenciones();
				$detencion->fecha=	$fecha;
				$detencion->modus_operandi=	$modus_operandi;
				$detencion->bienes_afectados=	$bienes_afectados;
				$detencion->monto_aproximado_afectacion=	$monto_aproximado_afectacion;
				$detencion->detenido_en=			$detenido_en;
				$detencion->ocupacion=				$ocupacion;
				$detencion->id_condicion=			$id_condicion;
				$detencion->id_identificacion=		$id_identificacion;
				$detencion->id_banda=				$id_banda;
				$detencion->id_persona=				$id_persona;
				//$detencion->id_dinero=			$id_dinero;
				$detencion->id_objetos_porta=		$objetosPorta['id_objetos_porta'];
				$detencion->id_canalizacion=		$canalizacion['id_canalizacion'];
				$detencion->id_municipio=			$id_municipio;
				$detencion->id_corporacion=			$id_corporacion;
				$detencion->id_lugar=				$id_lugar;
				$detencion->id_zona=					$id_zona;
				$detencion->hora=					$hora;
			$detencion->save();
			
			$personaAfectada= new PerPersonas();
				$personaAfectada->ape_pat				=$ape_pat_afectado;
				$personaAfectada->ape_mat				=$ape_mat_afectado;
				$personaAfectada->nombre				=$nombre_afectado;
				$personaAfectada->sexo					=$sexoAfectado;
			$personaAfectada->save();

			$domicilioAfectada= new PerDomicilios();
				$domicilioAfectada->calle				=$calle_afectado;
				$domicilioAfectada->id_municipio		=$municipioAfectado;
				//$domicilioAfectada->id_estado			=$EstadoAfectado;
				$domicilioAfectada->id_persona			=$personaAfectada['id_persona'];
			$domicilioAfectada->save();


			/*$afectado= new DetDetencionesAfectado();
				$afectado->id_detencion		=$detencion['id_detencion'];
				$afectado->id_persona 		=$personaAfectada['id_persona'];
				$afectado->id_domicilio		=$domicilioAfectada['id_domicilio'];
			$afectado->save();*/
			echo CJSON::encode(array(
				'id_persona'=>$persona['id_persona'],
				'id_descripcion'=>$descripcionFisica['id_descripcion'],
				'id_lugar'=>$lugarAseguramiento['id_lugar'],
				'id_domicilio'=>$domicilio['id_domicilio'],
				'id_domicilio'=>$domicilioAfectada['id_domicilio'],
				'id_detencion'=>$detencion['id_detencion'],
				'id_objetos_porta'=>$objetosPorta['id_objetos_porta'],
				'id_canalizacion'=>$canalizacion['id_canalizacion'],
				)
			);
			//$this->redirect(array('admin'));
		}
		if ($accion=='actualizar') {
			$persona = PerPersonas::model()->findByAttributes(array('id_persona'=>$id_persona));
			$persona->nombre=					$nombre;
			$persona->ape_pat=					$ape_pat;
			$persona->ape_mat=					$ape_mat;
			//$persona->telefono=				$telefono;
			$persona->sexo=						$sexo;
			//$persona->fecha_nacimiento=		$fecha_nacimiento;
			$persona->estado_civil=				$estado_civil;
			$persona->nacionalidad=				$nacionalidad;
			$persona->señas_particulares=		$señas_particulares;
			$persona->lesiones_presenta=		$lesiones_presenta;
			$persona->id_escolaridad=			$id_escolaridad;
			$persona->edad=						$edad;

			$persona->SaveAttributes(array(
				'nombre',
				'ape_pat',
				'ape_mat',
				//'telefono',
				'sexo',
				//'fecha_nacimiento',
				'estado_civil',
				'nacionalidad',
				'señas_particulares',
				'lesiones_presenta',
				'id_escolaridad',
				'edad'
				)
			);

			$lugarAseguramiento = DetLugarAseguramiento::model()->findByAttributes(array('id_lugar'=>$id_lugar));
			//$lugarAseguramiento->id_colonia		=$colonia;
			$lugarAseguramiento->calle			=$calle;
			$lugarAseguramiento->entre_calle	=$entre_calle;
			$lugarAseguramiento->y_la_calle		=$y_la_calle;
			$lugarAseguramiento->a_la_altura	=$a_la_altura;

			$lugarAseguramiento->SaveAttributes(array(
				'id_colonia',
				'calle',
				'entre_calle',
				'y_la_calle',
				'a_la_altura'
			));

			$descripcionFisica = PerDescripcionFisica::model()->findByAttributes(array('id_descripcion'=>$id_descripcion));
			$descripcionFisica->estatura			=$estatura;
			$descripcionFisica->color_piel			=$color_piel;
			$descripcionFisica->cejas				=$ceja;
			$descripcionFisica->color_ojos			=$color_ojos;
			$descripcionFisica->color_pelo			=$color_pelo;
			$descripcionFisica->menton				=$menton;
			$descripcionFisica->frente				=$frente;
			$descripcionFisica->complexion			=$complexion;

			$descripcionFisica->SaveAttributes(array(
				'estatura',
				'color_piel',
				'cejas',
				'color_ojos',
				'color_pelo',
				'menton',
				'frente',
				'complexion'
			));

			$domicilio = PerDomicilios::model()->findByAttributes(array('id_domicilio'=>$id_domicilio));
			$domicilio->calle			=$domCalle;
			$domicilio->id_colonia			=$domColonia;
			$domicilio->id_municipio	=$domMunicipio;
			$domicilio->id_persona		=$id_persona;

			$domicilio->SaveAttributes(array(
				'calle',
				'id_colonia',
				'id_municipio',
				'id_persona'
			));
			$objetosPorta = DetObjetosPorta::model()->findByAttributes(array('id_objetos_porta'=>$id_objetos_porta));
				$objetosPorta->camisa=			$camisa;
				$objetosPorta->tenis=			$tenis;
				$objetosPorta->pulsera=			$pulsera;
				$objetosPorta->cartera=			$cartera;
				$objetosPorta->pantalon=		$pantalon;
				$objetosPorta->zapatos=			$zapatos;
				$objetosPorta->gorra=			$gorra;
				$objetosPorta->lentes=			$lentes;
				$objetosPorta->playera=			$playera;
				$objetosPorta->botas=			$botas;
				$objetosPorta->cinturon=		$cinturon;
				$objetosPorta->sombrero=		$sombrero;
				$objetosPorta->chamarra=		$chamarra;
				$objetosPorta->otros=			$otros;
				$objetosPorta->descripcion=		$obDescripcion;
				$objetosPorta->cadena=			$cadena;
			$objetosPorta->SaveAttributes(array(
				'camisa',
				'tenis',
				'pulsera',
				'cartera',
				'pantalon',
				'zapatos',
				'gorra',
				'lentes',
				'playera',
				'botas',
				'cinturon',
				'sombrero',
				'chamarra',
				'otros',
				'descripcion',
				'cadena'
			));
			$canalizacion = DetCanalizaciones::model()->findByAttributes(array('id_canalizacion'=>$id_canalizacion));
				$canalizacion->juez_calificador		=$juez_calificador;
				$canalizacion->fuero_comun			=$fuero_comun;
				$canalizacion->fuero_federal		=$fuero_federal;
				$canalizacion->mixta				=$mixta;
				$canalizacion->tutelar_menores		=$tutelar_menores;
				$canalizacion->migracion			=$migracion;
				$canalizacion->otro			=$canaOtros;
			$canalizacion->SaveAttributes(array(
				'juez_calificador',
				'fuero_comun',
				'fuero_federal',
				'mixta',
				'tutelar_menores',
				'migracion',
				'otro'
			));

			$detencion = DetDetenciones::model()->findByAttributes(array('id_detencion'=>$id_detencion));
			$detencion->fecha=	$fecha;
			$detencion->modus_operandi=	$modus_operandi;
			$detencion->bienes_afectados=	$bienes_afectados;
			$detencion->monto_aproximado_afectacion=	$monto_aproximado_afectacion;
			$detencion->detenido_en=			$detenido_en;
			$detencion->ocupacion=				$ocupacion;
			$detencion->id_condicion=			$id_condicion;
			$detencion->id_identificacion=		$id_identificacion;
			$detencion->id_banda=				$id_banda;
			$detencion->id_persona=				$id_persona;
			//$detencion->id_dinero=			$id_dinero;
			$detencion->id_objetos_porta=		$id_objetos_porta;
			$detencion->id_canalizacion=		$id_canalizacion;
			$detencion->id_municipio=			$id_municipio;
			$detencion->id_corporacion=			$id_corporacion;
			$detencion->id_lugar=				$id_lugar;
			$detencion->id_zona=				$id_zona;
			$detencion->hora=					$hora;

			$detencion->SaveAttributes(array(
				'fecha',
				'modus_operandi',
				'bienes_afectados',
				'monto_aproximado_afectacion',
				'detenido_en',
				'ocupacion',
				'id_condicion',
				'id_identificacion',
				'id_banda',
				'id_persona',
				//'id_dinero',
				'id_objetos_porta',
				'id_canalizacion',
				'id_municipio',
				'id_corporacion',
				'id_lugar',
				'id_zona',
				'hora'
			));
/*
			$personaAfectada = PerPersonas::model()->findByAttributes(array('id_persona'=>$id_persona_afectada));
			$personaAfectada= new PerPersonas();
				$personaAfectada->ape_pat				=$ape_pat_afectado;
				$personaAfectada->ape_mat				=$ape_mat_afectado;
				$personaAfectada->nombre				=$nombre_afectado;
				$personaAfectada->sexo					=$sexoAfectado;
			$personaAfectada->SaveAttributes(array(
				'ape_pat',
				'ape_mat',
				'nombre',
				'sexo'
			));
			
			$domicilioAfectada = PerDomicilios::model()->findByAttributes(array('id_domicilio'=>$id_domicilio_afectada));
				$domicilioAfectada->calle				=$calle_afectado;
				$domicilioAfectada->id_municipio		=$municipioAfectado;
				//$domicilioAfectada->id_estado			=$EstadoAfectado;
				$domicilioAfectada->id_persona			=$personaAfectada['id_persona'];
			$personaAfectada->SaveAttributes(array(
				'calle',
				'id_municipio',
				//'id_estado',
				'id_persona'
			));*/
			//$this->redirect(array('admin'));
		}
	}
	public function actionCreate()
	{
		$model=new DetDetenciones;

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['DetDetenciones']))
		{
			$model->attributes=$_POST['DetDetenciones'];
			if($model->save())
				$this->redirect(array('admin','id'=>$model->id_detencion));
		}

		$this->render('create',array(
			'model'=>$model,
		));
	}

	/**
	 * Updates a particular model.
	 * If update is successful, the browser will be redirected to the 'view' page.
	 * @param integer $id the ID of the model to be updated
	 */
	public function actionUpdate($id)
	{
		$model=$this->loadModel($id);

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['DetDetenciones']))
		{
			$model->attributes=$_POST['DetDetenciones'];
			if($model->save())
				$this->redirect(array('admin','id'=>$model->id_detencion));
		}

		$this->render('update',array(
			'model'=>$model,
		));
	}

	/**
	 * Deletes a particular model.
	 * If deletion is successful, the browser will be redirected to the 'admin' page.
	 * @param integer $id the ID of the model to be deleted
	 */
	public function actionDelete($id)
	{
		$this->loadModel($id)->delete();

		// if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
		if(!isset($_GET['ajax']))
			$this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
	}

	/**
	 * Lists all models.
	 */
	public function actionIndex()
	{
		$dataProvider=new CActiveDataProvider('DetDetenciones');
		$this->render('index',array(
			'dataProvider'=>$dataProvider,
		));
	}

	/**
	 * Manages all models.
	 */
	public function actionAdmin()
	{
		$model=new DetDetenciones('search');
		$model->unsetAttributes();  // clear any default values
		if(isset($_GET['DetDetenciones']))
			$model->attributes=$_GET['DetDetenciones'];

		$this->render('admin',array(
			'model'=>$model,
		));
	}

	/**
	 * Returns the data model based on the primary key given in the GET variable.
	 * If the data model is not found, an HTTP exception will be raised.
	 * @param integer the ID of the model to be loaded
	 */
	public function loadModel($id)
	{
		$model=DetDetenciones::model()->findByPk($id);
		if($model===null)
			throw new CHttpException(404,'The requested page does not exist.');
		return $model;
	}

	/**
	 * Performs the AJAX validation.
	 * @param CModel the model to be validated
	 */
	protected function performAjaxValidation($model)
	{
		if(isset($_POST['ajax']) && $_POST['ajax']==='det-detenciones-form')
		{
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}
	}
}

<?php

class RegistroController extends Controller
{
	/**
	 * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
	 * using two-column layout. See 'protected/views/layouts/column2.php'.
	 */
	public $layout='//layouts/column2';

	/**
	 * @return array action filters
	 */
	public function filters()
	{
		return array(array('CrugeAccessControlFilter'));
	}

	/**
	 * Specifies the access control rules.
	 * This method is used by the 'accessControl' filter.
	 * @return array access control rules
	 */
	public function accessRules()
	{
		return array(
			
			array('allow', // allow authenticated user to perform 'create' and 'update' actions
				'actions'=>array('Registro'),
				'users'=>array('@'),
			),
			array('allow', // allow admin user to perform 'admin' and 'delete' actions
				'actions'=>array('Registro'),
				'users'=>array('admin'),
			),
			array('deny',  // deny all users
				'users'=>array('*'),
			),
		);
	}

	public function actionRegistro()
	{
		//var_dump($_POST);
		//Yii::app()->end();
		$perPersona=new PerPersonas();
		$detDetenciones=new DetDetenciones(); 
		
		$detIdentificacion=new DetIdentificacion();
		$perDescripcionFisica=new PerDescripcionFisica();
		$detCondicionesFisicas=new DetCondicionesFisicas();
		$detObjetosPorta=new DetObjetosPorta();
		$basMotivos=new BasMotivos();
		$detLugarAseguramiento=new DetLugarAseguramiento();
		$detDinero=new DetDinero();

		$this->performAjaxValidation($perPersona);
		$this->performAjaxValidation($detDetenciones);
		$this->performAjaxValidation($perDescripcionFisica);
		$this->performAjaxValidation($detCondicionesFisicas);
		$this->performAjaxValidation($detObjetosPorta);
		$this->performAjaxValidation($basMotivos);

		if (isset($_POST["PerPersonas"]) && isset($_POST["DetIdentificacion"]) &&
			isset($_POST["DetDetenciones"]) && isset($_POST["DetCondicionesFisicas"])
			&& isset($_POST["PerDescripcionFisica"]) && isset($_POST["DetObjetosPorta"]) &&
			isset($_POST["BasMotivos"]) && isset($_POST["DetLugarAseguramiento"]) &&
			isset($_POST["DetDinero"])) {

			$perPersona->attributes=$_POST["PerPersonas"];
			$detDetenciones->attributes=$_POST["DetDetenciones"];
			$detIdentificacion->attributes=$_POST["DetIdentificacion"];
			$perDescripcionFisica->attributes=$_POST["PerDescripcionFisica"];
			$basMotivos->attributes=$_POST["BasMotivos"];
			$detCondicionesFisicas->attributes=$_POST["DetCondicionesFisicas"];
			$detDinero->attributes=$_POST["DetDinero"];
			$detObjetosPorta->attributes=$_POST["DetObjetosPorta"];
			$detLugarAseguramiento->attributes=$_POST["DetLugarAseguramiento"];
			$detLugarAseguramiento->id_barrio=1;

			if ($perPersona->save() && $detIdentificacion->save() && $detCondicionesFisicas->save()
				&& $detDinero->save() && $detObjetosPorta->save() && $detLugarAseguramiento->save()) {
				$perDescripcionFisica->id_persona=$perPersona->getPrimaryKey();
				$detDetenciones->id_persona=$perPersona->getPrimaryKey();
				$detDetenciones->id_banda=1;
				$detDetenciones->id_identificacion=$detIdentificacion->getPrimaryKey();
				$detDetenciones->id_condicion=$detCondicionesFisicas->getPrimaryKey();
				$detDetenciones->id_dinero=$detDinero->getPrimaryKey();
				$detDetenciones->id_objetos_porta=$detObjetosPorta->getPrimaryKey();
				$detDetenciones->id_canalizacion=2;
				$detDetenciones->id_municipio=1;
				$detDetenciones->id_corporacion=2;
				$detDetenciones->id_lugar=$detLugarAseguramiento->getPrimaryKey();

			}
			
			if ($perPersona->save() && $detIdentificacion->save() && $perDescripcionFisica->save()) {
				echo "<script>console.log( 'Debug Objects: Persona->".$perPersona->getPrimaryKey()."' );</script>";
				
				$detDetenciones->id_persona=$perPersona->getPrimaryKey();
				
			}
		}
		$this->render('registro',array(
			"perPersona"=>$perPersona,
			"detDetenciones"=>$detDetenciones,
			"detIdentificacion"=>$detIdentificacion,
			"perDescripcionFisica"=>$perDescripcionFisica,
			"detCondicionesFisicas"=>$detCondicionesFisicas,
			"detObjetosPorta"=>$detObjetosPorta,
			"basMotivos"=>$basMotivos,
			"detLugarAseguramiento"=>$detLugarAseguramiento,
			"detDinero"=>$detDinero));
	}

	/**
	 * Returns the data model based on the primary key given in the GET variable.
	 * If the data model is not found, an HTTP exception will be raised.
	 * @param integer the ID of the model to be loaded
	 */
	public function loadModel($id)
	{
		$model=PerPersonas::model()->findByPk($id);
		if($model===null)
			throw new CHttpException(404,'The requested page does not exist.');
		return $model;
	}

	/**
	 * Performs the AJAX validation.
	 * @param CModel the model to be validated
	 */
	protected function performAjaxValidation($model)
	{
		if(isset($_POST['ajax']) && $_POST['ajax']==='per-personas-form')
		{
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}
	}
}
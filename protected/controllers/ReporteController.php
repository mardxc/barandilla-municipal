<?php

class ReporteController extends Controller
{
	public $layout='//layouts/column2';

	// Uncomment the following methods and override them if needed
	
	public function filters()
	{
		 return array(array('CrugeAccessControlFilter'));
	}

	public function accessRules()
	{
		return array(
			array('allow',  // allow all users to perform 'index' and 'view' actions
				'actions'=>array('index','view'),
				'users'=>array('*'),
			),
			array('allow', // allow authenticated user to perform 'create' and 'update' actions
				'actions'=>array('admin','ImprimeListado','printList','pdf'),
				'users'=>array('@'),
			),
			array('allow', // allow admin user to perform 'admin' and 'delete' actions
				'actions'=>array('admin','delete'),
				'users'=>array('admin'),
			),
			array('deny',  // deny all users
				'users'=>array('*'),
			),
		);
	}
	public function actionAdmin()
	{
		$sql="SELECT 
				dd.id_detencion,
			    CONCAT(pp.nombre,' ',pp.ape_pat,' ',pp.ape_mat) AS nombre,
			    pp.edad AS edad,
			    dd.fecha AS fecha_det,
			    dd.hora AS hora_det,
			    dd.ocupacion AS ocupacion,
			    bm.motivo AS motivo_det,
			    bc.corporacion AS corporacion,
			    bz.zona AS zona_det,
			    dd.id_canalizacion AS canalizacion,
			    pd.calle AS calle_dom,
			    pd.id_colonia AS colonia_dom,
			    pd.id_municipio AS municipio_dom,
			    pp.estado_civil AS civil,
			    bcol.colonia as colonia_det,
			    dla.calle as calle_det,
			    dla.entre_calle as entre_calle_det,
			    dla.y_la_calle as y_la_calle_det,
			    dla.a_la_altura as a_la_altura_det
			FROM
			    per_personas pp
			        INNER JOIN
			    per_domicilios pd ON pp.id_persona = pd.id_persona
			        INNER JOIN
			    det_detenciones dd ON pp.id_persona = dd.id_persona
			        INNER JOIN
			    det_motivos dm ON dd.id_detencion = dm.id_detencion
			        INNER JOIN
			    det_lugar_aseguramiento dla ON dd.id_lugar = dd.id_lugar
			    	INNER JOIN
			    bas_motivos bm ON dm.id_motivo=bm.id_motivo
			    	INNER JOIN
			    bas_corporacion bc ON dd.id_corporacion=bc.id_corporacion
			    	INNER JOIN
			    bas_zona bz ON dd.id_zona=bz.id_zona
			    INNER JOIN
			    bas_colonia bcol ON dla.id_colonia=bcol.id_colonia
			    GROUP BY dd.id_detencion";
		$dataProviderPro=new CSqlDataProvider($sql,
				array(
					'keyField'=>'id_detencion',
					'pagination'=>array('pageSize'=>100,)
				)
			);
		$this->render('admin',array(
			'dataProviderPro'=>$dataProviderPro
		));
	}
	public function actionImprimeListado(){

				$id_zona=isset($_GET['id_zona']) ? $_GET['id_zona'] : 0;

				$sqlGridFiltrado="SELECT 
				dd.id_detencion,
			    CONCAT(pp.nombre,' ',pp.ape_pat,' ',pp.ape_mat) AS nombre,
			    pp.edad AS edad,
			    dd.fecha AS fecha_det,
			    dd.hora AS hora_det,
			    dd.ocupacion AS ocupacion,
			    bm.motivo AS motivo_det,
			    bc.corporacion AS corporacion,
			    bz.zona AS zona_det,
			    dd.id_canalizacion AS canalizacion,
			    pd.calle AS calle_dom,
			    pd.id_colonia AS colonia_dom,
			    pd.id_municipio AS municipio_dom,
			    pp.estado_civil AS civil,
			    bcol.colonia as colonia_det,
			    dla.calle as calle_det,
			    dla.entre_calle as entre_calle_det,
			    dla.y_la_calle as y_la_calle_det,
			    dla.a_la_altura as a_la_altura_det
			FROM
			    per_personas pp
			        INNER JOIN
			    per_domicilios pd ON pp.id_persona = pd.id_persona
			        INNER JOIN
			    det_detenciones dd ON pp.id_persona = dd.id_persona
			        INNER JOIN
			    det_motivos dm ON dd.id_detencion = dm.id_detencion
			        INNER JOIN
			    det_lugar_aseguramiento dla ON dd.id_lugar = dd.id_lugar
			    	INNER JOIN
			    bas_motivos bm ON dm.id_motivo=bm.id_motivo
			    	INNER JOIN
			    bas_corporacion bc ON dd.id_corporacion=bc.id_corporacion
			    	INNER JOIN
			    bas_zona bz ON dd.id_zona=bz.id_zona
			    	INNER JOIN
			    bas_colonia bcol ON dla.id_colonia=bcol.id_colonia
			    	
				WHERE dd.id_zona=".$id_zona;

				$edad=
					empty($_GET['edad']) ? 
						0
						: 
						$sqlGridFiltrado=
							$sqlGridFiltrado . ' AND pp.edad BETWEEN ' .
						$_GET['edadInicio'].' AND '.$_GET['edadFinal'];

				$estado_civil=
					$_GET['estado_civil']=='empty' ? 
						0
						: 
						$sqlGridFiltrado=
							$sqlGridFiltrado . ' AND pp.estado_civil LIKE "%' .
						$_GET['estado_civil'].'%"';

				$hora=
					empty($_GET['hora']) ? 
						0
						: 
						$sqlGridFiltrado=
							$sqlGridFiltrado . ' AND dd.hora BETWEEN ' .
						$_GET['horaInicio'].' AND '.$_GET['horaFinal'];

				$ocupacion=
					empty($_GET['ocupacion']) ? 
						0
						: 
						$sqlGridFiltrado=
							$sqlGridFiltrado . ' AND dd.ocupacion LIKE "%' .
						$_GET['ocupacion'].'%"';

				$id_motivo=
					empty($_GET['id_motivo']) ? 
						0
						: 
						$sqlGridFiltrado=
							$sqlGridFiltrado . ' AND dm.id_motivo = ' .
						$_GET['id_motivo'];

				$id_corporacion=
					empty($_GET['id_corporacion']) ? 
						0
						: 
						$sqlGridFiltrado=
							$sqlGridFiltrado . ' AND dd.id_corporacion = ' .
						$_GET['id_corporacion'];

				/*$id_zona=
					empty($_GET['id_zona']) ? 
						0
						: 
						$sqlGridFiltrado=
							$sqlGridFiltrado . ' AND dla.id_zona = ' .
						$_GET['id_zona'];*/

				/*$dispocicion=
					empty($_GET['dispocicion']) ? 
						0
						: 
						$sqlGridFiltrado=
							$sqlGridFiltrado . ' AND dm.dispocicion = ' .
						$_GET['dispocicion'];*/

				$id_barrio=
					empty($_GET['id_colonia']) ? 
						0
						: 
						$sqlGridFiltrado=
							$sqlGridFiltrado . ' AND dla.id_colonia = ' .
						$_GET['id_colonia'];
				$sqlGridFiltrado= $sqlGridFiltrado.' GROUP BY dd.id_detencion';
				$dataProviderPro=new CSqlDataProvider($sqlGridFiltrado,
						array(
							'keyField'=>'id_detencion',
							'pagination'=>array('pageSize'=>100,)
						)
					);
				$this->render('admin',array(
					'dataProviderPro'=>$dataProviderPro
				));

		//$data=Yii::app()->db->createCommand($sqlGridFiltrado)->queryAll();

		//$this->renderPartial("printList"),array("model"=>$data),true);
		//$html2pdf = Yii::app()->ePdf->HTML2PDF('L');
		//$html2pdf = Yii::app()->ePdf->HTML2PDF('p',
		//	array(150,150),'en',true,'UTF-8',array(0,0,0,0));

        /*$html2pdf->WriteHTML($this->renderPartial('printList', array(
			'data'=>$data
        	), true));

        $html2pdf->Output('c:\wampadmin.pdf');*/
	}
	public function actionPrintList(){
		//$this->renderPartial("printList");

		/*$html2pdf = Yii::app()->ePdf->HTML2PDF('p',
			array(150,150),'en',true,'UTF-8',array(0,0,0,0));*/
			//$html2pdf = Yii::app()->ePdf->HTML2PDF('L');

		$id_detencion=$_GET['id_detencion'];
		if (!strpos($id_detencion,',')) {

			$arraId=explode(",", $id_detencion);

			$html2pdf = Yii::app()->ePdf->HTML2PDF('P');
			$html2pdf->WriteHTML($this->renderPartial(
				'printMultiList',
				array(
					'arraId'=>$arraId,
				), true
			));
			$html2pdf->Output('test.pdf');
		}else{
			
			//$html2pdf=new HTML2PDF('P','A4','es','true','UTF-8');
			$html2pdf = Yii::app()->ePdf->HTML2PDF('P');
			//$stylesheet = file_get_contents('certificate.css'); /// here call you external css file 
			//$stylesheet = file_get_contents('themes/miminum/css/bootstrap.min.css');
			//$stylesheet = file_get_contents(Yii::app()->theme->baseUrl'./css/bootstrap.min.css');
			//$html2pdf->WriteHTML($stylesheet);
			$html2pdf->WriteHTML($this->renderPartial(
				'printList',
				array(
					'persona'=>$persona,
	    			'domicilio'=>$domicilio,
	    			'descripcionFisica'=>$descripcionFisica,
	    			'detencion'=>$detencion,
	    			'objetosPorta'=>$objetosPorta,
				), true
			));
			$html2pdf->Output('test.pdf');
		}
	}

	 public function actionPdf()
    {
    	set_time_limit(300);
    	$id_detencion=$_GET['id_detencion'];
    	$arraId=explode(",", $id_detencion);

    	$html2pdf = Yii::app()->ePdf->HTML2PDF('P');
    	$html2pdf->WriteHTML($this->renderPartial(
    		'printMultiList',
    		array(
    			'arraId'=>$arraId,
    		), true
    	));
    	$html2pdf->Output('test.pdf');
    }
}
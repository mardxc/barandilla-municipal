<!DOCTYPE html>

<!--[if lt IE 7]> <html class="no-js lt-ie9 lt-ie8 lt-ie7" lang="en"> <![endif]-->
<!--[if IE 7]>    <html class="no-js lt-ie9 lt-ie8" lang="en"> <![endif]-->
<!--[if IE 8]>    <html class="no-js lt-ie9" lang="en"> <![endif]-->

<!--[if gt IE 8]><!--> <html class="no-js" lang="en"> <!--<![endif]-->

<head>
	<meta charset="UTF-8">
	
	<!-- Remove this line if you use the .htaccess -->
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">

	<meta name="viewport" content="width=device-width">
	
	<meta name="description" content="Miminum template.">
	
	<title><?php echo CHtml::encode($this->pageTitle); ?></title>
<!--<link rel="shortcut icon" href="<?php echo Yii::app()->theme->baseUrl; ?>/img/logomi.png">-->
<!-- start: Javascript -->
<?php
  $baseUrl = Yii::app()->theme->baseUrl; 
  $cs = Yii::app()->getClientScript();
  Yii::app()->clientScript->registerCoreScript('jquery');
  $cs->registerCoreScript('bbq');
?>
<?php 
	$cs->registerCssFile($baseUrl.'/css/bootstrap.min.css');
	$cs->registerCssFile($baseUrl.'/css/plugins/font-awesome.min.css');
	$cs->registerCssFile($baseUrl.'/css/style.css');
	$cs->registerCssFile($baseUrl.'/css/plugins/mediaelementplayer.css');
 ?>
  <?php
  //$cs->registerScriptFile($baseUrl.'/js/jquery.min.js');
  //$cs->registerScriptFile($baseUrl.'/js/jquery.ui.min.js');
  $cs->registerScriptFile($baseUrl.'/js/bootstrap.min.js');
  $cs->registerScriptFile($baseUrl.'/js/plugins/moment.min.js');
  $cs->registerScriptFile($baseUrl.'/js/plugins/jquery.nicescroll.js');
//  $cs->registerScriptFile($baseUrl.'/js/main.js');
  $cs->registerScriptFile($baseUrl.'/js/functionss.js');
?>
</head>
<body id="mimin" class="dashboard">
	<header id="navtop">
		<nav class="fright">
			<!-- start: Header -->
			<div class="container">
				<div class="col-md-8">
			<nav class="navbar navbar-default header navbar-fixed-top">
			    <div class="col-md-12 nav-wrapper">
			      <div class="navbar-header">
			        <div class="opener-left-menu is-open">
			          <span class="top"></span>
			          <span class="middle"></span>
			          <span class="bottom"></span>
			        </div>
			          <a href="#" class="navbar-brand"> 
			           <b>Barandilla</b>
			          </a>
			      </div>
			      
			  </nav>
				</div>
			</div>
			  
			<!-- end: Header -->
		</nav>
	</header>
	
		    <!-- start:Left Menu -->
		    <div class="home-page main">
		      <div id="left-menu">
		        <div class="sub-left-menu scroll">
		          <ul class="nav nav-list ">
		              <li><div class="left-bg"></div></li>
		              <li class="time">
		                <h1 class="animated fadeInLeft">21:00</h1>
		              </li>


 					 <li class="ripple">
 					 	<a href="/Barandilla/barandilla0/index.php?r=site/index">
 					 		<span class="fa-home fa"></span>Inicio</a>
 					 	</li>
 		              <li class="ripple">
 		              	<a class="tree-toggle nav-header">Configuracion Inicial.
 		              		<span class="fa-angle-right fa right-arrow text-right"></span>
 		              	</a>
 		                <ul class="nav nav-list tree">
   							<li>
   								<a href="index.php?r=basZona/admin">Registro de Zonas</a>
   							</li>
   							<li>
   								<a href="index.php?r=basCorporacion/admin">Registro de Corporaciones</a>
   							</li>
   							<li>
   								<a href="index.php?r=basColonia/admin">Registro de Colonias</a>
   							</li>
   							<li>
   								<a href="index.php?r=basBarrios/admin">Registro de Barrios</a>
   							</li>
   							<li>
   								<a href="index.php?r=basMotivos/admin">Registro de Motivos de Detencion</a>
   							</li>
 		                </ul>
 		              </li>
		              <li class="ripple"><a class="tree-toggle nav-header">Detenciones <span class="fa-angle-right fa right-arrow text-right"></span> </a>
		                <ul class="nav nav-list tree">
		                	<li><a href="index.php?r=detDetencionesOficiales/admin">Registro de Oficiales</a></li>
		                	<li><a href="index.php?r=detDetenciones/admin">Dentencion Admin</a></li>
		                  <li><a href="index.php?r=detDetenciones/registrar&id_detencion=#">Dentencion MAYORES</a></li>
		                  <li><a href="index.php?r=detDetenciones/registrar2&id_detencion=#">Detencion MENORES</a></li>
		                 	<li><a href="index.php?r=reporte/admin">Reporte Admin</a></li>
		                </ul>
		              </li>

		              <li class="ripple">
		              	<a class="tree-toggle nav-header">Registro Individual.
		              		<span class="fa-angle-right fa right-arrow text-right"></span>
		              	</a>
		                <ul class="nav nav-list tree">
  							<li>
  								<a href="index.php?r=detalias/admin&id_persona=0">Registro de Alias</a>
  							</li>
		                </ul>
		              </li>

		              <li><a href="index.php?r=cruge/ui/login">Log In</a></li>
		            </ul>
		          </div>
		      </div>
		    <!-- end: Left Menu -->


		
		    <!-- start: content -->
		    
		    <!-- mainmenu -->
	<?php if(isset($this->breadcrumbs)):?>
		<?php $this->widget('zii.widgets.CBreadcrumbs', array(
			'links'=>$this->breadcrumbs,
		)); ?><!-- breadcrumbs -->
	<?php endif?>

	<?php echo $content; ?>
			    </div>



		   <div class="span9" style="position: absolute;">
			   	<div class="main" style="position: relative;">
			   		
			   		
			   	</div><!-- content -->
		   </div>
		    <!-- end: content -->
	</div>
	<div class="container">	
		    <div id="mainmenu">
		<?php $this->widget('zii.widgets.CMenu',array(
			'items'=>array(
				array('label'=>'Logout ('.Yii::app()->user->name.')', 'url'=>array('/site/logout'), 'visible'=>!Yii::app()->user->isGuest)
			),
		)); ?>
		<?php $this->widget('zii.widgets.CMenu',array(
			'items'=>array(
			array('label'=>'Home', 'url'=>array('/site/index')),
			array('label'=>'About', 'url'=>array('/site/page', 'view'=>'about')),
			array('label'=>'Contact', 'url'=>array('/site/contact')),
			array('label'=>'Administrar Usuarios', 
			'url'=>Yii::app()->user->ui->userManagementAdminUrl, 
			'visible'=>!Yii::app()->user->isGuest),
			array('label'=>'Login'
			, 'url'=>Yii::app()->user->ui->loginUrl
			, 'visible'=>Yii::app()->user->isGuest),
			array('label'=>'Logout ('.Yii::app()->user->name.')'
			, 'url'=>Yii::app()->user->ui->logoutUrl
			, 'visible'=>!Yii::app()->user->isGuest),
			),
			)); ?>
	</div>

		    </div>

		    




</body>
<!-- custom -->
 <script src="<?php echo Yii::app()->theme->baseUrl; ?>/js/main.js"></script>
</html>
